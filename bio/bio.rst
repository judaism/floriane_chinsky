.. index::
   pair: Biographie ; Floriane Chinsky

.. _bio:

==========================================================================
Biographie
==========================================================================

- https://rabbinchinsky.fr/rabbin-floriane-chinsky/

Après avoir suivi des études rabbiniques à Jérusalem et enseigné dans le
cadre des écoles juives modernistes TALI, puis au sein du mouvement de
jeunesse sioniste Noam, Floriane Chinsky a été ordonnée rabbin en 2005...
