# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
# sys.path.insert(0, os.path.abspath("./src"))

project = "Floriane Chinsky"
html_title = project

author = "Noam"
html_logo = "images/floriane_chinsky.jpg"
html_favicon = "images/floriane_chinsky.jpg"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]
extensions += ["sphinx.ext.intersphinx"]

# https://sphinxcontrib-youtube.readthedocs.io/en/latest/usage.html#configuration
extensions.append("sphinxcontrib.youtube")

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx_design")

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "bokertov": ("https://judaism.gitlab.io/bokertov/", None),
    "judaisme": ("https://judaism.gitlab.io/judaisme/", None),
    "link_judaisme": ("https://judaism.gitlab.io/linkertree/", None),
    "guerrieres": ("https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/", None),
    "shabbats": ("https://judaism.gitlab.io/shabbats/", None),
    "jjr": ("https://jjr.frama.io/juivesetjuifsrevolutionnaires/", None),
}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://judaism.gitlab.io/floriane_chinsky",
    "repo_url": "https://gitlab.com/judaism/floriane_chinsky",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "indigo",
    "color_accent": "cyan",
    "theme_color": "#2196f3",
    "master_doc": False,
    "nav_title": f"{project} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://rabbinchinsky.fr/",
            "internal": False,
            "title": "rabbinchinsky.fr",
        },
        {
            "href": "https://judaism.gitlab.io/shabbats/",
            "internal": False,
            "title": "Shabbats",
        },
        {
            "href": "https://www.youtube.com/c/FlorianeChinsky/videos",
            "internal": False,
            "title": "Youtube",
        },
        {
            "href": "https://peertube.iriseden.eu/a/floriane_c/video-channels",
            "internal": False,
            "title": "Peertube",
        },
        {
            "href": "https://judaism.gitlab.io/bokertov/",
            "internal": False,
            "title": "bokertov (2020)",
        },
        {
            "href": "https://judaism.gitlab.io/judaisme-2023/",
            "internal": False,
            "title": "Judaisme 2023",
        },
        {
            "href": "https://judaism.gitlab.io/judaisme-2024/",
            "internal": False,
            "title": "Judaisme 2024",
        },
        {
            "href": "https://judaism.gitlab.io/judaisme",
            "internal": False,
            "title": "Judaïsme",
        },
        {
            "href": "http://www.calj.net/",
            "internal": False,
            "title": "Calj",
        },
        {
            "href": "https://www.hebcal.com/",
            "internal": False,
            "title": "Hebcal",
        },
        {
            "href": "https://judaism.gitlab.io/linkertree/",
            "internal": False,
            "title": "Liens Judaisme",
        },
        {
            "href": "https://linkertree.frama.io/pvergain/",
            "internal": False,
            "title": "Liens pvergain",
        },
    ],
    "heroes": {
        "index": "Floriane Chinsky",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True


copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |wow| image:: /images/wow_64.png
.. |guerrieres| image:: /images/guerrieres_de_la_paix_avatar.png
.. |paix| image:: /images/guerrieres_de_la_paix_avatar.png
.. |floriane| image:: /images/floriane_chinsky_avatar.jpg
.. |hanna| image:: /images/hanna_assouline_g_avatar.png
.. |sefaria| image:: /images/sefaria_avatar.jpg
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
