.. index::
   pair: Atelier ; Impulsion humaniste
   ! Humanisme

.. _humanisme:

==========================================================================
**Humanisme** Atelier impulsion humaniste
==========================================================================

- https://rabbinchinsky.fr/2021/10/24/atelier-impulsion-humaniste/

.. figure:: debut_chaine.png
   :align: center

   https://www.youtube.com/watch?v=rITeosRR8Fo


Atelier impulsion humaniste
=============================

- https://rabbinchinsky.fr/2021/10/24/atelier-impulsion-humaniste/

L’humanisme et le féminisme sont au coeur de mes réflexions depuis toujours.

Après 16 ans de pratique rabbinique, autant de travail interreligieux et
interconvictionnel, et 47 ans de travail personnel, je souhaite partager
ces grandes questions d’une façon renouvelée.

Dans cette perspective, le projet « Atelier impulsion humaniste » est lancé,
et je voulais dés à présent vous en faire part.

Les grands thèmes sur lesquels nous allons travailler sont les suivants,
je les envisage dans un esprit de co-création, vos réactions sont donc
plus que bienvenues.

Re-découvrir nos identités, Questionner nos héritages, Renforcer nos
piliers et nos repères, Partager nos féminismes, Vivre dans nos corps,
Accéder à toute notre puissance de penser, Embrasser le sacré de chaque instant.

Les ateliers commenceront en décembre, à raison d’une fois par mois, le
format sera bientôt communiqué.

Pour être tenus au courant directement par email, remplissez ce petit formulaire.
Les formulations peuvent évoluer, n’hésitez pas à me faire part de votre
sentiment et de vos commentaires sous cet article.

