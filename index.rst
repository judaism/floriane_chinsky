
.. figure:: images/floriane_chinsky.jpg
   :align: center


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://judaism.gitlab.io/floriane_chinsky/rss.xml>`_

.. un·e
.. ❤️💛💚

.. _floriane_chinsky:
.. _chinsky:

=================================================================================================
|floriane|  💚 **Floriane Chinsky** Un judaïsme humaniste, féministe et existentialiste
=================================================================================================

- https://rabbinchinsky.fr/
- https://rabbinchinsky.fr/about/rabbin-floriane-chinsky/
- https://akadem.org/author/floriane-chinsky
- https://fr.wikipedia.org/wiki/Floriane_Chinsky
- https://peertube.iriseden.eu/a/floriane_c/video-channels
- https://invidious.fdn.fr/channel/UCDtvtoXqGz0Niwl2QGM8gyQ (invidious chaine video sans pub de Floriane Chinsky)
- https://www.youtube.com/c/FlorianeChinsky/videos
- https://judaismeenmouvement.org/les-femmes-et-hommes/floriane-chinsky/
- https://libertejuive.wordpress.com
- https://www.youtube.com/c/FlorianeChinsky/videos
- https://www.youtube.com/user/chirhadach (Floriane Chinsky)
- https://www.instagram.com/floriane_chinsky/
- https://www.instagram.com/nelech_surmelin/ (Groupe d’étudiant.e.s partageant 
  les enseignements de @floriane_chinsky en chemin vers un judaïsme égalitaire et inclusif 🍃🌱🌿🪴) 
- https://linktr.ee/florianechinsky
- https://www.sefaria.org/texts |sefaria|
- https://rstockm.github.io/mastowall/?hashtags=mazeldon,jewish,jewdiverse&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=torah&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=chinsky&server=https://framapiaf.org

.. toctree::
   :maxdepth: 3

   bio/bio
   humanisme/humanisme
   livres/livres

.. toctree::
   :maxdepth: 6

   nouvelles/nouvelles
   ressources/ressources
