
==========================================================================
**Annonces dans les media**
==========================================================================

Passages medias
==================

- https://rabbinchinsky.fr/2021/11/03/des-femmes-et-des-dieux-prochaines-rencontres/


- Jeudi 4 novembre 2021 – 18h30 : TV5 monde
- Dimanche 7 novembre 2021 – 10h10 : RFI
- Vendredi 12 novembre 2021 – 8h: France 24
- Lundi 15 novembre 2021 – 18h45: Fréquence protestante (le séminaire RVJ est maintenu)


Séance de dédicace
===================

Dimanche 21 novembre 2021 14h-16h à JEM-Beaugrenelle


Novembre 2021
==============

- :ref:`ravflo_2021_11_04_tv5_monde`
- :ref:`ravflo_2021_11_01_france5`

Octobre 2021
--------------

- :ref:`ravflo_2021_10_27`
- :ref:`annonce_des_femmes_et_des_dieux`


Commentaires
===============

Xavier Perelmuter
---------------------

novembre 3, 2021 à 11:51

Pour la dédicace j’ai déjà acheter l’ouvrage à la librairie du temple.
Puis-je amener l’ouvrage pour le faire dédicacer.
Cordialement
Xavier perelmuter

Rinea
+++++++

novembre 3, 2021 à 12:11

Absolument. Au plaisir de vous y voir…

Azuelos
++++++++++

novembre 3, 2021 à 3:30

J’ai lu votre ouvrage collectif. Passionnant. Je le fais tourner autour
de moi. Par contre pas de possibilité d’un autographe j’habite trop loin de Paris.
Vue la proximité intellectuelle et spirituelle entre vous 3 je me dis
qu’il n’y aurait aucun inconvénient à participer à l’un de vos services
religieux. On serait très à l’aise !

Rinea
+++++++

novembre 4, 2021 à 12:36

Heureuse que cela vous ai plu. Et bien sûr, vous êtes le bienvenu à la synagogue.




