.. index::
   ! Des femmes et des dieux
   pair: Livre ; Des femmes et des dieux

.. _des_femmes_et_des_dieux:
.. _livre_femmesdieux:

==========================================================================
**Des femmes et des dieux**
==========================================================================

- https://www.arenes.fr/livre/des-femmes-et-des-dieux/
- https://fediverse.org/les_arenes
- https://judaismeenmouvement.org/agenda/oneg-du-livre-du-rabbin-chinsky/
- https://judaismeenmouvement.org/actualites/des-femmes-et-des-dieux/


.. figure:: images/floriane_sur_france_5.png
   :align: center

   https://youtu.be/_2knl0vkvRM?t=215

.. toctree::
   :maxdepth: 3

   description/description
   annonces/annonces
