
.. _desc_des_femmes_et_des_dieux:

==========================================================================
Description **Des femmes et des dieux** par les Arènes
==========================================================================

- https://www.arenes.fr/livre/des-femmes-et-des-dieux/


.. figure:: ../images/floriane_sur_france_5.png
   :align: center

   https://youtu.be/_2knl0vkvRM?t=215


Description
=============

Trois femmes ont décidé d’écrire un livre ensemble.

Elles sont rabbin, imame et pasteure et ont abordé tous les sujets qui
leur tenaient à cœur.

Quelle place pour les femmes dans leurs trois religions, marquées par des
siècles de patriarcat ?

Peut-on faire une lecture féministe de la Torah, de la Bible ou du Coran ?

Comment réagir aux représentations souvent dévalorisantes du corps de la femme ?

Comment distinguer ce qui relève du divin et de la tradition ?

Qu’est-ce qui est sacré ?

Elles apportent des éclairages théologiques passionnants et accessibles
à tous. Elles s’appuient sur leur histoire, confrontent leurs parcours,
réfléchissent et racontent les obstacles qu’elles ont surmontés, dans
un climat d’écoute et de concorde qui irradie tout le livre.

Des femmes et des dieux est le fruit de leur rencontre.

C’est un livre plein d’espoir qui nous aide à saisir l’essentiel.



Les auteurs
================

Kahina Bahloul
---------------

Kahina Bahloul est une islamologue franco-algérienne.
Elle est la co-fondatrice du projet d’association cultuelle La Mosquée Fatima,
qui promeut un islam libéral.


Floriane Chinsky
-----------------

Floriane Chinsky est rabbin depuis 2005, ainsi que docteure en droit et
formatrice en Écoute mutuelle. Elle a accompagné des communautés à Jérusalem
et à Bruxelles avant de venir à Paris. Elle exerce actuellement à
Judaïsme en mouvement (JEM), rue Surmelin, dans le XXe arrondissement.

Emmanuelle Seyboldt
-----------------------

Emmanuelle Seyboldt est pasteure et présidente du conseil national de
l’Église protestante unie de France.
