
.. _annonce_des_femmes_et_des_dieux:

=====================================================================================
Vendredi 22 octobre 2021 **Sortie de mon nouveau livre: Des Femmes et des dieux**
=====================================================================================

- https://www.arenes.fr/livre/des-femmes-et-des-dieux/
- https://rabbinchinsky.fr/2021/10/22/sortie-de-mon-nouveau-livre-des-femmes-et-des-dieux/


Présentation par Floriane Chinsky
===================================

J’ai le plaisir de vous annoncer la sortie de mon nouveau livre,
Des femmes et des dieux, en collaboration avec Kahina Bahloul et
Emmanuelle Seybolt, le 28 octobre 2021, aux éditions Les Arènes, 2021.

Chabbat Chalom à toutes et tous.

.. figure:: des_femmes_et_des_dieux.jpg
   :align: center




Des femmes et des dieux Kahina Bahloul, Floriane Chinsky, Emmanuelle Seyboldt, par les Arènes
==============================================================================================

- https://www.arenes.fr/livre/des-femmes-et-des-dieux/

Trois femmes ont décidé d’écrire un livre ensemble.
Elles sont rabbin, imame et pasteure et ont abordé tous les sujets qui
leur tenaient à cœur.

Quelle place pour les femmes dans leurs trois religions, marquées par
des siècles de patriarcat ?

Peut-on faire une lecture féministe de la Torah, de la Bible ou du Coran ?

Comment réagir aux représentations souvent dévalorisantes du corps de la
femme ? Comment distinguer ce qui relève du divin et de la tradition ?
Qu’est-ce qui est sacré ?

Elles apportent des éclairages théologiques passionnants et accessibles
à tous. Elles s’appuient sur leur histoire, confrontent leurs parcours,
réfléchissent et racontent les obstacles qu’elles ont surmontés, dans
un climat d’écoute et de concorde qui irradie tout le livre.

Des femmes et des dieux est le fruit de leur rencontre. C’est un livre
plein d’espoir qui nous aide à saisir l’essentiel.

Kahina Bahloul est une islamologue franco-algérienne.
Elle est la co-fondatrice du projet d’association cultuelle La Mosquée
Fatima, qui promeut un islam libéral.

Floriane Chinsky est rabbin depuis 2005, ainsi que docteure en droit et
formatrice en Écoute mutuelle. Elle a accompagné des communautés à Jérusalem
et à Bruxelles avant de venir à Paris.
Elle exerce actuellement à Judaïsme en mouvement (JEM), rue Surmelin,
dans le XXe arrondissement.

Emmanuelle Seyboldt est pasteure et présidente du conseil national de
l’Église protestante unie de France.


Des femmes et des dieux
==========================

- https://judaismeenmouvement.org/actualites/des-femmes-et-des-dieux/


Sorti aujourd’hui en librairie, découvrez le premier livre écrit par
trois femmes responsables d’une communauté juive, musulmane ou chrétienne.

Ensemble, le rabbin Floriane Chinsky, l’imame Kahina Bahloul et la pasteure
Emmanuelle Seyboldt ont voulu apporter leur regard sur les textes sacrés.

Libres d’être femme et cheffe de communauté
---------------------------------------------

Kahina Bahloul (imame), Floriane Chinsky (rabbin) et Emmanuelle Seyboldt (pasteure)
ouvrent ce livre par le récit de leur jeunesse et de leur décision de
devenir responsable d’une communauté religieuse : musulmane, juive ou chrétienne.

Comment trouver sa place dans un monde et des traditions domi- nés par les hommes ?

Emmanuelle montre le chemin ouvert depuis plusieurs siècles par l’Église
protestante ;

Floriane retrace son parcours rabbinique et raconte l’état
actuel des mouvements dans le judaïsme et leur relation à la place des femmes ;

Kahina, quant à elle, pose les premières pierres d’un islam libéral.


Leadership au féminin
------------------------

Comme dans de nombreux champs, le combat pour l’égalité entre hommes et
femmes est loin d’être gagné dans le domaine religieux.

Les trois autrices questionnent sans tabou, déconstruisent les barrières
mentales qui empêcheraient les femmes d’accéder aux mêmes responsabilités
que les hommes.

Si certains textes disent le contraire, il faut aller chercher aussi
ceux que l’on cache et qui mettent en avant les femmes.

Le corps des femmes
--------------------

Qu’elle soit juive, chrétienne ou musulmane, toutes les religions ou
traditions ont « un problème » avec le corps des femmes (tentation,
impureté, péché, etc.).

Les autrices parlent sans fard de cette question, en citant les textes
de la Torah, de la Bible et du Coran, mais aussi en partageant leur
expérience et de nombreuses situations où il a été question du corps
de la femme.

Le lecteur se rendra compte que Dieu est finalement très éloigné de
ces questions, qui ne sont que le fruit de nombreux siècles de domination
masculine et de patriarcat.


“C’était une manière pour nous de découvrir ce qu’on savait théoriquement :
les richesses, les traditions, ne sont pas menaçantes pour moi.

Nous sommes très différentes les unes des autres, mais il n’y a aucun
danger à être les unes avec les autres, et les uns avec les autres”

Emmanuelle Seyboldt

