
.. _ravflo_2021_10_27:

===============================================================================================================================
Mercredi 27 octobre **'Des femmes et des dieux' de Floriane Chinsky, Kahina Bahloul et Emmanuelle Seyboldt** sur France Inter
===============================================================================================================================

- https://www.arenes.fr/livre/des-femmes-et-des-dieux/
- https://www.franceinter.fr/emissions/l-invite-de-8h20-le-grand-entretien/l-invite-de-8h20-le-grand-entretien-du-mercredi-27-octobre-2021
- https://fediverse.org/franceinter/status/1453030731748462600?s=20

- https://youtu.be/a3CcD99DYqI?t=120

.. figure:: images/studio.png
   :align: center

   https://youtu.be/a3CcD99DYqI?t=1276


Passages
==========


Sortir de ma zone de confort
--------------------------------

Sortir de ma zone de confort..on a passé 1 semaine ensemble à creuser les
différents points de vue, essayer de voir où sont nos points de rencontres..
solidarité autour de tout ce que l'on vit au quotidien.

- https://youtu.be/a3CcD99DYqI?t=120


Mon choix a été d'étudier à Jérusalem
-----------------------------------------

Mon choix a été d'étudier à Jérusalem car il me fallait une légitimité
et un accès direct aux sources.. plus on sait des choses et plus on sait
ce qui est possible.
**Je ne voulais pas arriver à la fin de ma vie avant d'avoir essayé.**

- https://youtu.be/a3CcD99DYqI?t=317


Sur la place des femmes dans les religions
--------------------------------------------

Sa place dépend de la société globale dans laquelle elle vit.

- https://youtu.be/a3CcD99DYqI?t=450

C'est important que chacune, chacun prenne ses responsabilités
--------------------------------------------------------------

C'est important que chacune, chacun prenne ses responsabilités dans les
cercles où l'on est

- https://youtu.be/a3CcD99DYqI?t=558

Ce sujet ne doit pas être tabou
------------------------------------

- https://youtu.be/a3CcD99DYqI?t=699

Est-ce qu'on est capable d'avoir un plaisir responsable ?
-------------------------------------------------------------

- https://youtu.be/a3CcD99DYqI?t=868

Limiter la dépendance et favoriser l'autonomie
--------------------------------------------------

Limiter la dépendance et favoriser l'autonomie

- https://youtu.be/a3CcD99DYqI?t=1030


Lecture moderne des textes
-----------------------------

Dépasser un certain type de préjugés qui ne correspond pas à ce que j'ai
vécu.
Quand on parle de Dieu, on parle de quelque chose qu'on ne peut pas nommer.
On ne peut pas nommer Dieu dans le judaïsme. C'est désamorcer le risque
d'une croyance stupide.

- https://youtu.be/a3CcD99DYqI?t=1276
