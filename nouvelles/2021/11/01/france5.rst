
.. _ravflo_2021_11_01_france5:

================================================================================================================================
Lundi 1er novembre 2021 **Pasteure, imame, rabbin : des femmes et des dieux - F. Chinsky, K. Bahloul, E. Seyboldt** sur France5
================================================================================================================================


.. figure:: floriane_chinsky.png
   :align: center

   https://youtu.be/_2knl0vkvRM?t=9


Passages
=========

Reprendre le pouvoir et se l'approprier
----------------------------------------

5 femmes rabins en France.

- https://youtu.be/_2knl0vkvRM?t=276


Le premier humain est créé autant femme que homme
---------------------------------------------------

- https://youtu.be/_2knl0vkvRM?t=419


Talmud de Babylone
---------------------

Il ne faut passer à côté d'un plaisir dans ce monde.
Pourquoi le sexe est-il devenu tabou ? et à qui profite le crime ?.

- https://youtu.be/_2knl0vkvRM?t=507
