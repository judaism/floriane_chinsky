
.. _ravflo_2021_11_04_tv5_monde:

==============================================================================================
Jeudi 4 novembre 2021 **Les religions peuvent-elles être féministes ?** sur TV5 monde direct
==============================================================================================

- https://www.youtube.com/watch?v=WPuj4znk2JY

.. figure:: images/plateau.png
   :align: center

   https://youtu.be/WPuj4znk2JY?t=22


Passages
===========

Par le présentateur
---------------------

- https://youtu.be/WPuj4znk2JY?t=30


On a besoin de se battre pour la laïcite, l'écoute
------------------------------------------------------

.. figure:: images/floriane_chinsky_1.png
   :align: center


On a besoin de se battre pour la laïcite, l'écoute.
C'est pas mal de faire les choses autrement.

- https://youtu.be/WPuj4znk2JY?t=53

Plus d'égalité, plus de justice
--------------------------------------

Plus d'égalité, plus de justice et que chacun, chacune ait sa place.
Il faut être empathique avec la société de l'époque.

..

Défense par les rabbins du droit des femmes à être éduquées...
Amour des rabbins pour leurs femmes, pour leurs filles même si ça
reste très importannt aujourd'hui que des femmes aujourd'hui fassent leurs
propres commentaires des textes.

- https://youtu.be/WPuj4znk2JY?t=245


Je suis pour la diversité, mais pourquoi toujours mettre l'accent sur la beauté ?
--------------------------------------------------------------------------------------

- https://youtu.be/WPuj4znk2JY?t=585

Je suis pour la diversité, mais pourquoi toujours mettre l'accent sur la beauté ?
toujours l'accent sur le corps des femmes.


C'est le lieu fondamental du sacré
----------------------------------------

Le shabbat il faut bien manger parce que si on mange bien un jour par
semaine on a une chance de ne pas devenir un fou extrêmiste :) et de
s'occuper de son bien-être et de celui des autres et de changer le monde
d'une façon positive.

- https://youtu.be/WPuj4znk2JY?t=786


.. figure:: images/floriane_chinsky_2.png
   :align: center

   https://youtu.be/WPuj4znk2JY?t=827
