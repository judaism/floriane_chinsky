
================================================================
L’étude ce lundi 22 novembre 2021, avancer sur le long terme
================================================================

Voici les éléments écrits que nous utiliserons lundi soir, accueil à 18h45,
dernier accès 19h05.

Tout sera traduit et expliqué, donc, pas de panique !

Le cours est ouvert à toutes et tous à partir de 11 ans. si vous venez
avec des jeunes, prévenez-moi et connectez-vous un peu à l'avance si
possible. Lien d'inscription ici (uniquement pour les nouveaux inscrits):

https://framaforms.org/inscription-venir-au-judaisme-1591048937

J'enverrai le lien zoom lundi à 18h aux personnes inscrites avant 18h.


Si possible, je vous invite à préparer un texte qui a joué un grand rôle
dans votre vie.

Chant 1
=========

Ani véata néchané et haolam, ani véata az yavoou kvar koulam amrou et
zé kodem léfanaï lo méchané ani véata néchané et haolam
(écoutez ici: https://www.youtube.com/watch?v=gP6PS-poyMg)

Toi et moi nous changerons le monde, toi et moi puis tout le monde viendra,
d’autres ont dit cela avant moi mais peu importe, toi et moi nous changerons
le monde.


Etude et discussion sur l'étude, le texte sera envoyé aux personnes inscrites
================================================================================

Chant 2 – extrait du Ps. 133  - הִנֵה מַה-טּוֹב וּמַה נָעִים שֶבֶת אַחִים גַּם יָחַד

hiné ma tov ouma naim chévat aHim gam yaHad -
Que c’est bon et agréable un groupe d’adelphes ensemble

En vous inscrivant sur ce lien, vous recevrez le lien de ce lundi ainsi
que les lien zoom des cours jusqu’à janvier :

https://framaforms.org/inscription-venir-au-judaisme-1591048937
Si vous vous êtes déjà inscrits depuis septembre au cours du lundi, pas
besoin de le remplir à nouveau.
