
.. _dedicaces_2021_11_22:

==========================================================================
Samedi 27 novembre 2021 Dédicace: Oneg du livre, avec Floriane Chinsky
==========================================================================


akadem
=========


.. figure:: coup_de_coeur.png
   :align: center

   https://akadem.org/scopefiche.php?ID=133016


Précédé d’une Havdala, le prochain "Oneg" du livre met à l’honneur le
dernier livre du rabbin Floriane Chinsky **Des femmes et des dieux** (Les Arènes),
co-écrit avec l’imame Kahina Bahloul et la pasteure Emmanuelle Seybolt
présentes également pour la séance de dédicaces.



Vous avez des questions ? Réponses ce samedi 18h30, rencontre-dédicace autour du livre
========================================================================================

- https://rabbinchinsky.fr/2021/11/26/vous-avez-des-questions-reponses-ce-samedi-18h30-rencontre-dedicace-autour-du-livre/


.. figure:: des_femmes_et_des_dieux.png
   :align: center

Encore quelques minutes avant chabbat pour poser à l’avance les questions
auxquelles vous voudrez des réponses samedi soir….

avec Kahina Bahloul et Emmanuelle Seybolt.

Samedi 27 Novembre à partir de 18h30, Précédé d’une Havdala

Jem Surmelin, `24 rue Surmelin, 75020 Paris <https://what3words.com/brider.racontons.verbe>`_, `Métro Pelleport/Gambetta <https://what3words.com/rigoler.orienter.saler>`_

.. warning:: Rappel Présentation du pass sanitaire à l’entrée,
   Port du masque obligatoire pendant la soirée
