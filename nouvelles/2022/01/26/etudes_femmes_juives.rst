.. index::
   pair: Etudes; Mercredi 26 janvier 2022

.. _etudes_2022_01_26:

=============================================================================================================
Mercredi 26 janvier 2022 Journée internationale d’étude féministe ! réflexions en ces temps pleins de défis
=============================================================================================================

- https://rabbinchinsky.fr/2021/11/24/journee-internationale-etude-feministe/

Bonsoir à vous !

En cette veille de Hanouka, je souhaite vous parler… De la journée
internationale d’étude des femmes, à laquelle je participe cette année.

Nous aurons une rencontre par Zoom avec des femmes du monde entier.
L’étude sera en français. Mon angle d’approche sera:

Fortes, Fières, Féministes… et juives

Être ce que nous sommes aujourd’hui est loin d’être facile. En tant que
juives, en tant que femmes, nos forces sont souvent mis à mal.

En quoi notre judéité peut elle nous aider à exister et à faire rayonner
le meilleur de nous même ?

Nous examinerons différents outils, des pratiques, des objets, des textes
d’études, des textes de médiation, pour nous renforcer personnellement
et pour contribuer à un changement social en faveur des femmes juives
de notre temps.

Le cours prendra en compte les questions spécifiques de celles qui auront
rempli le petit document suivant: https://framaforms.org/sondage-journee-detude-1637778459

Je vous transmettrai les informations d’inscription bientôt.

Bonne soirée à toutes et tous,
