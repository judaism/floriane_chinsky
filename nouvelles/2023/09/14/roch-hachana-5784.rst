.. index::
   pair: Rosh Hachana ; 5784

.. _rosh_hachana_5784:

==========================================================================
2023-09-15 **Dans 24h précisément, Roch hachana commence!**
==========================================================================

- https://rabbinchinsky.fr/2023/09/14/roch-hachana-5784/


Bonne année 5784! Roch hachana est à SURMELIN (Kipour sera à l’Espace Reuilly)


N’hésitez pas à vous habiller de blanc et de sourire, à préparer pomme et miel…

Demain soir, vendredi 18h45, samedi 10h = Roch hachana 1, dans la solennité de la fête

Samedi 18h45, dimanche 10h = Roch hachana 2, pour poursuivre avec convivialité

Au programme, de merveilleux poèmes inspirant le renouveau de l’année,
AHot kétana (la petite soeur), Ountané tokef (donnez de la puissance),
et bien d’autres.

Des lectures d’histoires avec l’épreuve de Hagar et celle de Sarah, la
ténacité de Hannah et l’amour indéfectible de Rachel.

Samedi, roch hachana + chabat, dimanche roch hachana plein avec la sonnerie
du Chofar.

En primeur pour cette année, deux petites aides pour les offices:
soir-de-roch-hachana matin-de-roch-hachana

Auxquelles s’ajoutent les ressources traditionnelles:

- Seder de Roch hachana  à `télécharger seder-roch-hachana <https://poursurmelin.files.wordpress.com/2020/09/seder-roch-hachana-feuille-9.docx>`_ (https://poursurmelin.files.wordpress.com/2020/09/seder-roch-hachana-feuille-9.docx)
- `Playlist autour des prières sur youtube Prières de Tichri <https://www.youtube.com/watch?v=3AXkC5WlbLA&list=PLnHlXjFx9rOQG_pJnHun39jSqKB1Zd-Jj>`_ (https://www.youtube.com/watch?v=3AXkC5WlbLA&list=PLnHlXjFx9rOQG_pJnHun39jSqKB1Zd-Jj)
- `Moment de pensée et de prière, Boker-Tov/seliHot <https://www.youtube.com/watch?v=GjKXxwJiYI4&list=PLnHlXjFx9rOTgSNpxmZG5xhR6i-PDBFp7>`_ (https://www.youtube.com/watch?v=GjKXxwJiYI4&list=PLnHlXjFx9rOTgSNpxmZG5xhR6i-PDBFp7)

Drachot des années passées par écrit
============================================

- `Une seule solution, la transgression (5781) <https://rabbinchinsky.fr/2020/09/27/discours-kipour/>` (https://rabbinchinsky.fr/2020/09/27/discours-kipour/)
- `Une seule solution, la corruption (5781) <https://rabbinchinsky.fr/2020/09/18/corrompre-dieu/>`_ (https://rabbinchinsky.fr/2020/09/18/corrompre-dieu/)
- `Roch hachana, le jour du NON-jugement (5780) <https://rabbinchinsky.fr/2019/10/06/roch-hachana-jugement/>`_ (https://rabbinchinsky.fr/2019/10/06/roch-hachana-jugement/)
- `Cessons de croire… Réfléchissons! (5779) <https://rabbinchinsky.fr/2018/09/11/roch-hachana-croire/>`_ (https://rabbinchinsky.fr/2018/09/11/roch-hachana-croire/)
- `La Tsédaka, Changer le monde (5778) <https://rabbinchinsky.fr/2017/09/25/tsedaka-dracha-roch-hachana/>`_ (https://rabbinchinsky.fr/2017/09/25/tsedaka-dracha-roch-hachana/)
- `Nous sommes le chant du monde… (5777) <https://rabbinchinsky.fr/2016/10/11/roch-hachana-kipour-5777/>`_ (https://rabbinchinsky.fr/2016/10/11/roch-hachana-kipour-5777/)
- `Pour commencer, ouvrir les yeux… (5776) <https://rabbinchinsky.fr/2015/09/22/rosh-hashana-teshuva/>`_ (https://rabbinchinsky.fr/2015/09/22/rosh-hashana-teshuva/)

Textes divers
=================

- `Développement personne ou révolution sociale <https://rabbinchinsky.fr/2020/09/23/developpement-personnel-ou-revolution-sociale-du-9-av-a-yom-kipour/>`_ (https://rabbinchinsky.fr/2020/09/23/developpement-personnel-ou-revolution-sociale-du-9-av-a-yom-kipour/),
  du `9 av à Yom Kipour <https://rabbinchinsky.fr/2020/09/23/developpement-personnel-ou-revolution-sociale-du-9-av-a-yom-kipour/>`_ (https://rabbinchinsky.fr/2020/09/23/developpement-personnel-ou-revolution-sociale-du-9-av-a-yom-kipour/)
- `Le sens des offices de Roch hachana <https://rabbinchinsky.fr/2019/09/29/offices-roch-hachana/>`_
- `Embrasser le passé, Chérir l’avenir – pédagogie de Roch Hachana <https://rabbinchinsky.fr/2015/06/26/pedagogie-roch-hachana/>`_ (https://rabbinchinsky.fr/2015/06/26/pedagogie-roch-hachana/)

Sources diverses
==================

- https://rabbinchinsky.fr/2021/08/31/meditation-vers-le-meilleur-de-nous-memes-preparation-a-tichri/
- https://rabbinchinsky.fr/2020/09/21/boker-tov-de-ce-matin-techouva-et-maimonide/
- https://rabbinchinsky.fr/2020/09/22/techouva-et-maimonide-deux-autres-extraits-pour-ce-matin/
- https://rabbinchinsky.fr/2021/08/30/cours-sur-les-fetes-de-tichri-sources-pour-ce-soir/
