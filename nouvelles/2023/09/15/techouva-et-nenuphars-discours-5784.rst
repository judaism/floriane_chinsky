.. index::
   pair: Techouva ; 5784
   pair: Tshuvah; Techouva

.. _techouva_5784:

==========================================================================
2023-09-15 **La téchouva (Tshuvah) et les nénuphars! Chana tova 5784**
==========================================================================

- https://rabbinchinsky.fr/2023/09/15/techouva-et-nenuphars-discours-5784/
- https://bonpote.com/climat-point-de-bascule-et-optimisme/

Bonne année 5784! Roch hachana est à SURMELIN dans une heure (Kipour
sera à l’Espace Reuilly)

En avant première, des éléments de mon discours de ce soir (mais il y aura
des surprises…)

Je sais que vous connaissez cette histoire.

Il y a un déluge, une inondation, une catastrophe naturelle, comme en Libye
où s’est déversée en quelques heures la quantité de pluie habituellement
tombée en une année.

La radio informe : quittez vos maisons ! remontez dans les terres ! mais
certains se disent : nous allons prier, dieu nous aidera.
L’eau monte, elle emporte les voitures envahit les rez-de chaussée, les
entêtés montent d’un étage mais ne partent pas. « nous prierons, dieu
nous aidera ».
L’eau continue, les fenêtres sont brisées les secours prennent des risques
inconsidérés, « montez dans les canots ». « pas besoin, dieu nous aidera ».
Ils sont maintenant sur le toit, ils prient, en pleine concentration, le
vent se déchaine, l’hélicoptère des secours se surpasse, « montez enfin !
montez ! », « nous avons notre conscience avec nous, nous avons fait
téchouva, nous croyons dans le secours divin ».
Quelques minutes après, à la porte du paradis : « que faites vous là ? »
« dieu ne nous a pas sauvés, pourtant nous avons prié de toutes nos forces ! »
« mais je vous ai envoyé la radio et vous n’avez pas écouté, des canots
et vous n’avez pas écouté, un hélico et vous n’avez pas écouté ! »

Les histoires que nous nous racontons ne nous sauvent pas de la réalité.

Selon moi, cette histoire a une suite : les fervents n’entrent pas au paradis.
« mais nous avons eu la foi ! » « mais vous avez mis en danger les secours,
vous avez répandu des théories d’irresponsabilité, votre place n’est pas
ici, mais dans les brulantes flammes de l’enfer ! »

Le paradis et l’enfer ne sont pas des idées juives. Cette histoire, au
contraire, est typique. Dans la droite ligne de notre tradition, elle
enseigne : ce sont nos actions qui comptent, et nous sommes responsables
de comprendre la réalité, et agir avec solidarité et intelligence.
Roch hachana, est justement là pour donner une tête, roch, une direction,
à l’année.

Vous connaissez également le tout début de la torah écrite :
Au commencement (béréchit), les Forces créa les cieux et la terre.

Plus important encore, vous connaissez le premier commentaire de la
torah écrite, rachi sur ce verset : Il aurait fallu commencer la torah
non par l’histoire du monde, mais par l’histoire de notre responsabilité.
Rachi dit : il aurait fallu commencer par « ce mois est pour vous le
commencement (réchit) des mois », celui où la fête de la liberté a lieu.
Il parle bien sûr du mois de … Nissan.

Mais cela s’applique à nous aussi, en Tichri.
**Nos actes sont plus importants que les histoires que nous nous racontons**.

Les histoires sont là pour soutenir la justice et la paix dans nos actes.

Rabbi Chimon dit dans les pirké avot : ולא המדרש הוא העיקר, אלא המעשה, ce
n’est pas l’histoire le principal, mais l’acte.

A roch hachana, nous racontons une belle histoire : il suffit de faire
téchouva téfila et tsédaka et tout ira bien.
**Cette histoire a du sens si elle soutient les actes appropriés**.
Sinon, elle n’est que futilité et mots perdus dans le vent comme le dit
l’Ecclésiaste, que nous lirons à Soukot.

Téchouva, téfila, tsédaka
============================

En principe, c’est simple : on prend conscience d’une erreur, on l’admet
verbalement et on règle la question avec la personne concernée, puis on
le règle avec soi-même, et enfin on donne des ressources au collectif,
et c’est fini, on repart d’un nouveau pied, on ouvre une nouvelle page.

Pour ne pas s’alourdir, on refait l’inventaire tous les ans.

Dans la randonnée de la vie, on veut voyager léger, on vérifie ce qui est
pesant ou inutile, on se déleste.
Et ensuite, on est mobiles, adaptatifs, on a la force de préserver l’essentiel,
l’eau, la nourriture, les vêtements qui nous protègent, les cartes qui
nous orientent.

Donc, en principe, c’est simple. On se raconte l’histoire qui nous aide à agir.

La période de Tichri nous aide à prendre conscience de la pesanteur de
nos comportements.
Les dix jours de téchouva nous donnent le temps d’examiner les changements
que nous voulons réaliser et de corriger nos erreurs.
La téfila de Kipour, l’introspection de ce jour nous permet de régler nos
comptes avec nous-mêmes et avec la transcendance.
Les dons à la synagogue ou aux œuvres, la tsédaka, la solidarité, nous
permet de retrouver un rapport de générosité au monde. Et c’est tout.

Ça marche très bien dans les petits groupes, ou chacun, chacune, peut se
parler et s’interpeler directement. Ca marche très bien quand on cesse
de se raconter des histoires sur les autres, et qu’on se tourne vers eux
dans le réel.
**Ca marche… et il faut le faire réellement. FAISONS-LE RÉELLEMENT!**

Ceci, c’est la partie facile.
Mais comment faire lorsque l’autre ne peut pas nous répondre ?

A roch hachana, yom hadin, jour du jugement, c’est l’avenir de la terre
qui est en jeu.
**Peut-elle demander justice ou réparation ?
Sommes-nous capables de l’entendre ?
Ou bien sommes nous paralysés par le lachon hara, le mauvais langage qui
nous entoure ?**

Un article de l’université de Cambridge en 2020 analyse 12 discours pervers
qui sont de l’ordre du lachon hara vis-à-vis de la terre. Les voici :

« c’est trop tard, on ne peut rien faire, c’est la faute des autres,
nous on en fait assez, les autres vont profiter de nos efforts, la
technologie va nous sauver, grandes déclarations sans actes, ça va
s’arranger tout seul, il faut laisser les gens comprendre par eux-mêmes,
les gens vont s’y opposer, ce serait abandonner les pays pauvres, ce
serait pénaliser les personnes en difficulté. »

**Comme les personnes de notre première histoires, nous pouvons trouver
tous les prétextes du monde, mais la réalité se moque des excuses et
des auto-justifications**.

Le climat est un exemple fondamental dont tout le reste dépend.

Et nous pouvons appliquer nos réflexion à tous les autres domaines de nos vies.

Aujourd’hui, nous devons nous mettre en chemin.
La question de départ est : « sur quoi est-il urgent d’agir dans ma vie cette année.
Le point d’arrivée est : « Mon programme d’action est en place, je connais
toutes les étapes ».
Le timing est : identifier les points de changement aujourd’hui et demain
(venez aux offices), pleurer nos échecs passés avec le jour du Jeune de
Guédalia lundi, mettre en place notre plan d’action pendant les dix jours
qui nous séparent de kipour (venez aux seliHot), ancrer notre volonté à
Kipour, marquer notre confiance à Soukot, fêter la finalisation du
programme à SimHat Torah, ici-même, dans un mois.

**En ce jour de roch hachana, nous agissons dans nos vies, et dans la vie
de notre communauté, et dans la vie de notre planète**.

Parmi les plans d’action pour préserver une terre vivable pour les humains,
voici une liste de 26 éléments, que vous pourrez consulter à roch hachana
et à kipour et sur mon site, parlons-en pour modifier nos pratiques.

Pourquoi avons-nous manqué à ce point d’efficacité ? Le pardon chrétien,
le pardon juif, la soi-disant loi du talion œil pour œil, la michna…
Je développerai peut-être en vidéo.

Mon père disait que lorsqu’on trouve des solutions, on s’inspire mutuellement,
et qu’on peut faire changer le monde.
Je ne sais pas si cela suffit.
Mais sans aucun doute, depuis 3000 ans, notre tradition est une immense
caisse de résonance pour faire changer le monde. Nous pouvons, ici-même,
expérimenter cette caisse de résonance en incarnant l’équation du nénuphar.

Vous connaissez peut-être cette énigme : Chaque jour, pour chaque nénuphar
existant, un deuxième nait. Au bout de 30 jours, l’étang en est recouvert.
A quel jour l’étang était-il à moitié plein ?
Mais notre question est différente : comment rendre nos actions aussi
fructueuses que ces nénuphars ?

Mais surtout, que nous apprend cette histoire, et comment pouvons-nous
la rendre réelle, la transformer en acte… Car l’acte est l’essentiel.

Bravo, nous sommes passé.es de l’histoire aux actes, il ne reste plus
qu’à décider de l’action que nous mettons en œuvre, et à la faire
résonner, à pleine voix, nous commencerons à SimHat Torah.


Ressources complémentaires
============================

- https://www.nationalgeographic.fr/environnement/planet-possible/26-facons-de-reduire-son-impact-environnemental
- https://terreetavenir.com/coment-choisir-une-banque-responsable/
- https://bonpote.com/climat-les-12-excuses-de-linaction-et-comment-y-repondre/
- https://fr.wikipedia.org/wiki/Erica_Chenoweth (Au sein de la communauté des chercheurs, elle est connue pour son travail sur les mouvements de résistance civile non-violents)
- https://www.cambridge.org/core/services/aop-cambridge-core/content/view/7B11B722E3E3454BB6212378E32985A7/S2059479820000137a.pdf/discourses_of_climate_delay.pdf
- https://bonpote.com/climat-point-de-bascule-et-optimisme/


Complément (NDLR)
------------------------------

- https://bonpote.com/les-cartes-des-12-discours-de-linaction-climatique/
- https://bonpote.com/10-actions-simples-pour-devenir-ecolo/
- https://bonpote.com/10-erreurs-de-communication-sur-le-climat-a-rectifier-durgence/
- https://bonpote.com/infographie-10-bonnes-pratiques-de-communication-sur-le-climat/
- https://bonpote.com/les-cartes-des-12-discours-de-linaction-climatique/
- https://www.lemonde.fr/les-decodeurs/article/2023/05/08/le-guide-critique-des-arguments-et-intox-climatosceptiques_6172472_4355770.html
- https://fr.wikipedia.org/wiki/Techouva
