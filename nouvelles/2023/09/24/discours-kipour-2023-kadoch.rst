.. index::
   ! Kadoch

.. floriane_chinsky_2023_09_24:

=============================================================================================
2023-09-24 Discours Kipour 2023 /Kadoch - gestion de l’espace, gestation de l’espèce
=============================================================================================

- https://rabbinchinsky.fr/2023/09/24/discours-kipour-2023-kadoch/


Discours Kipour 2023 /Kadoch - gestion de l’espace, gestation de l’espèce
septembre 24, 2023 Rinea 1 Cycle du calendrier, Kipour, mot du rabbin, tichri

Introduction
==============

Que feriez-vous, là, tout de suite, si vous aviez une baguette magique ?

Attention, voir ses vœux se réaliser est parfois dangereux.

Vous connaissez l’histoire populaire de ce couple qui rencontre une fée
qui leur propose trois vœux.
La femme dit immédiatement : je veux du boudin. Le mari, fâché qu’elle
ait gâché un vœu s’énerve : que ton boudin se colle sur ton nez !
Manque de chance, cette phrase constitue elle-même un deuxième vœu, qui
se réalise immédiatement.
Tous deux se mettent à gémir : ah non ! si seulement le boudin pouvait
disparaître ! et le troisième vœu se réalise.
Retour à la case départ.

Voilà ce qui se produit quand on n’a aucun recul sur ses propres désirs.

Donc, imaginons que vous ayez une baguette magique. Quel vœu feriez-vous ?
Dites-le à votre voisin.e (vous avez le droit d’être fantaisistes).
On peut avoir quelques exemples ? C’est bien. Je vous ai peut-être pris
au dépourvu en vous posant cette question à brûle pourpoint.
On refera l’exercice demain soir, vous avez 25 heures pour approfondir
votre réflexion.

Mais… Suffit-il d’exprimer nos désirs pour qu’ils se réalisent ?
Eh bien… Peut-être. Par exemple si nous n’osons pas rêver, oser rêver
est la pierre angulaire du changement.
Rêver en soi est déjà un changement qui va modifier la réalité.
Peut-être mon désir, que j’ai enfin identifié, va se réaliser maintenant
que je cesse de le fuir.

Autre exemple, si nous n’osons pas exprimer notre désir. Peut-être que
ce manque d’expression est le seul frein à sa réalisation, c’est possible.
Dans ce cas, exprimer mon désir est la seule chose à faire pour qu’il
se réalise.
Parfois, il suffit de formuler nos désirs pour qu’ils se réalisent, ou
en tout cas qu’ils commencent à se réaliser. Parfois dépasser les limites
que nous nous imposons à nous-mêmes est la clef de tout le reste.

Nous allons dire beaucoup de choses au cours de ce voyage de Kipour.
Mais au fond, nous ne disons qu’une seule chose : Nous avons des désirs
pour l’année à venir, nous voulons vivre vraiment, pleinement, en liberté.
Nous voulons trouver un vrai chemin d’action pour que tous les êtres
vivants puissent vivre leur meilleure vie.

Nous disons : **Itgadal véitkadach chémé rabba : que le grand nom, celui
de la justice et de la liberté, s’agrandisse**.

Nous disons : **anou féoulatéHa véata yotsrénou Nous sommes ton action
et tu es notre créateur, qui signifie : nous sommes fait.es de justice
et de respect et nous sommes capables de tout pour cela**.

C’est une partie du pouvoir magique de Yom Kipour, nous passons du temps
à dire notre désir de vivre.

Suffit-il donc d’exprimer nos désirs pour qu’ils se réalisent ?
Parfois, non. Certains désirs ne peuvent pas se réaliser dans cette
réalité.
**Nos corps sont éphémères et nous n’y pouvons rien**.
Les autres humains agissent à leur guise et cela leur appartient.
L’histoire humaine a créé des sociétés dont les limites s’imposent en
partie à nous. Ces limites rendent certains de nos désirs impossibles.
Mais cela ne nous empêche pas de les nommer.
Nous avons le droit peut-être parfois le devoir, de désirer l’impossible
et le droit d’être entendu.es.
Nos proches, peut-être, peuvent nous comprendre, et notre tristesse,
peut-être, en sera allégée.
Ou sinon, nous pouvons imaginer qu’une force suprême nous connait mieux
encore que nous-mêmes, connait et reconnait et comprend tous les recoins
et toutes les méandres de nos cœurs, de nos trippes, de nos pensées.
Cela aussi nous le visualisons en chantant nos textes de Kipour.

Nous disons : **adonaï adonaï el raHoum véHanoun qui signifie :Eternel, Eternel,
force accueillante et bienveillante, au-delà de la colère grande par ta
bonté et ta fidélité, qui supporte l’erreur destructrice et qui dépasse…**

Nous sommes des magiciens et des magiciennes.
Nous déclarons ce jour « sacré », et il devient « sacré ».
Nous nous déclarons « sacré.es » et nous devenons nous-mêmes « sacré.es ».
Attention à ce que nous dirons en ce jour. En ce jour magique de Kipour,
tout ce que nous avons dit par le passé peut-être effacé, tout ce que
nous voudrons dire pourra se réaliser.
Les anciens vœux sont effacés par le kol nidré, Nous pouvons en faire de nouveaux.

Et si nous voulons que nos vies changent, elles changeront.
A condition que nous soyons assez libres pour oser rêver de ce que nous
voulons. Que nous soyons assez conscient.es pour identifier ce qui nous
entrave. Et si nos rêves sont objectivement impossibles, à nous d’avoir
le courage d’en rêver d’autres, ambitieux d’une autre façon.

Quelles clefs le judaïsme nous propose-t-il sur ce chemin ?
================================================================

La première clef est ce jour même.
Le fait de séparer un jour de tous les autres jours, de le rendre spécial,
de le délimiter : entre l’annulation des limites à kol nidré et la fermeture
des portes à néila, entre ce soir et demain soir, un nouvel espace des
possibles s’ouvre.

La deuxième clef est l’idée de séparation elle-même.
Si nous sommes UN.E, nous n’avons pas de recul sur nous-mêmes. Nous avons
besoin en nous-mêmes d’une autre voix intérieure qui peut examiner où nous
en sommes.
Nos yeux ne peuvent pas se voir eux-mêmes, mais un œil intérieur peut
les imaginer, les voir en pensée.

Nous devons trouver un point d’appui pour déplacer le monde comme dirait
Archimède. Mais comment trouver un appui hors du monde pour soulever le
monde alors que nous sommes immergés dans le monde ?

Nous devons tirer très forts sur nos propres cheveux pour nous sortir des
sables mouvants comme le baron de Münchhausen. Mais, si vous avez déjà
essayé, vous avez surtout retrouvée des poignées de cheveux dans vos mains
sans être pour autant sorti.es d’affaire. (et les philosophes ont développé ce point)

Nous devons nous tenir les un.es aux autres, mais si la voiture démarre
rapidement, nous nous retrouverons malgré tout à terre comme le prouve
la démonstration des dupontds, dupont t et dupond d, dans Tintin.

Pour nous comprendre nous-mêmes, nous devons nous voir de l’extérieur
de nous.
Nous devons échapper à notre nature. Nous devons avoir une instance à la
fois intérieure et extérieure, c’est peut être cette instance que nous
appelons conscience, âme, transcendance, part divine.
C’est peut-être à elle que nous faisons appel lorsque nous employons
les mots « dieu, éternel, source de vie, abondance ».

Lorsque nous disons « adonaï », « mon seigneur », nous nous référons
peut-être à la part la plus noble, la plus absolue, digne, intouchable
de nous-mêmes.
Et qui sait si cette part est rattachée à une autre force, plus grande,
qui comme notre conscience est à la fois dans le monde et en dehors du monde.
Libre à nous de l’imaginer à notre façon. Et cette part, elle peut-être
sollicitée intensément pendant 25 heures.
**C’est ce que veut croire le judaïsme**.

kédoucha, kadoch
=====================

Et qui sait si un jour comme tous les autres, le jour d’aujourd’hui, peut
être rendu infiniment spécial comme nous le faisons, et se transformer
en « jour de kipour » ? Cela, nous le savons, car nous réalisons cet
exploit chaque année.

L’idée dont je parle, vous la connaissez, est l’idée la plus centrale du
judaïsme (peut-être avec celle de beraHa) : l’idée de Kédoucha.
La kédoucha, c’est le fait de créer un endroit spécial, différent, hors
du monde, un espace magique où tout est possible, où les règles de
l’habitude, du conformisme et de la convention ne s’appliquent pas.

Pour cette raison, ce jour de kipour est kadoch, « mikra kodech », il est
différent, séparé, un point d’appui inattaquable dans nos vies.
Les règles habituelles ne s’appliquent pas, nous ne travaillons pas, nous
ne mangeons pas, nous sommes dégagé.es du monde matériel.

Pour cette raison, **la transcendance, le divin est appelé kadoch**.

**La force de vie, de justice et de paix** que nous nous représentons est
au-dessus de toute contingence, elle est inattaquable, elle est kadoch.
Nous disons « kadoch kadoch kadoch adonaï tsévaot ».

Pour cette raison, nous sommes appelés kadoch.

Notre identité personnelle est spéciale, précieuse, particulière.
Et pour cette raison encore la force de vie nous appelle à vivre pleinement
notre spécificité, notre kédoucha. Nous sommes « am kadoch ».

La transcendance nous demande d’être transcendant.es, le principe d’espoir
nous demande de cultiver notre capacité d’espérance, la force de vie
nous demande de faire le choix de la vie, la source de liberté nous
demande d’être fidèles à notre liberté.
Et même si par nature nous sommes faits d’espérances, de vie et de liberté,
**nous devons malgré tout cultiver notre capacité à rêver un monde meilleur**, à
nous exprimer et à réaliser de grandes choses.

Le Lévitique 19 que nous lirons demain après-midi raconte que la transcendance
a demandé à Moïse de parler aux enfants d’Israël et de leur dire de sa part :
assumez votre particularité, soyez kadoch, soyez spéciaux et spéciales,
soyez au maximum de votre liberté, car je suis moi-même kadoch.

דַּבֵּר אֶל כָּל עֲדַת בְּנֵי יִשְׂרָאֵל וְאָמַרְתָּ אֲלֵהֶם קְדֹשִׁים תִּהְיוּ  כִּי קָדוֹשׁ אֲנִי ה’ אֱלֹהֵיכֶם

Vous l’avez compris, pendant tout kipour, nous formulons le vœu d’être
fidèles à nous-mêmes, maintenant et pour l’année à venir.

Nous cultivons notre petit coach intérieur en nous répétant à nous-mêmes,
et les un.es aux autres : autorisez-vous à vivre votre meilleure vie,
à être vous-mêmes, à être spéciaux, spéciales.

En ce jour de Yom Kipour, nous nous autorisons

- à être juif et juives malgré l’antisémitisme
- à être libéraux/libérales malgré l’intronisation du consistoire par
  Napoléon
- à combattre toutes les discriminations malgré la pression de la pensée
  d’exclusion et d’extrème droite, à refuser le racisme, l’homophobie,
  la transphobie, le validisme, l’agisme, le repli national et au contraire,
  à favoriser l’inclusion
- nous nous autorisons à être pauvres et fier.es malgré la stigmatisation
- à être riches sans surconsommer et à être généreux, malgré la publicité
- à préserver la planète de toutes les façons possibles malgré l’immensité
  de la tâche
- à toutes autres choses qui comptent pour nos vies.

En ce jour de Kipour, nous créons une faille dans le temps, un espace sacré,
dans lequel quelque chose de neuf pourra naitre et grandir.

Hannah Arendt dit cela très bien un dans Les origines du totalitarisme :
« Cet espace entre les hommes est l’espace vital de la liberté.
La terreur et son cercle de fer éliminent non seulement l’espace vital
de la liberté mais la source même de cette liberté, celle qui réside
dans la capacité qu’a chaque homme d’engendrer un nouveau commencement. »

**Kipour nous a invités, et nous sommes là, l’espace que nous définissons
par notre présence constitue un « espace vital de la liberté ».
En constituant cet espace, nous sommes le début d’un rempart contre la
terreur**.

Kipour nous demande d’exercer notre capacité d’engendrer de nouveaux commencements.

- Les intelligences artificielles savent réarticuler une sorte de moyenne
  de ce qui a déjà été fait.
- Notre intelligence du quotidien sait suivre la trace de ses propres habitudes.
- Notre intelligence de kipour est une intelligence originale, spéciale,
  séparée, sacrée, kadoch. A nous de la mettre en œuvre.

Gmar Hatima Tova
