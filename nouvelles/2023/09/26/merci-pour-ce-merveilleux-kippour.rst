.. index::
   ! Kadoch

.. floriane_chinsky_2023_09_24:

=============================================================================================
2023-09-24 Discours Kipour 2023 /Kadoch - gestion de l’espace, gestation de l’espèce
=============================================================================================

- https://rabbinchinsky.fr/2023/09/24/discours-kipour-2023-kadoch/
- https://poursurmelin.files.wordpress.com/2023/09/kipour-5784-2023.pdf

Merci à toutes et tous pour votre présence, votre façon de vous accueillir
mutuellement, votre chant et vos idées.

Quel bonheur que de partager toute cette intensité et toute cette liberté!


Ressources
=============

Discours de Kol Nidré
-------------------------

- https://poursurmelin.files.wordpress.com/2023/09/kipour-5784-2023.pdf

Texte d’étude sur les femmes et le chofar
---------------------------------------------

- https://poursurmelin.files.wordpress.com/2023/09/etude-chofar-femmes-du-mur.pdf


Translittérations et suggestions pour le premier soir de Kipour, Kol Nidré lien kol nidré
-------------------------------------------------------------------------------------------------

- https://poursurmelin.files.wordpress.com/2023/09/soir-de-kipour-kol-nidre.pdf


Prospectus avec mes cours de l’année prochaine (ceux qui sont totalement définis, il en en aura d’autres) : Lien vers le dépliant des cours
-----------------------------------------------------------------------------------------------------------------------------------------------

- https://poursurmelin.files.wordpress.com/2023/09/cours-2023-24-kipour.pdf


Femmes et Chofar, étude de Tichri
------------------------------------

- https://invidious.fdn.fr/watch?v=BFVyc8AZWDc

Le chofar  nous fait vibrer ensemble de ses sons touchants et inspirants.

Les femmes sont-elles aussi associées à ce commandement?

Nous étudions une source précise, le sefer hamaharil (Mayence 14e-Worms 15e),
qui entérine la décision des femmes de s'associer pleinement à cette pratique.


