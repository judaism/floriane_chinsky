

.. _simhat_kippour_2023:

==============================================================================================================================
2023-09-29 **🍋 🌿🌴Chabat Chalom et Hag SaméaH! Préparez-vous à Soukot et SimHat Torah, ce chabat et chabat prochain…**
==============================================================================================================================

- https://invidious.fdn.fr/watch?v=r2ootbTvRbU


Feed-Back ouvre un espace à la façon dont on découvre, redécouvre et
approfondit son judaïsme.

Il y a une pluralité de juif et de juives, toustes ont leur place.

Puisque la liberté des un.es commence là où commence celle des autres,
partageons nos façons de trouver nos voies dans la trame de la vie et
dans la trame des sagesses juives.


Après ce merveilleux Kipour, les fêtes continuent!

Ce chabbat, nous célébrerons également la fête de Soukot.

Ce n’est pas moi qui assurerai les offices, mais voici quelques ressources
pour vous préparer en famille, pour la synagogue, et en prévision de
SimHat Torah la semaine prochaine.

Un document pour accueillir 7 femmes sous la Souka, les Ouchpizot
=====================================================================

- https://poursurmelin.files.wordpress.com/2019/10/meguila-14a-les-7-prophetesses-partiel.docx


Un document pour accueillir 7 femmes sous la Souka, les Ouchpizot, à télécharger ici.

Retrouvez les chants de SimHat Torah sur la playlist suivante ainsi que le hallel ici et les hakafot ici.
===============================================================================================================

- https://invidious.fdn.fr/watch?v=Z0D4WqnFayE&list=PLnHlXjFx9rOQ0HHw90GHxyDilsFzX7NlN

Ainsi que le hallel ici et les hakafot ici.

- https://www.youtube.com/watch?v=h5CPZbEwLuI&list=PLnHlXjFx9rORKcgMmmDK3zvWUqsopfWmU

Préparez-vous un super Seder de Soukot, tout est expliqué ici.

Trouvez ici des commentaires sur les parachiot lues à l’occasion des fêtes de Soukot et SimHat Torah.

