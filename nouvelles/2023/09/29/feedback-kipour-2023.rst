

.. _feedback_kippour_2023:

==========================================================================
2023-09-29 **Feedback kippour à Surmelin**
==========================================================================

- https://invidious.fdn.fr/watch?v=r2ootbTvRbU


Feed-Back ouvre un espace à la façon dont on découvre, redécouvre et
approfondit son judaïsme.

Il y a une pluralité de juif et de juives, toustes ont leur place.

Puisque la liberté des un.es commence là où commence celle des autres,
partageons nos façons de trouver nos voies dans la trame de la vie et
dans la trame des sagesses juives.
