.. index::
   pair: rabba ; Valérie Stessin
   pair: Peertube ; Entretien avec la Rabba Valérie Stessin, sur la situation des civils en israël
   ! Valérie Stessin

.. _valerie_stessin_2023_10_11:

============================================================================
2023-10-11 **Rencontre avec la Rabba Valérie Stessin**
============================================================================

- https://rabbinchinsky.fr/2023/10/11/rabba-valerie-stessin/

Description
===============

Bonjour à toutes et à tous, merci pour votre large participation hier soir.

Comment prendre soin de soi et des autres lorsqu’on est touché.es de
près ou de loin par une catastrophe?

Il n’existe pas forcément de recettes, mais la question mérite d’être posée.

Que faites-vous pour vous-mêmes et pour les autres?

Ce type de partage nous pousse à la solidarité et nous éloigne du sentiment
d’impuissance.

La parole talmudique dit **les actes d’entraide (tsedaka) sauvent de la mort**.

Elle veut dire exactement cela: **l’action nous permet d’échapper au vide
et à la terreur**.

Partagez svp, pour que cela renforce notre pouvoir de vie, de quelle façon
vous prenez soin de vous-mêmes et des autres.

Hier, nous avons entendu **la Rabba Valérie Stessin**.

Ce soir, je vous propose un zoom court de suivi pour partager ce que sont
nos difficultés personnelles autour de la catastrophe israélienne et
ses retentissements pour nous.

Liens cités hier soir
=============================

- https://www.mdais.org/en (Magen David Adom)
- https://womenofthewall.org.il/ |wow| (Les femmes du mur)
- https://www.kashouvot.org/
- https://en.eran.org.il/ (Emotional First Aid)
- https://www.theparentscircle.org/en/about_eng-2/


La vidéo sur peertube **Entretien avec la Rabba Valérie Stessin, sur la situation des civils en israël**
===========================================================================================================

- https://peertube.iriseden.eu/w/nETDQiMtYjTn7PaC2DMvRN (Entretien avec la Rabba Valérie Stessin, sur la situation des civils en israël)

.. raw:: html

   <iframe title="Entretien avec la Rabba Valérie Stessin, sur la situation des civils en israël" width="800" height="400" src="https://peertube.iriseden.eu/videos/embed/af7e01c9-1ddf-482e-b1ad-5f105c7350d4"
       frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

- 00:00 introduction: prenez soin de vous au cours de ce partage
- 01:30 rappel de notions de CNV, d’Écoute Mutuelle et des différentes formes de pouvoir
- 04:30 chant d'ouverture (étrange mais intéressant, merci de votre
  liberté de partage!)
- 07:10 début de la discussion - Rabba Valérie Stessin, qui êtes-vous?
- 11:15 ce qui s'est passé samedi matin
- 15:00 l'importance de l'information de première source - le pouvoir
  ensemble, l'équilibre entre s'informer et se protéger
- 20:20 La dimension instrumentale du fonctionnement médiatique - chéma
- 23:17 l'entraide comme clef en temps de crise
- 29:00 notre capacité à nous organiser librement
- 30:50 l'évidence de l'entraide en israël
- 35:00 décupler notre pouvoir d'action par le partage, donner accès à la réalité israélienne aux autres, partager sa complexité, le traumatisme actuel en Israël et son intensité
- 41:00 comment aider
- 43:00 présentation de quelques associations intéressantes Aide d'urgence psychologique, forum des familles israéliennes et palestiniennes, vidéo de Robie Dalmin, site de Kashouvot, Magen David Adom, Femmes du mur.
- 46:00 vivre au jour le jour, l'instabilité de la vie
- 48:40 questions - comment être avec les femmes du mur si leur office n'a pas lieu?
- 50:45 comment gérer ses émotions, et comment écouter ses proches qui se trouvent en situation de crise? être disponible autant que possible, ne pas s'offusquer de ses changements si elle change de priorités, proposer et envoyer des texto, ne pas s'offusquer de ses demandes, s'entourer de personnes positives
- 55:05 vous pouvez venir à la synagogue, suivre mon site rabbinchinsky.fr, les outils de la sagesse juive - les bénédictions,
- 57:05 nous allons bientôt nous séparer... la force du chant et la force de la rencontre
- 58:25 mot de la fin des Rabbotes Valérie et Floriane, Merci et Continuons
- 1:01:00 exercice de connexion et chant
