.. index::
   pair: Chabat ; Béréchit

.. _chabat_berechit_2023_10_13:

==========================================================================
2023-10-13 **Ce chabat, Chabat Béréchit + mévarHim** 🕯 🍷🍰🕎🏆📚🎶
==========================================================================

- https://rabbinchinsky.fr/2023/10/13/ce-chabbat-chabat-berechitmevarhim-%f0%9f%95%af-%f0%9f%8d%b7%f0%9f%8d%b0%f0%9f%95%8e%f0%9f%8f%86%f0%9f%93%9a%f0%9f%8e%b6/
- https://peertube.iriseden.eu/a/floriane_c/video-channels
- https://peertube.iriseden.eu/feeds/videos.xml?accountId=4929
- https://rabbinchinsky.fr/20-minutes-5784/
- https://www.youtube.com/watch?v=76WlujBzvSo (Debunkible #1 : debunkons la Bible et le personnage de Eve)
- https://www.hebcal.com/sedrot/bereshit-20231014?i=on (Parashat Bereshit 5784)


Parashat Bereshit
====================

- https://www.hebcal.com/sedrot/bereshit-20231014

Parashat Bereshit is the 1st weekly Torah portion in the annual Jewish
cycle of Torah reading.

Bereishit (“In the Beginning”), the **first parashah in the annual Torah
reading cycle**, begins with God’s creation of the world.

The first people, Adam and Eve, eat from the Tree of Knowledge and are
banished from the Garden of Eden.

Their elder son, Cain, kills their younger son, Abel, and Cain is destined
to a life of wandering


Introduction
============

Chère communauté, cher.es toutes et tous,

**Après une semaine si éprouvante**, le temps de la paix du Chabat s’apprête
à nous accueillir.

Rejoignons ensemble ce bastion de tradition pour y retrouver les forces
dont nous avons tant besoin. Vendredi soir à Pelleport autour du Rabbin FARHI
(je serai parmi vous dans l’assemblée) et samedi matin à Surmelin.

Nous étudierons en format `intergénérationnel à 10h (20 minutes de la Rabbin 5784) <https://rabbinchinsky.fr/20-minutes-5784/>`_,
notre office inclura une nomination et la lecture de Béréchit le tout
début de la Torah à 10h30, je vous prépare une étude de soutien dans
les moments difficiles à 13h et nous chanterons avec la chorale de roch Hodech à 14h.

Venez quand vous voulez, comme vous voulez.

**Il est important dans ces moments difficiles de mesurer nos forces et
d’évaluer nos besoins**.

Chéma (écoute)
=================

**Les vidéos d’horreur ont un effet destructeur**.

Notre tradition nous dit "chéma", écoute, et non pas "vois", car la vision a une dimension
envahissante.
L’objectif est de réduire le traumatisme et la haine chaque fois que
c’est possible, faisons attention à cela.

Dans cette optique, une **position éthique de base** consiste à donner des
"trigger warning" = dire ce qui peut être traumatisant pour éviter que
chaque clic soit un risque de traumatisme.

C’est ce que je fais et ferai ici en différenciant les vidéos
"liées à la catastrophe" de celles qui n’y font pas référence.
Les deux sont importantes. Je vous propose donc :

- une vidéo "traumatisme free" de ma nouvelle série `"débunkible"
  sur ce lien pour la voir `sur youtube (Debunkible #1 : debunkons la Bible et le personnage de Eve) <https://www.youtube.com/watch?v=76WlujBzvSo&ab_channel=FlorianeChinsky>`_,
  et `ce lien pour la voir sur peertube (Debunkible #1 debunkons la Bible et le personnage de Eve) <https://peertube.iriseden.eu/w/quTqPzpoo4ULRyK6nvQRCc>`_,
  sans publicité et sans manipulation par algorithme.

- une vidéo qui parle de la situation avec la Rabba Valérie Stessin, qui
  vit à Jérusalem et qui est la fondatrice de l’association Kashouvot, pour
  l’accompagnement des personnes en situations de souffrance ici, sur `peertube (Rabbin Chinsky) <https://peertube.iriseden.eu/c/rabbin_chinsky/videos>`_


Offices ce chabat
======================

🕯 Offices ce chabat: vendredi 18h45 Pelleport, le Rabbin Farhi officie,
je serai présente dans ce moment difficile, Samedi 10h Surmelin 🕯


🍷 Ce soir en fonction de l’organisation de Pelleport, demain kidouch
par la famille de la jeune fille.


📚 Ce samedi à 10h: les 20 minutes du rabbin, temps intergénérationnel
autour de la question "être adulte/ prendre ses responsabilités" 📖


✡️ Atelier Chorale de Roch Hodech à 14h: Venez partager un temps
d’harmonie et de joie, tous niveaux et tous âges bienvenus 🎷

Berechit
=============

📜 La paracha cette semaine: `Lectures sur hebcal (Parashat Bereshit 5784) <https://www.hebcal.com/sedrot/bereshit-20231014?i=on>`_ ,

vidéos de commentaires

- `Playlist paracha/Genèse, Béréchit: Adam, (Béréchit : Adam, Eve, qui est arrivé la première ?) <https://www.youtube.com/watch?v=u7GLdGZ2WUg&list=PLnHlXjFx9rOQtc3NdqEGXbirDkHk_hW0j&ab_channel=Juda%C3%AFsmeEnMouvement>`_
- `Le déluge/le climat, échec de dieu ou de l’humanité (Paracha Béréchit - Le déluge/le climat: échec de Dieu ou échec de l'humanité? - (sur un pied 5779)) <https://www.youtube.com/watch?v=wt_wBeNO8_k&list=PLnHlXjFx9rOQtc3NdqEGXbirDkHk_hW0j&index=4&ab_channel=FlorianeChinsky>`_
- `Avons-nous le libre arbitre ? <https://www.hebcal.com/sedrot/bereshit-20231014?i=on>`_

Roch Hodech avec les femmes du mur
==========================================

- https://rabbinchinsky.fr/offices-wow-5784/

Roch Hodech avec les femmes du mur si elles le peuvent, entre nous de
toute façon `Rosh Hodesh Heshvan – Monday, October 16, 2023, inscriptions et informations (Offices renouveau du mois/Femmes du Mur 5784) <https://rabbinchinsky.fr/offices-wow-5784/>_`.

Leadership inclusif
===========================

Le cours au café des psaumes "Leadership inclusif" est maintenu ce
mercredi, inscriptions et informations `ici (Leadership inclusif 5784) <https://rabbinchinsky.fr/leadership-5784/>`_

Liens, ressources peertube, fediverse
=======================================

Debunkible #1 debunkons la Bible et le personnage de Eve
-----------------------------------------------------------

.. raw:: html

   <iframe title="Debunkible #1 debunkons la Bible et le personnage de Eve" width="800" height="400" src="https://peertube.iriseden.eu/videos/embed/c663d8d4-6b16-4eae-8405-1d4ec10e9147"
       frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">
   </iframe>

- https://peertube.iriseden.eu/w/quTqPzpoo4ULRyK6nvQRCc

- Debunkible #1 debunkons la Bible et le personnage de Eve

- 10mn toutes les 2 semaines, pour discuter du judaïsme. Discussion,
  puis quizz et question de réflexion.
  Premier épisode: la Bible et le personnage de Eve. Pour aller plus loin,
  `Rabbinchinsky.fr <http://rabbinchinsky.fr/>`_
- 00:00 Débunkible, c'est quoi?
- 00:40 Le pouvoir de suggestion du cadre
- 01:32 Il y a deux Eves
- 02:15 Quelques mots sur la Bible: célèbre, bibliothèque, croire en la Bible est impossible
- 03:55 Personne ne croit dans la Bible hébraïque
- 05:10 Datation de la Bible et naissance du judaïsme, la Michna
- 06:10 S'il en est ainsi, à quoi sert la Bible?
- 06:45 Eve, la version de (Saint)-Cyprien est de bien d'autres vs. la version Biblique
- 08:50 Le cadre d'écriture de la Bible, l'importance de revenir au texte initial, hébreu biblique vs hébreu israélien
- 09:55 Le Quizz!
- 11:00 La question de réflexion: Faut-il obéir? A quoi, A qui?
