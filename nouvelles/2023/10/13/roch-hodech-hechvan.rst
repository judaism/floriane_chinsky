.. index::
   pair: Roch chodesh ; cheshvan

.. _roch_hochech_2023_10_13:

=============================================================================================================================================
2023-10-13 |wow| **Roch Hodech Hechvan lundi 16 octobre 2023, entrer dans le nouveau mois en renforçant nos liens avec les femmes du mur**
=============================================================================================================================================

- https://rabbinchinsky.fr/2023/10/13/ce-chabbat-chabat-berechitmevarhim-%f0%9f%95%af-%f0%9f%8d%b7%f0%9f%8d%b0%f0%9f%95%8e%f0%9f%8f%86%f0%9f%93%9a%f0%9f%8e%b6/
- https://www.hebcal.com/holidays/rosh-chodesh-cheshvan-2023


.. figure:: images/dates_roch_chodesh_2020_2027.png
   :align: center

   https://www.hebcal.com/holidays/rosh-chodesh-cheshvan-2023

Rosh Chodesh Cheshvan 2023/5784  רֹ 578אשׁ חוֹדֶשׁ חֶשְׁוָן
====================================================

- http://www.calj.net/roshchodesh
- https://www.hebcal.com/holidays/rosh-chodesh-cheshvan-2023
- https://www.ou.org/holidays/cheshvan/

Rosh Chodesh Cheshvan for Hebrew Year 5784 begins at sundown on Saturday,
14 October 2023 and ends at nightfall on Monday, 16 October 2023.

Start of month of Cheshvan on the Hebrew calendar. חֶשְׁוָן (transliterated
Cheshvan or Heshvan) is the 8th month of the Hebrew year, has 29 or 30 days,
and corresponds to October or November on the Gregorian calendar. רֹאשׁ חוֹדֶש

ׁ, transliterated Rosh Chodesh or Rosh Hodesh, is a minor holiday that
occurs at the beginning of every month in the Hebrew calendar.

It is marked by the birth of a new moon.


Introduction
============

- https://mailchi.mp/womenofthewall/ouronlychance-2690131?e=c306018c49
- https://invidious.fdn.fr/channel/UC4cD25kSAS_vRH0WZ1kOcLg/playlists (chaine youtube sans pub womenofthewall)
- https://invidious.fdn.fr/feed/channel/UC4cD25kSAS_vRH0WZ1kOcLg (le Fil RSs de la chaine Youtube womenofthewall)
- https://womenofthewall.org.il/

Voici la newsletter des femmes du mur, publiée il y a quelques minutes,
que j’ai traduite pour vous.

**Mobilisons-nous à leurs côtés pour roch hodech.**

Nous assisterons à leur zoom, et serons en contact entre nous via un
groupe telegram et via youtube, `les liens seront communiqués aux personnes
inscrites ici (Inscription offices femmes du mur ) <https://framaforms.org/inscription-offices-femmes-du-mur-1695899101>`_.

Que la douleur et l’adversité reculent pendant ce nouveau mois, et que
la cohésion et l’entraide triomphent.

La newsletter des femmes du mur (Women of the Wall) , Nat Hoffman
===================================================================

.. figure:: images/anat_hoffman.png
   :align: center


Chères sœurs, sympathisant.es et ami.es,

La catastrophe qui nous a frappés le 7 octobre 2023 a changé le paysage
en Israël.
Je veux dire littéralement.

Les trottoirs dans les villes de tout Israël sont remplis de vieux meubles,
d’appareils rouillés et d’ustensiles de cuisine vieux et fissurés.
En temps de paix, les abris sont utilisés comme entrepôt, mais maintenant
les gens ont vidé les abris de leurs bâtiments. Ils se sont débarrassés
de la malbouffe afin de se faire de la place dans cette crise.
Je suis convaincu qu’un processus similaire est en cours dans l’établissement
de nos priorités nationales. Nous savons maintenant qui et quoi est
important et central pour notre survie.

Les Juifs de la diaspora démontrent chaque minute de la journée qu’ils
sont avec nous de la manière la plus profonde et la plus émouvante.

WOW a reçu une avalanche de lettres de soutien et d’encouragement tout
au long de cette semaine. Ces lettres viennent du monde entier.

Rabbin Yoram Rokmaker, Amsterdam
-----------------------------------

Dans les périodes où nous sommes les plus vulnérables dans la vie,
nous montrons le plus de force.
Cela rend nécessaire de réfléchir à ce que nous pensions être vrai, à
nos valeurs et à l’avenir. Et nous surmonterons cela aussi.

– Rabbin Yoram Rokmaker, Amsterdam


Rosanne Hertzberger, Pays-Bas
--------------------------------

Je voulais vous soutenir surtout en ces temps sombres. S’il vous plaît,
continuez votre combat pour l’égalité. Nous sommes tous à vos côtés dans
nos cœurs et nos esprits.

-Rosanne Hertzberger, Pays-Bas


David et Debbie Astrov, Washington, D.C.
--------------------------------------------

Cela a renforcé mon amour d’Israël, mon amour des Israéliens, mon amour
du judaïsme, mon sionisme, ma détermination à combattre et à dénoncer la
haine partout où je la vois, mon amour et mon admiration pour vous et
tout ce que j’ai appris de vous, mon cher professeur, leader et ami.

-David et Debbie Astrov, Washington, D.C.


Rabbin Amy Perlin, Virginie
-------------------------------

Je suis ici pour écouter, avec une épaule sur laquelle m’appuyer.

Je prierai pour que vous et ceux que vous aimez alliez bien. Aussi bien
que n’importe qui peut l’être en ce moment. Mon cœur se brise avec le
tien, mes yeux pleurent avec toi, mon amour est toujours avec toi.

-Rabbin Amy Perlin, Virginie

Ces derniers jours ont été parmi les plus sombres auxquels le peuple
juif ait eu à faire face depuis des années.
Nous avons vu le nombre de morts grimper douloureusement plus haut,
nous avons entendu des nouvelles poignantes d’otages à Gaza, de personnes
arrachées de leur lit par des terroristes et d’autres horreurs indicibles.

Les nombreuses lettres et mots de soutien que nous avons reçus nous ont
soutenus et nous ont donné de l’espoir pendant ces jours noirs.

Alors que nous nous réunissons pour célébrer Rosh Hodesh Heshvan, nous
le faisons non seulement dans l’ombre de la guerre, mais dans la chaleur
d’une communauté mondiale.
Nous avons décidé d’organiser les services de Rosh Hodesh via Zoom le
lundi 16 octobre 2023 à 7 heures du matin, heure d’Israël.

Maintenant, nous avons besoin que tout le monde se rassemble et prie pour
la sécurité du peuple d’Israël.
Ensemble, nous persévérerons et nos prières serviront de phare sur le
chemin de la paix.

Rejoignez la réunion zoom de Roch Hodech des femmes du mur ce lundi à
6h heure de Paris ici : https://us02web.zoom.us/j/87573220579

>>>>>Rejoignez le groupe de chat francophone qui nous permettra de commenter
ce zoom en français autour du rabbin Floriane Chinsky, `le lien sera
communiqué aux personnes inscrites ici (Inscription offices femmes du mur) <https://framaforms.org/inscription-offices-femmes-du-mur-1695899101>`_
<<<<<

Maintenant, nous avons besoin que tout le monde se rassemble et prie pour
la sécurité du peuple d’Israël.
Ensemble, nous persévérerons et nos prières serviront de phare sur le
chemin de la paix.

« …. Et une nation resta immobile, le cœur brisé mais respirant, Pour
recevoir sa récompense, envoyée par le ciel, en bas. » Le plateau d’argent –
Nathan Alterman

Amen. Selah.

.. figure:: images/anat_hoffman.png
   :align: center

Donate
--------

- https://womenofthewall.org.il/donate/
