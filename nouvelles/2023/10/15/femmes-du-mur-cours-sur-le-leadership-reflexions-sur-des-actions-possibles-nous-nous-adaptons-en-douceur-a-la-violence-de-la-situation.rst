.. index::
   pair: Femmes du mur ; 2023-10-15
   ! Valérie Stessin

.. _femmes_du_mur_2023_10_15:

===========================================================================================================================================================
2023-10-15 **Femmes du mur/ cours sur le leadership/ réflexions sur des actions possibles… nous nous adaptons en douceur à la violence de la situation**
===========================================================================================================================================================

- https://rabbinchinsky.fr/2023/10/15/femmes-du-mur-cours-sur-le-leadership-reflexions-sur-des-actions-possibles-nous-nous-adaptons-en-douceur-a-la-violence-de-la-situation/

Les femmes du mur ne pourront pas être au Kotel demain matin
===============================================================

Les femmes du mur ne pourront pas être au Kotel demain matin, elles feront
leur office sur zoom.
Rejoignez-les ici, demain à 6h du matin: https://us02web.zoom.us/j/87573220579


Sur telegram
===============

Comme elles seront sur zoom et que nous ne pourrons pas suivre deux zoom
en même temps, au même moment, nous serons sur telegram.

Vous pouvez: vous connecter avec elles par zoom par ordi et avec notre
groupe francophone par telegram sur votre téléphone par exemple.

Ou le contraire.

Nous n’avons pas l’expérience de cela mais nous trouverons des solutions.

Pour vous connecter par télégram, communiquez-moi votre adresse sur le
lien suivant et je vous enverrai une invitation


Pour le cours de mercredi soir sur le leadership juif, le café des Psaumes ne pourra pas l’accueillir
========================================================================================================

- https://framaforms.org/cours-leadership-juif-1697361226

Pour le cours de mercredi soir sur le leadership juif, le café des Psaumes
ne pourra pas l’accueillir.

Nous procéderons donc de la façon suivante: Vous vous inscrivez, je vous
envoie un lien zoom, les personnes qui peuvent accueillir le cours me
communiquent leur adresse, je choisis un lieu, j’enverrai l’adresse à
quelques personnes pour que nous puissions avoir une partie présentielle,
j’essaierai de trouver les meilleurs solutions pour tout le monde.

Faites-moi part de vos préférences ici: https://framaforms.org/cours-leadership-juif-1697361226


Enfin, j’envisage différentes possibilités d’actions
=========================================================

- https://framaforms.org/votre-adresse-email-1678137185

Enfin, j’envisage différentes possibilités d’actions et je voudrais votre avis,
je propose un zoom qui sera ouvert pas uniquement du point de vue juif,
mais aussi largement que possible, pour collecter des propositions et
des objections, probablement jeudi à 18h, si vous êtes intéressé.es,
mini formulaire de contact ici: https://framaforms.org/votre-adresse-email-1678137185


bon mois de Hechvan
======================

Prenez-soin de vous,

Bonne semaine et bon mois de Hechvan
