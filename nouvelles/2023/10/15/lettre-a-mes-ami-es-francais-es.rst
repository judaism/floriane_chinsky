.. index::
   pair: Lettre à mes ami.es français.es ; 2023-10-15

.. _lettre_2023_10_15:

===========================================================================================================================================================
2023-10-15 **Lettre à mes ami.es français.es**
===========================================================================================================================================================

- https://rabbinchinsky.fr/2023/10/15/lettre-a-mes-ami-es-francais-es/


Je suis française.

Je suis beaucoup d’autres choses, et je suis française, et je suis humaine.

Les idées que je développe ici, je me les adresse également à moi-même.

Si vous êtes dans l’infini de la peine et de la peur, que vous avez besoin
d’une main tendue, et de rien d’autre, ne lisez pas cet article.

Si penser la souffrance de l’autre est trop dur car votre douleur est
trop forte, j’espère que vous trouverez consolation ou apaisement, un
peu, au moins, que dire et que souhaiter…

L’infini des ressources du monde devrait être à portée de main, et je
pleure la pénurie des ressources, et je cherche comment faire évoluer cela.

J’espère que moi aussi, je trouverai apaisement de mes peines, de mes
craintes et de mes douleurs, qui sont bien petites par rapport à la
majorité des souffrances des êtres humains dans le monde.

Dans ces quelques mots, je veux laisser s’exprimer une autre part de moi-même,
**celle qui a conscience que la raison est le seul salut final**.

En tant qu’humaine, je ne serai au repos que quand tous les êtres humains
seront au repos, ce n’est pas demain la veille.

En tant que juive, appartenant à une minorité souvent attaquée, **je ne
serai en sécurité que lorsque la puissance de paix de l’humanité aura été
fondamentalement renforcée**.

Et ceci est vrai, je le crois, pour toute personne qui veut pouvoir
s’exprimer en liberté.

Pour s’exprimer en liberté, nous avons besoin de la liberté des autres
=========================================================================

**Pour s’exprimer en liberté, nous avons besoin de la liberté des autres**.

Zamenhof le savait, et pour cela, il a consacré sa vie à l’Esperanto.

Marshall Rosenberg le savait, et pour cela il a consacré sa vie à la
communication nonviolente.

Les rabbins de la michna le savaient, et **pour cela ils ont consacré leur
vie à un judaïsme-culture et non à un judaïsme-guerrier**.

Ceci est peut-être valable pour toutes les minorités:

Les soutiens ou les attaques des plus puissants que nous ne sont pour
eux que leur façon de se faire la guerre entre eux, à travers nous.

**Je ne veux pas être soutenue, ni être attaquée, je veux juste pouvoir exister**.

Je pense que c’est le désir de tous les pacifistes, quelles que soient
leurs religions ou leurs nationalités, leur genre ou leur couleur de peau,
etc…

Pour tout vous dire, en ce moment, **je préférerais rester dans le silence**.

**J’envisage fortement que, à l’avenir, mon seul commentaire sur la situation
soit**: « si tu me soutiens, méditons ensemble pour faire face aux mouvements
de la violence du monde en nous »,

**si tu soutiens celles et ceux que tu crois être mes ennemis, méditons
ensemble pour apaiser les mouvement de la violence du monde en nous.**

Quand j’entends les discours que j’entends, je ressens de la peur, et
**sachez que ce texte me demande du courage**.

A titre personnel, j’ai besoin de paix et de respect, de la part des
personnes qui adhèrent à ces quelques mots comme des personnes qui adhèrent
partiellement ou qui en sont critiques, puisque **nous partageons la même
humanité, et que nous habitons sur la même planète**.

Que penser et que dire de la situation israélo-palestinienne quand on est en France
======================================================================================

Que penser et que dire de la situation israélo-palestinienne quand on
est en France :

1. :ref:`reconnaître la douleur <reconnaitre_la_douleur>`.
2. circonscrire le cercle de la haine.
3. aider dans les cercles qui sont les nôtres.
4. en parler pour mener des actions de **soutien matériel aux besoins
   matériels de toutes les personnes et danger physique**, et pour mener
   des **actions de baisse de niveau de la violence de tous les autres**.


.. _reconnaitre_la_douleur:

1. Reconnaitre la douleur
-------------------------

La première chose bien sûr est de tendre la main vers la peine des personnes
touchées au premier plan.

**Face à l’augmentation de la violence, mobiliser l’infini de la compréhension**.

La peine, la peur, le sentiment d’impuissance sont des sentiments normaux
dans ces circonstances.
Peut-être que vous éprouvez ces sentiments ou que vos proches les éprouvent.
Le mieux que nous puissions faire est de nous écouter, de nous comprendre,
de pleurer ensemble, de nous écouter mieux, de nous comprendre mieux, de
pleurer ensemble, d’apprendre à écouter encore mieux, de nous écouter
encore mieux, de nous comprendre, et de chercher de l’aide spécialisée
si nécessaire.

2. Circonscrire le cercle de la haine
----------------------------------------

La haine, la peur, la colère, la tristesse provoquent parfois un effet
domino.

La haine des uns ravive la haine des autres.

Il faut au contraire saisir toutes les chances de désamorçage.
La Torah demande d’aider à décharger l’âne de son ennemi (Exode 23:5).

Pour l’âne, trop chargé.

Et pour l’occasion de dépasser le manichéisme ami/ennemi.

Et pour désamorcer tout ce qui peut l’être.

On ne parle pas ici d’une personne qui vous met le couteau sous la gorge,
là, la réponse appropriée, est de se protéger, par tous les moyens.
(sauf 3 moyens : assassiner une tierce personne, commettre un viol et
renier ce qu’on est, sanhédrin 74a).

Le texte biblique qui demande de soulager l’âne de son ennemi.e parle
ici d’antagonismes installés, qu’il faut désamorcer.

**La violence engendre la violence. Toute violence désamorcée contribue à la paix**.

Puisque la violence génère la violence, la violence à laquelle nous
sommes exposé.es génère de la violence en nous.

Il faut limiter notre propre exposition à la violence.

Nous ne pouvons pas nous détourner, mais pas non plus nous laisser
brûler.
Il n’y a pas de devoir de rester devant les infos ou de visionner les
images les plus dures.

Le monde médiatique a ses raisons de diffuser, et nous avons les nôtres
pour filtrer.

L’information est une chose, l’exposition à des images choquantes en est
une autre.
L’information sert à la compréhension et à l’action.

Les images choquantes, en quantité limitée peuvent parfois être éclairantes.
Au delà d’un certain seuil, elles servent au contraire la stupeur et la haine.

A chacun, chacune, de mesurer les doses absorbables, de se protéger,
de se soigner.

Puisque la violence génère de la violence, la violence à laquelle nous
sommes exposé.es génère de la violence en nous.

Il faut amortir cette violence. Dans le cas contraire, elle ressortira
à notre insu, polluera nos analyse, nous poussera à voir le monde en
« ennemis/amis », nous rejettera dans des clans, préparera les prochains
conflits.

**Attention, pas de confusion : Il ne s’agit pas du tout de se laisser
faire**.

Désamorcer une violence peut renvoyer à différentes situations. Par
exemple désamorcer sa propre violence en la soignant, en se soignant.
Ou empêcher la violence de quelqu’un d’autre en se protégeant ou en se
défendant ou en aidant autrui à se défendre.

L’usage protecteur de la force est légitime pour empêcher une violence.

Différentes façon de désamorcer les violences
===================================================

**Il existe différentes façon de désamorcer les violences. Et l’une d’entre
elle est la précision dans les mots que nous employons et dans la clarté
de notre pensée**.

En particulier **il faut éviter les généralisations**.

Il ne faut pas réduire l’autre à ce qui peut nous faire peur en lui.
Lorsqu’on est fou furieux, l’ennemi est juste l’ennemi. Mais c’est notre
responsabilité de rester sain.es d’esprit.
Entre personnes raisonnables, on n’a pas d’ennemis. Chaque fois que
c’est possible ( c’est à dire quand on n’est pas emporté par des sentiments
irrésistibles ) **il faut être précis.es dans le langage et la pensée**.

Pour cette raison, selon le texte du livre des Nombres, les guerriers
doivent réintégrer leur équilibre avant de retourner à la vie civile
(Nombres 31:18).
Ils ont le devoir de se battre pour se défendre, mais l’interdiction de
revenir à la vie civile sans « se purifier » autrement dit, trouver un
sas de sécurité qui les rend à leur équilibre moral et psychique.

C’est normal d’être parfois submergé.e par ses blessures, mais pas de
prétendre à l’impartialité dans ces moments-là.
Tous les sentiments sont audibles, mais tous ne peuvent pas servir de
base à l’action.

Dans certaines situations, les sentiments nous submergent, on mérite de
l’empathie et du soutien.

Dans d’autres circonstances, on est en mesure de garder la tête sur les
épaules, et de prodiguer aux autres de l’équilibre et de l’humanité.

Dans d’autres encore, les émotions des autres sont des vulnérabilités
que certains exploitent.

**Espérons au maximum que les équilibres restent du côté de la paix et de
la recherche de solutions**.

La précision permet la solution
===================================

**La précision permet la solution**.

Je vais essayer d’être claire sur cette question.

**La formulation change la vision du problème**. Aucune paix n’est possible
avec des « ennemis ».

Mais de nombreuses solutions sont possibles face à un problème commun.
Rien n’est possible si on croit que l’autre est le problème.

Il est peut-être à la mode dans certains milieux de clamer sa colère au
nom de la souffrance des israéliens ou ou au nom de la souffrance des
palestiniens.

Agir ainsi, c’est utiliser notre position privilégiée pour instrumentaliser
leur douleur. **Nous sommes privilégié.es car nous n’avons pas faim et froid**,
nous n’avons pas du fuir nos maisons ou accueillir des étranger.es chez
nous, nous n’avons pas de sifflements de bombes au dessus de nos têtes,
d’enfants mobilisé.es, de grands-mères et de petits enfants otages.

Nous avons cet avantage sur eux et elles, **et il est indécent d’en profiter
pour parler à leur place**, sélectionner lesquels d’entre eux et elles
auront une parole publique.

**Nous avons cet avantage et il est interdit d’en faire usage**.

**La tendance aux grands discours sur la situation est infiniment mortifère**.
Restons à notre place.

De ma place, quelles sont les atteintes objectives qui me touchent,
quels sont mes sentiments, quels sont mes besoins, quelles actions
je souhaite prendre.

De notre place, et non de la leur.

Faisons face courageusement à nos propres difficultés sans prendre le
prétexte de l’horreur des autres pour échapper à nos responsabilités.

Le Hamas est une organisation terroriste. Certaines personnes ont commis
des actes atroces, affolants, inconcevables.
Mais les palestiniens ne sont pas le Hamas, les musulmans non plus et
les arabes ne sont pas tous musulmans.
**Et derrière toutes ces catégories se trouvent des vies humaines.
Les généralisations tuent.**

De même, les israéliens ne sont pas réductibles à leur extrême droite,
et les juifs et juives du monde ne sont pas israéliens et israéliennes.

Si nous laissons la situation catastrophique en Israël rejaillir sur la
situation en France, nous soutenons le cycle du malheur.
**Eux et elles, sur place, font face au pire. Nous nous devons au minimum
de garder notre sang froid**.

L’humanité et la solidarité sont également contagieuses, heureusement
==========================================================================

**L’humanité et la solidarité sont également contagieuses, heureusement**.

Sommes-nous capables de produire des actes de solidarité aussi forts que
les actes de déshumanisations dont nous sommes témoins ?

L’humanité et la solidarité, partout où elles peuvent progresser, contribuent
à l’avancée de toutes les paix.

C’est ce que font par exemple les familles israéliennes et palestiniennes
endeuillées qui se rencontrent ici : https://www.theparentscircle.org/en/about_eng-2/
(The Parents Circle – Families Forum (PCFF) is a joint Israeli-Palestinian
organization of over 600 families)


Avant toute autre action, il faut circonscrire le cercle de la haine
=========================================================================

Avant toute autre action, il faut circonscrire le cercle de la haine.
Sinon, en réalité, nous rejouons nos blessures intérieures dans la guerre
d’autrui. Nous nourrissons la guerre d’autrui de nos propres lâchetés. C’est dangereux.

Dans un premier temps, donc, il faut gérer la haine en nous si elle existe.

Et la peine, et la colère.
Par la clarté des mots et le refus des  généralisations.

Par le travail émotionnel sur les vieilles blessures, lointain dans nos
passés personnels et collectifs.
Exprimer nos sentiments en notre nom propre, et écouter ceux des autres,
en leur nom propre, sans se les approprier.

C’est la première règle : ne pas instrumentaliser les problèmes des autres.

C’est en ce point que se rejoignent Dom Helder Camara, Hillel et sans
doute beaucoup d’autres:

L’Evèque catholique brésilien du XXe siècle Dom Helder Camara disait:
« Avant de nous aider à nous libérer, occupez-vous déjà de la justice
chez vous et faites déjà le ménage chez vous, et vous avez du boulot. »
(approximatif, conférence publique, vers 1968, selon le témoignage d’une
personne présente à cette conférence).

Hillel, le sage né à Babylone et revenu en terre d’Israël du -Ie siècle
disait: « Si je ne suis pas pour moi qui le sera, si je ne suis que pour moi,
que suis-je, si ce n’est pas maintenant, quand?

Prenons soin de nous, restons ouvert.es aux autres, de toutes nos forces.

Puisque vous avez lu cet article jusque ici, partagez en commentaire vos
plus belles citations, et bien sûr tout ce que vous voudrez…


Citations
============

En ce qui concerne les citations en voici 3:

Citation de Rabbi Danya Ruttenberg
---------------------------------------

We have 3 jobs on this planet:

1) To enjoy the exquisite beauty here
2) To love big & bravely
3) To care for one another & fight for justice & equity for everyone.


We have much to do on 3) now. But don’t neglect 1) and 2).

You need them. AND they offer fuel for the fight.

Citation D'albert Camus
-------------------------------

Toute forme de mépris, si elle intervient en politique, prépare ou
instaure le fascisme.

Citations concernant le nommage en informatique
---------------------------------------------------

In software, naming matters, because names reflect how you think about
a problem.  Code is also communication, and naming is a big part of making
it work. (François Chollet)

You should name a variable using the same care with which you name a
first-born child” (Clean Code (Robert C. Martin))
