

.. _ca_se_passe_ici_2023_10_17:

===========================================================================================================================
2023-10-17 🧡 **Ça se passe ici #1** En toute simplicité, un partage d'expérience, en langage non violent #NonViolence
===========================================================================================================================

- https://invidious.fdn.fr/watch?v=yhmB-UDOKdA

On parle beaucoup de ce qui se passe "là-bas".

Mais nous devons avant tout savoir ce qui se passe ICI, autour de nous.

En toute simplicité, un partage d'expérience, en langage non violent #NonViolence


Transcription
----------------

Nous sommes témoins de façon directe de certaines violences ou alors nous
nous en sommes victimes et dans ce cas-là la question c'est de les faire
cesser et la question c'est celle de l'action immédiate pour s'en prémunir
pourtant souvent nos ressentis sont plus le reflet de ce qui se passe à
l'intérieur de nous

la bonne nouvelle c'est que dans ce deuxème cas nous avons un grand pouvoir d'action
très important pour amoindrir notre ressenti de violence et également
la violence que nous répercutons sur le monde

Il faut différencier ce qui ne dépend pas de nous de ce qui dépend de nous
il faut différencier les faits des sentiments des besoins et des actions
que nous pouvons mener **c'est ce que nous faisons dans ces vidéos pour
nous donner davantage de paix intérieure et de puissance de pacification
vis-à-vis de l'extérieur**

c'est parti merci d'être avec nous

que s'est-il passé comment le décrire

alors les faits font suite au  contexte qui est présent depuis maintenant
une semaine et de ce qui se  passe donc en Israël et ce qui s'est passé
euh comme acte enlèvement et cetera euh ; acte teréroriste et et en fait
j'étais chez moi euh et bien sûr habitée par toutes ces images habitée
par toutes ces ses narrations ces récits et aussi avec l'impression
d'essayer de chercher la réalité là-dedans parce que ça ne se passe
que par les réseaux sociaux pour l'instant et en fait j'étais chez
moi après une journée travail à un moment donné j'ai entendu l'action
à l'extérieur euh l'action à l'extérieur qui j'ai d'abord pensé que
c'était lié à des matchs de foot rugby ou je ne sais pas quoi je suis
complètement déconnectée de la réalité parfois par rapport à ça
du moins et j'ai essayé de quoi mon chien s'est mis à aboyer et je me
suis mise à la fenêtre et je me suis dit c'est vraiment ce qui est en
train de se passer et j'ai vu en fait une partie de la manifestation ;
c'est une manifestation sauvage je crois qui pas pas déclarée qui était
dispersée à République et qui en fait fenêtre et j'habite au-dessus
d'un hyper cacher et donc à ce moment-là donc c'est un énorme groupe
qui hurle et qui crie tout ce qui va avec les fait actuels qui disent
on est tous Palestiniens euh Israël assassin pour certains qui crient mort
aux Juifs, libérer la Palestine et en fait à ce moment-là rien que d'en
parler ça me crée de l'émotion parce qu'en fait à ce moment-là je
me suis senti complètement en insécurité et je me suis dit que dans
cet environnement là ma voix pouvait pas être entendu et que j'avais
l'impression que j'étais prise dans un énorme amalgame
et que là-dessus plus de prise en fait ; que l'irréel qui se passe
derrière les réseaux sociaux quand on imagine la peine des autres ce
qui se passe qui est atroce qu'on essaie de quantifier graduer bah là en
fait le réel m'a percuté plein de fois en bas de la maison

d'accord donc on est dans un exercice un peu de canalisation donc on est dans
un exercice un peu de canalisation donc ça vaut la peine aussi juste
d'écouter sans interrompre mais c'est pas exactement ce qu'on est en
train de faire là mais donc sur les faits c'est quand donc que ça se
formulerait comme ça dites-moi si ma formulation est bonne et refaites la
mieux si nécessaire

quand j'ai entendu et vu un grand nombre de personnes
qui disaient d'une voix très forte nous sommes tous Palestiniens et pour
certains d'entre eux mort au Juifs ça c'est les faits

oui Israël assass assassin Israël assassin h d'accord donc ça c'est les faits
et ce que vous avez décrit d'autres a tour c'est le contexte qui donne
plus des informations sur vos besoins que sur les faits c'est précisément ça oui et
donc maintenant indépendamment de toute autre chose quand vous avez
entendu ça qu'est-ce que vous avez ressenti quel était le sentiment
derrière

je crois que le pire sentiment ça a été de l'insécurité
ça a été vraiment de me dire que là-dedans je suis seule en fait
pardon ça me crée l'émotion mais en fait c'est très difficile
à appréhender en fait ce sentiment que qui va qui va aider
ou comprendre enfin si jamais enfin il y a une masse qui arrive comme
ça qui va pouvoir entendre ma voix à ce moment-là que ben moi à mon
échelle j'y peux rien que que que le nous qui sont en train de quand ils
me disent vous bah moi j'ai envie de dire mais à quel moment est-ce que
c'est moi quoi et et que en fait ma  peine ou ma peur peut pas être
entendu que en fait on va toujours me scander un oui mais et que c'est
je me sens complètement désarmé dans ce enfin à ce moment-là je me
suis dit ça peut venir jusqu'à l'intérieur de moi-même quoi voilà

d'accord donc c'est à vous qui elle appartient de dire ce que sont vos
sentiments mais de ce que j'entends vous avez ressenti de la peur de la
solitude de la confusion de la détresse

oui de de l' j'essaye toujours de faire deux poids et 2 mesures de comprendre
et en fait je je comprends que des personnes puissent penser autrement
mais là je me suis mais qui va essayer de penser comme moi en fait et je
crois

donc une une dimension de désespoir

ouais vraiment désemparé désespérer et ça m'a en fait
ça ça a rajouté en fait ça ça a rajouté à plein de détail de fait
aussi autour de moi où Ben ma nièce est dans une école juive également
et de penser que mon petit mon petit bébé ma nièce de 3 ans et et avec
je sais pas qui pourrait potentiellement une cible ça me noue le cœur
aussi quoi enfin c'est plein de petites choses qui mis bout à bout voilà

c'est bien et donc si on va toucher quel besoin a été heurté et quel
besoin en vous doit être nourri donc un besoin c'est une chose
qui est universelle j'ai pas besoin que tu fasses ça mais j'ai besoin
de tu vois j'ai pas besoin que tu donnes de la nourriture mais c'est
un besoin j'ai besoin de nourriture il y a bien sûr des des besoins
émotionnels aussi si vous cherchiez quel besoin a été heurté vous
direz que c'est quoi

bah la sécurité en fait c'est c'est vraiment la
sécurité ouais c'est surtout ça la sécurité sécurité après bien
sûr il y en a d'autres d'affections compassion, empathie mais le plus la
flagrance a été la sécurité

d'accord et donc question suivante qui est toujours en négociation
quand on est en communication non violente parce qu'on l'élabore au fur
et à mesure V que là on mesure j que là on revient dans le concret et
puis vous avez vous n'avez pas forcément la la réponse tout de suite
mais toujours poser la question c'est important et donc qu'est-ce que
vous pourriez demander à qui de concret par rapport à ce besoin que
vous avez formulé donc le besoin que vous avez formulé principalement
c'est le besoin de sécurité en creusant il y en a peut-être d'autres ou
c'est peut-être qui vous pourriez demander quoi y compris à vous-même
ça veut dire je me demande à moi-même de faire telle chose ou je
souhaite demander à telle personne de faire telle chose qu'est-ce que
vous diriez

euh pour moi ça a été de proposer à ma sœur donc quelqu'un de mon noyau
très très proche de ma compagnie en fait à la synagogue et de retrouver
des personnes où je savais que là il y avait on avait un besoin
commun je crois ou une envie commune à ce moment-là et de de me détacher
de de mettre de la distance en fait de mettre de la distance en fait par
rapport à à l'actualité les médias et cetera et de de retrouver ma ce
qui me fait du bien en fait  à moi c'est d'abord je me le suis apporter
à moi-même euh en acceptant mes émotions euh et en fait en me disant
reconnecte-toi juste à quelque chose  simple basique et voilà et

d'aller excellent très bien bah vous
pouvez continuer on peut toutes et tous continuer à chercher quelles sont
les façons de mieux nourrir nos besoins et et super bon ben donc je vous
propose de de clore un petit peu cette partie de prendre trois respirations
et ensuite de débriefer cette petite session donc moi je pense qu'elle a
été utile à différents niveaux mais mais c'est important pour moi de savoir
pour vous comment vous avez vécu ce quadrillage faits, sentiments, besoin
proposition verbalisé parce que apparemment vous l'aviez déjà fait de
façon interne pas mal donc prenons trois respirations

ouais on expire 2 et 3

je sais pas si ça suffit pour avoir du recul mais c'est
déjà un premier pas euh petit petit débrief sur ce que nous venons de
faire ensemble

bah nécessaire sincèrement de de bien cadrer comme ça
c'est nécessaire de savoir fa en fait le parce que le la construction
de partir des faits et dans le contexte et la personne se resitue à ce
moment-là sur ce qu'elle a ressenti et en fait en quoi enfin c'est ce
que je fais dans dans mon travail en tant que thérapeute aussi c'est de
baser sur des faits et de petits à petit remonter sur les valeurs qui ont
été touchées à ce moment-là et de savoir bah là ça a été pour moi
la valeur de sécurité insécurité que est-ce que ça a généré Cère
de la tristesse de la colère tout ça mais euh c'était nécessaire de
savoir vraiment l'élément en déclencheur en fait

d'accord euh moi ce que
je ressens c'est euh [Musique] alors une une certaine frustration que
ce soit pas zoom et pas en visuel parce que j'aurais en visuel parce
que j'aurais envie de davantage d'intensité autour de ça ressens une
admiration parce que c'est je trouve enfin d'une part de l'admiration
donc de la connexion parce que ce chemin que vous avez fait de vous-même
autour de cette violence il est très intéressant donc du côté donc
la connexion et de l'admiration et et de la reconnaissance aussi parce que
je pense que c'est important de pouvoir se faire ce petit partage et que
moi j'ai moi-même un besoin de contribuer et de et j'ai aussi un besoin
de sécurité et le fait de permettre de partager nos expériences pour que
d'autres comprennent ce que nous vivons et donner une légitimité et pas
être toute seule pour donner une légitimité à ce que nous ressentons
et tout ça dans une démarche où on partage la volonté d'endiguer les
violences inérieur à tous les niveaux pour moi ça contribue fortement
fortement à mon besoin de sécurité donc je vous et puis on continue
on va voir comment comment on fait la suite vous avez encore un mot à
ajouter peut-être

non juste merci en tout cas d'avoir proposé cette
échange parce que euh je pense que ça permet de d'autant plus de poser
des mots sur des mots et de rendre légitime de faire sortir quelque chose
qui est à l'intérieur de comprendre à l'intérieur de comprendre l'impact
de certaines choses et de reconnaissance identification enfin tout ça
ça ça fait du bien en fait vraiment de parce que même en ayant par
exemple en parallèle échangé avec ma sœur ma sœur m'a dit je sais
pas comment l'expliquer mais je me sens profondément seule par rapport
à ce qui se passe ma mère aussi enfin pour et du coup on s'est soudé
autour de ça quoi et ça ça a fait du bien

c'est bien et le fait d'avoir pu moi aussi exprimer quelque chose pour moi là ça a
vraiment renforcé aussi la connexion donc excellent merci beaucoup et
on continue merci beaucoup beaucoup et on continue

