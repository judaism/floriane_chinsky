
.. _combattre_en_restant_humain:

========================================================================
2023-10-18 🧡❤️ **Comment combattre tout en restant humain ?**
========================================================================

Source:
===========

- https://www.youtube.com/watch?v=OGUQCydy9-A
- https://invidious.fdn.fr/watch?v=OGUQCydy9-A
- https://peertube.iriseden.eu/w/726FRrCqiL4QMVfmnJTJ99
- https://rabbinchinsky.fr/

- https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/566928/
- https://www.kan.org.il/content/kan/kan-reka/p-10846/ (Kan en français)


Introduction
===============

Une humble contribution pour éclairer la façon dont la télévision israélienne
parle du conflit.

Pour info: ils n'ont pas diffusé les vidéos choquantes, et ils diffusent
ce type de reportages.

Je l'ai trouvé intéressant, je l'ai traduit, je fais quelques commentaires
au passage.

L'intégralité du reportage ici: Nom officiel du reportage Il est allé
sur le terrain du massacre -  https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/566928/

Transcription
----------------

Je viens de découvrir cette vidéo sur le site de la télévision israélienne
( https://www.kan.org.il)

elle a été publiée vendredi 14 octobre 2023 vous voyez c'est
le site de la télévision israélienne vous voyez un petit peu quelles
sont les nouvelles aujourd'hui vous voyez l'URL: https://www.kan.org.il/content/kan/kan-actual/p-11894/news-item/566928/
et je vais la partager avec vous parce qu'elle montre ce que signifie
et ce que peut signifier être en guerre et se mobiliser pour se défendre
pour Israël dans un contexte où la guerre n'est pas antinomique de la
recherche des valeurs mais au contraire où la guerre est une nécessité
pour se défendre une ; nécessité qu'il faut mener de façon efficace pour
vivre et ensuite se reconstruire donc est-ce qu'on peut mener la guerre en étant un humaniste
ça semble fou quand on est loin de tout ça mais c'est forcément une
nécessité et donc ce personnage que présente le reportage est Yair
Golan un général un ancien général d'armée qui est maintenant à la
retraite mais vraiment à des très très haut niveaux le reportage le
dit et qui est maintenant réserviste puisque l'armée et la société
civile ne sont pas dissociés en Israël de telle sorte que les personnes
qui sont à l'armée sont en même temps des étudiants des étudiantes, des
mères de famille des pères de famille bien sûr plus les les hommes que
les femmes quand même euh mais des médecins des artistes des personnes
des enseignants des personnes des mécaniciens des personnes de
toutes les strates de la société et donc c'est un peu différent de ce
qu'on imagine quand on est loin donc faisons la connaissance de yirgolan
et donc je mettrai sur pause régulièrement pour vous traduire ce qu'il
[Musique] dit


donc on a vu un activiste israélien de droite lui tomber dans
les bras en disant euh ça y est je veux plus être dans dans la haine je
te dis toute mon admiration et l'image sur laquelle je me suis arrêtée
présente Yair Golan dans sa position de raver knesset de parlementaire au
nom du parti Meret vous voyez des écritures en hébreu et également en
arabe évidemment puisque l'arabe est une langue nationale d'Israël et que
Meretz c'est un parti de gauche son commentaire au journaliste est tu vois c'est
une rencontre qui est pleine d'espoir dit-il ça signifie que les choses
peuvent changer pour quelque chose de meilleur de façon extrêmement
[Musique] rapide

il raconte de quelle façon il s'est engagé dans les
combats et c'est important parce que c'est significatif de ce qui s'est
passé dans la société israélienne où au niveau de la mobilisation et
de l'organisation étatique ça s'est pas bien passé mais au niveau de
la mobilisation individuelle il y a énormément de choses qui se sont
produites de façon assez extraordinaire il dit que tout a commencé
avec un un appel téléphonique de sa sœur qui lui demande de déposer
des gens et ensuite un appel téléphonique d'un journaliste qui est en
contact avec son fils au festival vous avez vu des images du festival au
festival de musique qui est en train de se cacher et qui dit on tire sur nous on
n'arrive pas à s'en sortir le père dit je viens te chercher mais bien
sûr il peut pas lui-même aller chercher son fils et il appelle donc
yair Golan qui qui lui demande d'envoyer la géolocalisation du fils et
il va le chercher avec un certain nombre d'autres personnes c'est comme
ça que l'entraide s'est organisée de façon très  libre

il ny a pas d'héroisme là-dedans on m'a demandé d'aller voir ce qui ce qui se
passait je suis allé voir et voilà et maintenant ce sont des images
qui montrent sa rencontre avec des personnes avec qui il a combattu

et là on voit les alors on a vu les images de bénévoles qui étaient juste
avant complètement en révolution contre l'État compte tenu des gros
gros problèmes de glissement antidémocratique précédent dans l'État
d'Israël donc là on est sur les images des manifestations

et donc il vient de dire que le fait que une organisation de lutte contre
le gouvernement se transforme en organisation de mobilisation et de
soutien et de bénévolat civil c'est une expression de l'État d'Israël
dans le meilleur de lui-même c'est-à-dire avec de la mobilisation la
capacité de se poser et aussi la capacité de se mobiliser

ici il dit que les manifestations sont parfaites pour créer de la cohésion
mais que ça va pas suffire compte tenu de la surdité du gouvernement et qu'il va falloir organiser une
révolte citoyenne de grande ampleur non violente donc on voit qu'ici il
y a énormément de force dans sa contestation ça vaut la peine d'écouter
directement ce qu'il dit là parce que il parle de l'importance de refonder
les valeurs de l'État d'Israël

euh la clé de la sortie de la stupeur c'est le fait de se ressaisir et
d'être capable de redéfinir les valeurs de l'État

quelle société nous voulons construire

et ça c'est avant toute autre chose sa base en terme de valeur

est-ce qu'on sanctifie c'est le mot même s'il a pas les mêmes conotation
en hébreu, la force, la richesse et le pouvoir ou l'honneur

ou est-ce qu'on sanctifie d'autres valeurs l'honnêteté, la droiture, l'égalité
et la justice

c'est ça le cœur cœur cœur de la question à la fin tout revient à la
base des valeurs

le journaliste demande après tout ce qui s'est passé est-ce qu'il y a
encore avec qui parler et il répond je pense pas que maintenant ce soit
le temps des paroles je pense que là c'est le moment de complètement détruire
jusqu'au bout le Hamas

c'est nécessaire non seulement contre le Hamas qui est une organisation
assassine mais aussi pour que le hezbollah le comprenne et pour que
l'Iran le comprenne ici le reportage parle d'une visite qu'il
fait chez les Bédouins qui ont aussi subi de lourdes perteq et un moment
extrêmement traumatisant

il a commencé par dire je veux vous témoigner de mon du fait que je
m'associe à votre deuil et que je suis conscient que la peur est
sérieuse et existante et le Bédouin en face qui il parle dit sa stupeur du
fait que les terroristes soient rentrés et leur ait tiré dessus tout en
voyant qu'ils n'étaient pas du tout habillés comme des Juifs et qu'il
n'en a eu rien à faire et puis ensuite d'avoir
été abandonné par l'État, l'État d'Israël qui a mis longtemps à
venir et ça c'est un point commun dans toutes les situations euh les
organisations étatiques ont été prises de cours mais bien sûr on
comprend que ça raisonne particulièrement pour un village Bédouin qui
est dans une situation particulière qui est pas reconnue par l'état
et bien sûr ça c'est un problème vous avez vu que là c'est moi qui commentais

il écoute l'histoire dde ce Bédoin qui raconte que
il a fait un acte d'héroïsme aussi en en allant chercher les enfants et
en  traversant les balles pour pouvoir les mettre en sécurité pardon
le bédouinoin dit qu'il a sauvé des civils israéliens dans cette histoire
là le journaliste l'interpelle en disant mais est-ce que les Bédouins
ne sont pas susceptibles de se retourner contre Israël et il répond
l'important, la chose cruciale c'est de donner à la majorité
modérée la voix la plus puissante et la et la plus démocratique

et de ne pas donner aux personnes qui construisent une carrière politique

sur ne pas donner aux personnes qui construisent leur carrière sur le
fait d'alimenter la haine à conduire la société israélienne ; donc il
faut les arrêter et donner la force à la majorité qui est bienveillante
et qui veut vivre en paix ; le journaliste l'interviewe sur ce qu'il pense des
dissensions qui affaiblissaient Israël avant la guerre et qui sans doute
continue à l'affaiblir

il dit que premièrement selon lui il faut déjà se concentrer complètement
ensemble pour le fait de gagner cette guerre mais que doit sortir de
cette guerre un Israël différent ; si Israël
n'est pas une démocratie libérale forte si en Israël les Juifs qui sont
la majorité ne créent pas une structure de **judaïme libéral ou humaniste** qui
sache voir l'être humain en tant qu'être humain avant toute chose alors on
perd le point

et je veux que toi en tant que druze tu trouves que ce soit le meilleur
lieu au monde pour toi qu'il n'y a pas mieux pour toi et que tu sois bien ici
en tant que citoyen israélien fier ; de même que le citoyen arabe,
le citoyen bédouin le citoyens chrétien et le minimum
que je dois te garantir pour toute personne qui ne fait pas partie de
la majorité c'est l'égalité

c'est une discussion avec un chrétien de natseret ; le journaliste lui
dit tu as été marginalisé à cause de tes idées mais là tu reçois des
embrassades aujourd'hui est-ce que ça te fait du bien ?
il répond ça enseigne une leçon et cette leçon c'est le fait de s'attacher
à la vérité et de s'attacher aux valeurs qui sont des valeurs porteuses
de santé et c'est mon message dans tout ce reportage pour tous les citoyens
isréliens s'attacher à la vérité et aux valeurs c'est la seule façon de
créer une bonne société à partir de personnes biens

tu reviendras à la politique ?
pour l'instant je porte un uniforme on verra après

Voilà il m'a semblé que ce que disait cette vidéo avait peu de chance
d'être connue de tout le monde et et je suis pas surprise par tous les
aspects mais quand même j'ai appris pas mal de choses moi aussi donc
je voulais la partager avec vous

Voilà donc bien bien sûr la société israélienne est multiple et
tout le monde est pas comme cette personne mais c'est intéressant que à
la télévision israélienne maintenant il y ait la place pour ce genre
de reportage et que tout le monde ne soit pas dans la folie furieuse
mais qui une réflexion sur l'après de la part de pas mal de gens et
ça c'est quelque chose que j'ai vu dans d'autres rencontres également

voilà n'hésitez pas à aller voir le film directement sur la télé
israélienne je le mets dans les commentaires à bientôt on continue en
essayant de faire au mieux de là où on est chacun et chacune.

A bientôt

- https://rabbinchinsky.fr/


