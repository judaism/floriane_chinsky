
.. _chabbat_chalom_2023_10_20:

============================================================================================================
2023-10-20 **Chabat Chalom, des ressources douces et précises…** ❤️🧡💛💚 Parashat Noach 5784 / פָּרָשַׁת נֹחַ
============================================================================================================

- https://rabbinchinsky.fr/2023/10/20/chabat-chalom/

Sauvegarde sur `web.archive <https://web.archive.org/web/20231020160232/https://rabbinchinsky.fr/2023/10/20/chabat-chalom/>`_


Introduction
=============

Cher toutes et tous,

ici un petit mot, un lien pour les rendez-vous avec moi, et des vidéos
de conte, d’étude, de traductions et de réflexions.

Ce chabbat offices avec la Rabbin Ann-Gaëlle Attias,

- 18h45 Pelleport,
- 10h30 Surmelin.


Parashat Noach 5784 / פָּרָשַׁת נֹחַ
----------------------------------

21 October 2023 / 6 Cheshvan 5784

Parashat Noach is the 2nd weekly Torah portion in the annual Jewish
cycle of Torah reading.

Lectures de ce chabbat ici: https://www.hebcal.com/sedrot/noach-20231021?i=on


Après les fêtes de Tichri qui nous ont porté.es, vient le temps du retour
au quotidien.

L’actualité récente nous a hautement sollicitée, et elle va continuer
à le faire.

La michna nous dit (horayot 3:6) : Entre le régulier et l’exceptionnel,
c’est le régulier qui passe en premier.

Notre action régulière est garante de notre survie et de notre équilibre
mental à long terme.
Les sagesses juives nous encourage à considérer chaque acte de vie
quotidienne comme sacré, à prononcer ces bénédictions pour accompagner
notre lever et notre coucher, nos joies et nos peines, notre nutrition
et notre repos du chabat.


Rendez-vous
==============

Pour demander un rendez-vous avec moi, c’est ici: https://framaforms.org/demande-de-rendez-vous-rabbin-chinsky-2023-1696317774


Le cours **Leadership inclusif**
=================================

Cette semaine, nous avons réagi avec courage et créativité à
l’augmentation des risques en terme de sécurité.

Le cours « Leadership inclusif » a eu lieu par zoom ET en présentiel,
chez l’une des personnes inscrites.
Quand la vie devient plus difficile, d’autres portes s’ouvrent.
Restons fidèles à ce principe.

Ressources vidéos
=======================

J’ai également tenu à mettre à votre disposition des ressources vidéos
clairement identifiées pour que vous puissiez les consulter en toute
connaissance de cause.

Les vidéos précédées d’un 💚 sont totalement dépourvues d’allusions à la
situation, pour les moments où vous avez avant tout besoin de douceur,
puis le degré de « difficulté » augmente jusqu’au ❤️, pour les moments
où vous êtes en recherche d’informations plus directes.

Par ailleurs, j’héberge désormais mes vidéos sur une plateforme dépourvue
d’algorithme et de publicité, peertube.

Infos ici https://joinpeertube.org/fr/.

Elles sont également disponibles sur ma chaine youtube,
https://www.youtube.com/@FlorianeChinsky/videos où l’algorithme joue à
plein, **continuez à partager, commenter, à vous abonner et à liker pour
que d’autres voix soient entendues**.

Les vidéos de la semaine
=================================

- ❤️🧡💛💚 `Prévenir pour choisir mettre en place des trigger warning <https://peertube.iriseden.eu/w/hXjawwtAKGZTcDBbhL73iU>`_
- 💚 `Hechvan Femmes et héroïsme <https://peertube.iriseden.eu/w/sVqtGtBV8Zqe1Zwxoj1R9u>`_
- 💚 `Debunkible #1 debunkons la Bible et le personnage de Eve <https://peertube.iriseden.eu/w/vfPz6dEpyyjQxfwDpf9n1o>`_
- 💚 `Celui qui avait toujours raison et voulait le prouver... Un conte talmudique <https://peertube.iriseden.eu/w/57rUrqAwnCBb8oerDQmsN5>`_
- 💚 `Étude Hechvan Femmes et héroïsme <https://peertube.iriseden.eu/w/sVqtGtBV8Zqe1Zwxoj1R9u>`_
- 🧡 `Ça se passe ici #1 cris - sécurité <https://peertube.iriseden.eu/w/cBSsApdWZpKcgi3XRzoT2h>`_
- 🧡 `Comment désamorcer la violence <https://peertube.iriseden.eu/w/eeDi6qsVBp3xhtqcnBFVBV>`_
- ❤️🧡 `Comment combattre tout en restant humain traduction-résumé d'un reportage <https://peertube.iriseden.eu/w/726FRrCqiL4QMVfmnJTJ99>`_
- ❤️ `Ce sont des moments difficiles, nous les traverserons ensemble traduction résumée d'un clip du ministère de la santé <https://peertube.iriseden.eu/w/6Kny181Hxv5geQh3KWVgGz>`_
- ❤️🧡 -Femmes et héroïsme, étude complète des sources
