
.. _desamorcer_la_violence_2023_10_20:

============================================================================================================
2023-10-20 🧡 **Comment désamorcer la violence**
============================================================================================================

- https://peertube.iriseden.eu/w/eeDi6qsVBp3xhtqcnBFVBV


Comment désamorcer les guerres on peut réfléchir d'une façon très très
simple et dire que ce qui permet aux guerres de continuer ce sont les armes
et que donc si on supprime les les armes il y aura pas de guerre et dans
ce cas-là et bien il faut se tourner vers les personnes qui produisaient
les armes les personnes qui les vendent et les personnes qui en tirent
bénéfice et leur dire de cesser et leur dire qu'elles n'ont pas voix au
chapitre pour plaider la cause de la paix alors qu'elles-mêmes soufflent sur
les braises et contribue elles-mêmes à provoquer l'incendie.
--
Toutes les armes ne sont pas concrètes il existe des armes qui sont
abstraites qui sont des armes psychologiques qui jouent un rôle dans
les guerres psychologiques si on atteint quelqu'un psychiquement et bien
cette personne perd ses forces perd sa combativité perd son désir de vie
perd sa liberté et donc c'est quelque chose de très efficace en fait
on peut détruire des populations mobiliser des populations les unes
contre les autres et avancer dans la guerre à travers une guerre
psychologique
--
mon compte en banque n'est associé à aucun producteur ou vendeur d'armes
et de même je souhaite écarter ma personne, ma pensée et ma vie au maximum
de tout trouble psychologique qui servirait la guerre des autres et
même de tout déséquilibre psychique au maximum bien sûr donc on peut
interpeller d'une part les fabricants d'armes et les vendeurs d'armes
et d'autre part on peut interpeller celles et ceux qui produisent des
images violentes en portant atteinte une atteinte renouvelée à la dignité
humaine qu'on attache au corps humain ainsi qu'aux personnes qui divulguent
qui propagebt qui diffusent ces images violentes et peut-être aussi on
peut s'interpeller nous-mêmes quand on s'expose trop à ce type d'image
car en faisant ça on nourrit les armes de la guerre psychologique

je parle ici d'une guerre très très générale de des personnes qui cherchent la stabilité contre
les personnees qui cherchent l'instabilité du camp de du pluralisme
du respect de tous de la tranquillité contre le camp de la violence et
l'enflammement des populations
--
et je pense d'ailleurs que cette guerre a un rapport avec les imageries
qu'on a du masculin et du féminin parce que si on associe la puissance
à la masculinité à la guerre et à la victoire et bien on encourage des
choses extrêmement pernicieuses et si on associe le soin et l'entraide
à la féminité et à la faiblesse on voit bien qu'on est complètement à
côté de la plaque et on entretient une imagerie une vision qui nous dessert
car l'héroïsme ne consiste pas à crier avec la meute et à crier plus fort
que les autres dans la meute l'héroïsme consiste à garder son sang froid
et faire en sorte que la vie soit possible au maximum pour le maximum de personnes
partout où je peux le faire
--
donc la vraie virilité est ce qui attribue au féminin mais tout ça c'est
des histoire qu'on se raconte la question c'est qu'est-ce qu'on défend
dans la réalité j'aime bien ce proverbe juif qui dit qui est la personne
forte c'est la personne qui canalise ses impulsions
--
lorsqu'un feu n'a  plus de combustible il s'étend une des façons de
désamorcer la guerre  c'est ne de ne plus nourrir son feu et de le
laisser s'éteindre donc  on peut prendre soin de nous à bientôt


