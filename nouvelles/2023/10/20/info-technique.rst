
.. _info_technique_2023_10_20:

==============================================================================
2023-10-20 Information technique
==============================================================================

Merci pour toutes ces informations et tous ces précieux conseils.

Pour partager de l'information purement technique, l'adresse de la chaîne peertube
de la rabba Floriane Chinsky est https://peertube.iriseden.eu/c/rabbin_chinsky/videos

Et j'ai appris très récemment que pour sauvegarder des pages web dans les archives internet
on on pouvait utiliser le service "https://web.archive.org/save".

Et donc cette page peut être vue aussi ici: https://web.archive.org/web/20231020160232/https://rabbinchinsky.fr/2023/10/20/chabat-chalom/

