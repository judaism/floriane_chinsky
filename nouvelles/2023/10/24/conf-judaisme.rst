
.. _annonce_conf_2023_10_24:

==================================================================================
2023-10-24 💚 **Qu’est-ce qu’être juif.ve? Comment le devient-on ?** 💻🤷🏼‍♀️🌿
==================================================================================


**Cette question fait l’objet de débats mouvementés !**

Nous évoquerons ces questions le mercredi 8 novembre, de 19h à 20h par visio.

L’objectif de cette conférence sera de mieux connaitre la définition
rabbinique et traditionnelle du judaïsme, de comprendre les dérives
actuelles et en particulier la crispation orthodoxe, mais aussi d’ouvrir
le débat sur les autres modes de définition et le rôle que chacun et
chacune peut y jouer.

Rendez-vous en visio, le lundi 6 novembre, de 19h à 20h, le lien vous
sera envoyé aux coordonnées que vous aurez communiquées dans ce
formulaire: https://framaforms.org/inscription-quest-ce-quetre-juifve-comment-le-devient-on-1698149970
