.. index::
   pair:Lachon Hara ; 2023-10-30


.. _aime_tes_proches_2023_10_30:

====================================================================================================
💚 **Aime tes proches comme toi-même, mode d'emploi + lachon hara**
====================================================================================================

- https://invidious.fdn.fr/watch?v=j49bvIVAmn8


Le principe: aime ton prochain, ta prochaine comme toi-même, Lévitique 19:18
===================================================================================

aime ton prochain ta prochaine comme toi-même au cœur de toutes les
interactions au cœur de tous les groupes sociaux pas facile à
réaliser c'est ce que dit le Lévitique et c'est ce dont nous parlons dans
cette petite vidéo donc ce passage est connexe à un autre qui dit
argumente avec ton prochain et ta prochaine


.. figure:: images/1_aime_ta_prochaine_comme_toi_meme.png


Et pour cela: argumente avec ton prochain, ta prochaine, Lévitique 19:17
=============================================================================


.. figure:: images/2_argumente_avec_ta_prochaine.png


Ainsi on évite
==================

.. figure:: images/3_ainsi_on_evite.png


Concrètement
===============

.. figure:: images/4_concretement.png


Attention
=============

.. figure:: images/5_attention.png


Pour faire simple
======================

.. figure:: images/6_pour_faire_simple.png


La communication non violente
===============================

.. figure:: images/7_communication_non_violente.png


Le principe
==============

.. figure:: images/8_principe_pa_de_lachon_hara.png


Le lachon hara tue 3 personnes
================================

.. figure:: images/9_le_lachon_hara_tue_3_personnes.png


Présentez vos excuses
=========================

.. figure:: images/10_presentez_des_excuses.png


Pas du même monde
=======================

.. figure:: images/11_pas_du_meme_monde.png


Comment réparer
===================

.. figure:: images/12_comment_reparer.png


Ne porte pas la haine
==========================

.. figure:: images/13_ne_porte_pas_la_haine.png





