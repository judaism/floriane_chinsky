
.. _resistance_2023_11_01:

===================================================================================================================
2023-11-01  **Interview sur RFI : Hausse des actes antisémites en France: vivre en paix est une résistance**
===================================================================================================================

- https://www.youtube.com/watch?v=KJ92OmCpr0w&t=3s&ab_channel=FlorianeChinsky


Introduction
================

Des étoiles de David dessiné sur la façade d'immeubles à Paris
des témoignages qui se succèdent qui s'accumulent de membres de
la communauté juive victime d'actes ou de propos antisémite d'autres qui
décident tout simplement de ne plus porter leur kipa dans la rue ou de ne
plus faire leur courses dans des boucheries cachères par exemple voilà
ce qu'expliquent beaucoup de personnes juives en France ces derniers jours
puisque depuis le le 7 octobre 2023 l'attaque d'Israel par le Hamas les
actes antisémites ont explosé ; plus de 850 cas recensés par les autorités
rendez-vous compte en en seulement 3 semaines on a eu plus d'actes
antisémites dans le pays que depuis le début de l'année

on en parle avec l'invité de la France à midi Floriane Chinsky

bonjour bonjour vous êtes Rabin du mouvement juif libéral de France
docteure en sociologie du droit alors madame ma première question elle
est simple

Comment on peut expliquer cette hausse cette explosion des actes antisémites en France ?
==========================================================================================

Elle est pas très compliquée à expliquer et le plus important c'est comment
l'endiguer mais pour l'expliquer on sait que la violence entraîne la
violence donc toute surexposition à la violence risque d'en entraîner
d'autres donc la première chose c'est de calmer la violence ; en soi

et on peut parler clairement de guerre psychologique entre les personnes
nos comportements intérieurs qui font grandir la violence et puis nos
comportements intérieurs qui la font baisser

par rapport à la question plus spécifiquement il y a des fausses croyances
il y a une tendance à essayer de classifier les gentils contre les méchants et je pense qu'il
faut absolument **avoir le courage intellectuel** de réfléchir des situations
de crise extrêmement tristes et pénibles mais avec avec précision sans
rentrer directement dans les gentils les méchants

voilà entre autre


Est-ce que vous constatez cette explosion d'acte antisémite ?
===============================================================

Alors oui de multiples façons et parfois les personnes concernées peuvent pas
faire grand-chose parce que c'est sur leur boîte à lettres donc dans
ce cas-là ce qu'elles peuvent faire c'est les copropriétés peuvent
faire effacer les signes antisémites et ainsi montrer leur solidarité et
leur contribution à la paix et puis parfois c'est compliqué aussi parce
que c'est sur les réseaux sociaux où des gens sont engagés et ont eu
toute une vie et se protéger ça signifierait se mettre en retrait et
donc oui oui je le constate à tous les niveaux il y a il y a beaucoup
de tension de ce côté-là ; appel à prendre soin de soi au maximum

::

    ça devient invivable pour pour la communauté juive en France finalement
    puisque jusqu'à chez soi ; jusqu'à sa boîte aux lettres ;  jusqu'à sa porte on
    peut être victime de cela ?

**Vivre en paix est une résistance**
======================================

oui et de ce point de vue là je veux rappeler
pour tous les juifs et juives de France et puis pour tout le monde que
**vivre en paix est une résistance** le fait de vivre de s'occuper de soi
de s'occuper des autres et dans la tradition juive
c'est aussi quelque chose qui peut être étendu mais quand on mange quelque chose
en disant une bénédiction on dit que **on a le droit de vivre** ;
**on affirme son droit au plaisir et à l'existence** et tant qu'on est en
vie **il faut faire vivre la vie en soi** ; il y a du danger en même temps
il y en a un peu toujours eu

on a une sagesse très ancienne qui
nous dit on est là ; soyons ensemble ; chantez ensemble ; partager et se protéger
au maximum tout en essayant de garder le contact chaque fois que c'est
possible et du côté du des personnes qui sont touchées de façon moins
direct avoir des lieux safe où nos amis juifs et juives peuvent savoir
qu'ils vont pas être interpellé sur la situation israëlo-palestinienne
parce qu'on a autre chose à faire à certains moments que de rétablir les
faits que de trouver des solution on a besoin de protéger nos relations
interpersonnelles ici en France

et justement vous évoquez cela
Etre juif ce n'est pas forcément devoir s'exprimer sur la situation en Israël ou en Palestine

Est-ce que finalement l'antisionisme tend petit à petit vers l'antisémitisme ?
================================================================================

alors la question derrière là ; est-ce que les Juifs doivent ou pas
s'exprimer le problème c'est que c'est extrêmement difficile de garder
le silence parce toute chose nous interpelle sans cesse donc ne rien dire
c'est quasiment impossible mais dire quelque chose c'est consacré tout
son temps et son énergie c'est très très inconfortable

après la question du sionisme c'est
la question de est-ce que les Juifs peuvent à un certain endroit dans le
monde avoir un lieu où nous ne sommes pas minoritaires et je rappelle
qu'il y a un peu plus de 15 millions de Juifs dans le monde plus de 2
milliards de chrétiens plus un peu moins de 2 milliards de musulmans

**est-ce qu'il y a un endroit où on peut se poser et ça c'est l'enjeu
derrière l'existence de l'État d'Israël**


ensuite il y a une difficulté
pour beaucoup de juifs et juives aujourd'hui qui est qu'y a une forte
opposition à des politiques du gouvernement israélien qui sont
voilà je voilà

et en même temps une vraie inquiétude par rapport à la poursuite
de l'existence de l'État d'Israël qui est le seul lieu où le judaïsme
peut s'exprimer en étant pas minoritaire et c'est pas facile
d'être minoritaire


Sur quoi faut-il compter ?
================================

::

    On a évoqué en début d'entretien euh les stratégies à
    à adopter pour faire face à ces actes antisémites certains en appellent
    à une parole de la société civile pour alerter qu'il y ait une prise
    de conscience réelle efficace est-ce que c'est c'est l'une des pistes
    pour faire face à ce fardeau ou faut compter sur la politique, l'éducation
    l'école, les paroles civiles,  sur quoi faut-il compter ?

Alors d'abord d'une façon générale **il faut compter sur soi-même**
il faut se faire du bien à soi c'est ce que disait ilel l'ancien qui vivait en Israël
au 1er siècle c'est ce que disait Elder Camara l'évêque brésilien
**il faut d'abord se sauver soi-même ça c'est la première chose**

ensuite on a à la fois dans le Talmud et dans le Coran une parole qui dit qui
sauve une personne sauve l'humanité entière donc on comprend et c'est
important de pouvoir s'occuper aussi des problèmes et de les répercuter
maintenant la première chose c'est voir nous personnellement qu'est-ce
qui nous concerne et qu'est-ce qu'on sait de première main moi je sais
ce qui m'arrive à moi et je peux en parler avec la personne en face

deuxièmement c'est faire notre job ; quand on a une légitimité d'un
côté ou de l'autre le faire clairement et je cite je veux pas je veux
pas je je vais embêter personne mais sur le site euh France Info hier
il y avait écrit ou avant hier il y avait écrit qu' il y a une soldate
israélienne qui était libérée par le Hamas faut pas quoi enfin pas
faut faut vérifier les faits et les dire bien sinon on s'en sort plus
donc s'occuper de soi, s'occuper des autres faire son job et puis bien

sûr euh renflouer le budget de la santé mentale en France parce que
quand on est en situation dramatique pour pouvoir s'en occuper que ce soit
parce qu'on est juif ou pour autre tout autre problématique qui existe
et ça contribue c'est essentiel pour la paix sociale

pareil pour l'éducation nationale

::

    et et c'est entendu
    merci beaucoup pour ce
    message madame FL Floriane shinskski Rabin du mouement juif libéral de
    France docteur en sociologie merci à vous d'avoir accepté l'invitation
    d' RFI l'invité de la France à midi sur RFI c'est du lundi au vendredi
    et à réécouter quand vous le souhaitez


