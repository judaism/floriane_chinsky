.. index::
   pair: Eric Berne ; Analyse Transactionelle
   ! Eric Berne

.. _alternatives_plus_plus_2023_11_05:

=============================================================================================================================
2023-11-05  💚  **Alternatives / L'attitude ++ (Eric Berne)**
=============================================================================================================================

- https://invidious.fdn.fr/watch?v=DHzKX713Wl0
- https://fr.wikipedia.org/wiki/%C3%89ric_Berne


Eric Berne
============

Éric Berne , né à Montréal (Canada) (sous le nom d'**Éric Bernstein**),
le 10 mai 1910, mort le 15 juillet 1970, psychiatre américain, fondateur
de l'analyse transactionnelle.

Né dans une famille juive émigrée d'Europe de l'Est, il étudie la médecine
à l'Université McGill, puis la psychiatrie à l'université Yale aux
États-Unis, et devient citoyen américain en 1939.

Établi en Californie en 1946, il y élève sa famille tout en menant une
carrière très chargée en psychiatrie (hôpital, clinique, services conseils
pour l'armée et cabinet privé).

Dans les années 1950, Berne est toujours inspiré par Freud, mais prend
ses distances par rapport à la psychanalyse.
Il cherche à développer un outil thérapeutique efficace et rapide, donc
moins coûteux et accessible à tous.
Il met au point des concepts originaux dont il fait état dans plusieurs
articles scientifiques.
Lors de la parution de son ouvrage Transactional Analysis and Psychotherapy,
en 1961, sa théorie fait déjà parler d'elle dans les milieux psychothérapeutiques
et psychiatriques.
En 1964, Berne et ses collègues fondent l'International Transactional
Analysis Association (ITAA), qui existe toujours aujourd'hui.


1. **There are alternatives (TAA) ++**
========================================

- There are alternatives (TAA)
- '**++**'

.. figure:: images/1.png
   :width: 400

2. **Avoir une attitude ++**
================================

Avoir une attitude '**++**' signifie:

- que je me considère positivement '**+**'
- et que je considère **l'autre** positivement '**+**'

.. figure:: images/2.png
   :width: 400


3. **Prenez soin de vous, vous êtes +++**
==============================================

- Prenez soin de vous, vous êtes '**+++**'
- Ne laissez ni les autres, ni l'actualité, s'immiscer dans votre vision
  de vous-mêmes.


.. figure:: images/3.png
   :width: 400


4. **Se considérer + et considérer l'autre + est la seule façon d'avoir une relation**
==========================================================================================

Se considérer '**+**' et considérer l'autre '**+**' est la seule façon
d'avoir une relation:

- **respectueuse**
- **dépourvue d'emprise**
- **sans jeux psychologiques**

.. figure:: images/4.png
   :width: 400


5. **Prenez soin de vous, vous êtes +++**
============================================

- Prenez soin de vous, vous êtes '**+++**'
- Ne laissez ni les autres, ni l'actialité, s'immiscer dans votre vision
  de vous-mêmes.

.. figure:: images/5.png
   :width: 400

6. **Côtoyez des personnes qui vous considèrent +++**
========================================================

- Côtoyez des personnes qui vous considèrent '**+++**'
- Soutenez votre entourage, ils sont '**+++**'

.. figure:: images/6.png
   :width: 400

7. Si vous êtes dans la haine, c'est compréhensible, à vous de le régler
==========================================================================

- Si vous êtes dans la haine, c'est compréhensible, à vous de le régler
- Si vos interlocuteur·ices sont dans la haine, c'est compréhensible,
  c'est à eux et elles de s'en occuper

.. figure:: images/7.png
   :width: 400

8. **Progégez vous, nourrissez le respect**
==================================================

Aujourd’hui, quelles que soient les difficultés que vous rencontrez, face à
toutes les agressions, avec des proches, dans le cadre professionnel,
dans les réseaux sociaux ou les médias

**Progégez vous, nourrissez le respect**

.. figure:: images/8.png
   :width: 400

9. **A nous de choisir**
===========================

- Crédits: Eric Berne, créateur de l'Analyse Transactionnelle.
- '**++**'

.. figure:: images/9.png
   :width: 400
