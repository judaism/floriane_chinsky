.. index::
   pair: Guerrières de la paix; 2023-11-05
   pair: Guerrières de la paix; Hanna Assouline
   pair: Vidéo ; Guerrières de la paix

.. _guerrieres_paix_2023_11_05:

=============================================================================================================================
2023-11-05 💛 **Rassemblement sans slogan ni drapeau Bastille Paris à 18h30 – Les Guerrières de la Paix** 💛 |guerrieres|
=============================================================================================================================

- https://rabbinchinsky.fr/2023/11/05/%f0%9f%92%9brassemblement-sans-slogan-ni-drapeau-a-18h30-le-guerrieres-de-la-paix%f0%9f%92%9b/
- :ref:`guerrieres:guerrieres`
- https://www.lesguerrieresdelapaix.com/
- https://www.lesguerrieresdelapaix.com/forum-mondial-des-femmes-pour-la-paix/

J’ai pu voir des interviews ref:`d'Hanna Assouline <guerrieres:hanna_assouline>`, l’une des deux fondatrices,
et j’admire sa capacité de clarté et de calme, de concentration sur le
message principal, y compris sur des plateaux un peu compliqués

**Ensemble luttons contre les tensions intercommunautaires liées aux
répercussions du conflit israélo-palestinien**.

Agissons sur ce que nous connaissons de façon directe, et non médiatisé
et instrumentalisé par d’autres.


.. figure:: images/affiche.png

   **Ensemble pour la Paix et la Justice en Israël et en Palestine
   ; Unis et solidaires face à la Haine dans notre pays**

Je ne connais pas encore Fatima Bousso, mais cela viendra peut-être.

Je serai présente cet après-midi parce que:

- Je me sens en cohérence avec ce qu’elles disent,
- je veux contribuer à faire entendre ces idées qui sont également les miennes,
- **je suis soulagée de pouvoir me joindre à une autre initiative**.

Peut-être nous y retrouverons-nous….

Bonne semaine et à très bientôt.


.. _floriane_et_hanna_2023_11_05:

|guerrieres| 💚 Floriane Chinsky et Hanna Assouline 2 guerrières de la paix 💚 |hanna|
============================================================================================

- https://www.instagram.com/rabbinfloriane/
- https://www.instagram.com/stories/highlights/17855143584036973/

.. figure:: images/floriane_et_hanna.png

   |guerrieres| 💚 Floriane Chinsky et Hanna Assouline 2 guerrières de la paix 💚 https://www.instagram.com/stories/highlights/17855143584036973/


.. _video_2023_11_05:

Vidéo de Floriane Chinsky le 5 novembre 2023
===============================================

- https://invidious.fdn.fr/watch?v=vvdcLWzVJSo

- A rechercher la vidéo d'avril 2022.
- marche d'octobre 2017

voir:

- https://www.instagram.com/womenwagepeace/
- https://www.youtube.com/@WomenWagePeace/videos
- https://invidious.fdn.fr/channel/UCX3HoKfbGUmrDOOs-ODrWTA
- https://invidious.fdn.fr/feed/channel/UCX3HoKfbGUmrDOOs-ODrWTA
- https://www.youtube.com/watch?v=0qRoGPz1JQk&ab_channel=WomenWagePeace-%D7%A0%D7%A9%D7%99%D7%9D%D7%A2%D7%95%D7%A9%D7%95%D7%AA%D7%A9%D7%9C%D7%95%D7%9D
- www.womenwagepeace.org.il/en/
- https://www.womenwagepeace.org.il/en/summary-of-the-activities-of-women-wage-peace-2022/
