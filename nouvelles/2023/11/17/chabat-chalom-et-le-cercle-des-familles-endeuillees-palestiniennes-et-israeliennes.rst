
.. _chabbat_2023_11_17:

========================================================================================================
2023-11-17 💚 Chabat chalom, et le cercle des familles endeuillées palestiniennes et israéliennes
========================================================================================================

- https://parentscirclefriends.org/

Rencontre avec Youval RaHamim, ici: https://www.youtube.com/watch?v=DhumLgptUf4


💚❤️ Youval RaHamim était à JEM-EST ce chabbat
==================================================

Un témoignage très fort du co-directeur Général des  Familles Palestiniennes 
et Israéliennes endeuillées. Ils et elles travaillent à la paix, malgré le deuil. 

Une inspiration. Le son s'améliore au fil de l'intervention, car au début, 
il y avait des tout-petits qui crapahutaient, et c'est important aussi ;-)

❤️ car il parle d'une situation extrêmement difficile mais 💚pour la puissance 
humaine qui se dégage de son action.

