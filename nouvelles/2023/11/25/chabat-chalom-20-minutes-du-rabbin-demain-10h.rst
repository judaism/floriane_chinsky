
.. _chabat_shalom_2023_11_25:

==========================================================================
2023-11-25 **Chabat chalom, 20 minutes du Rabbin demain 10h**
==========================================================================


Ce soir, nous nous retrouvons à 18h45 pour la BM de Anouk
==============================================================

Alors que nous avons tant besoin de paix et de chaleur, nous nous retrouvons 
ce chabbat pour une BM. Ce soir, nous nous retrouvons à 18h45 pour la BM de 
Anouk les offices ont tous lieu à Surmelin. 


Demain à 10h, les 20 minutes du Rabbin, sur le thème « Qu’est-ce qu’être juif ou juive? »
=============================================================================================

Demain à 10h, les 20 minutes du Rabbin, sur le thème « Qu’est-ce qu’être juif ou juive? »

chabbat Parashat Vayetzei is the 7th weekly Torah portion in the annual Jewish cycle of Torah reading
==========================================================================================================

- https://www.hebcal.com/sedrot/vayetzei-20231125

Les lectures de ce chabbat sont les suivantes: https://www.hebcal.com/sedrot/vayetzei-20231125


Hanouka
========

Hanouka approche, si vous voulez anticiper et vous préparer, voici quelques 
ressources:

- Nouveauté: `Vidéo Hanouka, midrach, antisémitisme et écoute mutuelle <https://www.youtube.com/watch?v=86UQabJDYp0>`_

Les essentiels
------------------

- https://poursurmelin.files.wordpress.com/2013/12/chants-de-hanouka-pour-surmelin.pdf
- https://poursurmelin.files.wordpress.com/2018/11/chants-complc3a9mentaires-de-hanouka.docx

télécharger la feuille de chant en français, hébreu et translittération ici. 

- `Chants de Hanouka <https://poursurmelin.files.wordpress.com/2013/12/chants-de-hanouka-pour-surmelin.pdf>`_   
- `Chants complémentaires de Hanouka <https://poursurmelin.files.wordpress.com/2018/11/chants-complc3a9mentaires-de-hanouka.docx>`_

Vidéos
--------

Vidéos: Voir des vidéos sur les chants pour les apprendre ici. 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `Vidéo d’accompagnement des chants de Hanouka et des chants complémentaires <https://youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_
- `Vidéo Paracha de la semaine et Hanouka ici <https://www.youtube.com/watch?v=ORev_TWE6eg>`_, 
- `Vidéos Boker Tov spécial Hanouka ici <https://www.youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_
- `Vidéos avec des activités pour Hanouka ici (Hanouka mode d’emploi) <https://www.youtube.com/playlist?list=PLnHlXjFx9rOSME-6R48c5YH08Tr43lkiR>`_

Des chants de Hanouka avec les paroles en hébreu dans les sous-titres 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- `vidéo 1 <https://www.youtube.com/watch?v=pg3Be6doSCU>`_, 
- `vidéo 2 <http://youtube https://www.youtube.com/watch?v=c_MUTab2IyM>`_, 
- `vidéo 3 <http://youtube https://www.youtube.com/watch?v=2TDPLFut7V0>`_

Textes: Deux textes sur les origines de la fête 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++=

- https://libertejuive.wordpress.com/2015/11/12/hanouka-universel
- https://libertejuive.wordpress.com/2015/11/26/hanouka-historique/
- https://libertejuive.wordpress.com/2012/12/07/allumage-hanouka/
- https://poursurmelin.wordpress.com/2014/11/10/miracles-hanouka
- https://rabbinchinsky.fr/2019/12/13/appel-hanouka/
- https://rabbinchinsky.fr/2018/12/03/activites-hanouka/

- `Hanouka fête de la lumière universelle <https://libertejuive.wordpress.com/2015/11/12/hanouka-universel/>`_,  
- `Hanouka, fête historique et légendaire <https://libertejuive.wordpress.com/2015/11/26/hanouka-historique/>`_
- Un petit texte de réflexion: `Célébrons les miracles de demain <https://poursurmelin.wordpress.com/2014/11/10/miracles-hanouka/>`_
- un article: `Hanouka, Hareng, Messie et Liberté <https://rabbinchinsky.fr/2019/12/13/appel-hanouka/>`_, 
- Une étude pour réfléchir à notre façon de faire l’allumage: `Texte pour embellir l’allumage <https://libertejuive.wordpress.com/2012/12/07/allumage-hanouka/>`_

`Des jeux pédagogiques autour des bougies, disponible sur ce lien <https://rabbinchinsky.fr/2018/12/03/activites-hanouka/>`_

