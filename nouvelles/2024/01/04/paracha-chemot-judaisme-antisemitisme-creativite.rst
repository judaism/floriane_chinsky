.. index::
   pair: Paracha ; Chemot (2024-01-04)

.. _chemot_2024_01_04:

==========================================================================
2024-01-04 **Paracha Chemot : judaïsme, antisémitisme, créativité**
==========================================================================

.. youtube:: kPI_fWn9u4c

.. tags:: Judaïsme, antisémitisme


Texte
=======

dans ces quelques réflexions nous allons parler de deux questions voilà de
petites questions comme ça:

1) qu'est-ce que le judaïsme ?
2) qu'est-ce que l'antisémitisme ? 
3) quel est le rapport entre les deux et dans ce rapport entre
   les deux pour moi la chose la plus importante c'est la créativité 
   
comment est-ce que tout ça se tisse dans la pensée le judaïsme naît en tant que
peuple dans notre parachat chez mot chez mot les nom le livre de l'Exode dans la
vision chrétienne le livre de chemot le livre des noms dans la vision juive et
donc dans tout le livre que nous commençons à étudier ce shabbat c'est chez
mot c'est les nom c'est la naissance du peuple juif dans le passage de cette
semaine donc nous lisons nous commençons avec le nom de certains Hébreux
et le judaïsme est un système dans lequel on compte les personnes par leur
nom non et pas par leur nombre par leur valeurs créatrices individuellees
et non pas par leur puissance de travail servile donc chaque personne est
une création une créativité originale donc ça c'est peut-être une des
premières définitions du judaïsme la pensée juive voit chaque personne
comme un individu à part entière notre lecture se poursuit par un délire
antisémite paranoïque de Pharaon et le judaïsme effectivement est un système
qui fait face à la folie les autres systèmes euh parce que on vit dans un
ensemble dans une collectivité de système de famille de la terre de système
de pensée et de système politique donc ce délire est immédiatement suivi par
une oppression physique les Hébreux sont réduits en esclavage donc c'est une
oppression physique une contrainte et puis il y a une dimension génocidaire
puisque les enfants hébreux garçons devront être tués à la naissance et
il est sous-entendu que les filles ne comptent pas elles sont de toute façon
annexes et en fait elles seront recruté comme génitrices pour les Égyptiens
à la façon des coutumes anciennes nous racontons donc ensuite l'histoire de
deux femmes de sagesfemmes qui refusent l'autoritarisme de Pharaon et ça aussi
nous donne une vision de ce qu'est le judaïsme c'est-à-dire un mouvement qui
vit par la puissance de liberté de courage et de résistance de tout et tous
et ici par l'exemple de ces deux sages-femmes l'histoire continue et on parle
d'un couple qui se fait et qui se défait et qui se refait et le judaïsme
est constitué comme tous les autres systèmes d'ailleurs de personnes qui
renoncent puis reprennent courage ici bien sûr c'est sans doute le cas
de chacun et chacune mais ici c'est quelque chose qui est énoncé qui est
valorisé et qui est reconnu c'est pas un groupe de personnes qui sont toutes
parfaites et toutes héroïques de façon continue un enfant naît sa sœur
le protège une étrangère le recueil et le judaïsme est donc un système
qui existe par une coopération au service de la vie au service de cet enfant
ici l'enfant sera nommé Moïse de façon beaucoup plus tardive on connaît
déjà toute son histoire quand il reçoit son nom et c'est parce qu'il a
été tiré des os nous dit la torora et donc en R tout enfant est tiré des
EAU sorti du liquide amniotique et de ce point de vue l'histoire de Moïse
et l'histoire de tout et tous l'enfant prend conscience du monde se révolte
s'engage se sent menacé fuit et revient et donc le judaïsme on y revient
est un système dans lequel les personnages héroïques sentent l'injustice
et la refuse la combattent Moïse cependant se fait prier pour revenir il a
conscience de l'immense difficulté à laquelle il il va devoir faire face donc
le système juif est pas un système de pouvoir magique euh mais de conscience
et de prise de risque mesuré et puis euh dans cette mesure il y a aussi une
projection dans le concret on on veut des garanties on veut savoir comment
on va faire les choses et on y réfléchit avant de partir Moïse tel Tamar
devant Juda demande des garanties il se projette dans sa mission il construit
son discours il construit sa mise enenseigne il crée des signes magiques euh
qui vont appuyer son son langage il demande un allié humain qui l'aidera à
formuler concrètement ses demandes ce sera son frère Aaron et il reste aussi
en contact étroit avec un allié absolu la divinité ou ou l'absolu de de la
nécessité de la liberté en tout cas il reste en contact avec quelque chose
d'absolu qui exige de lui le plus haut comportement éthique donc le judaïsme
met en avant une construction précise de l'action au service de la justice
pour reprendre toutes les choses qu'on a dites le judaïsme est une culture
qui met en avant l'action et l'implication personnelle concrète fondé sur
un idéal de justice commun à de nombreuses personnes qui agissent ensemble
de façon libre dans un esprit de coopération sans culte de la personnalité
acceptant les faiblesses passagères et prenant en compte la personnalité
de tout et tous ce système bouscule les visions hiérarchiques bien sûr la
hiérarchie exige la soumission de chacun à son chef elle ne peut tolérer
la liberté de toutes et tous l'antisémitisme c'est exactement ça c'est la
volonté de soumettre la puissance de vie et de liberté qui existe dans le
judaïsme donc résister à l'antisémitisme consiste un à se garder en vie
et deux à se garder en liberté riche des outils de vie que nous donne notre
tradition vous avez vu le rapport avec la créativité la Créativ activité
juive naî de la rencontre entre la grande liberté intérieure et culturelle du
judaïsme d'une part et les grandes contraintes extérieur auquel le système
juif est soumis et dans ce contexte on risque sérieusement d'être broyé et
donc faire face au danger exige de développer des ressources il doit y avoir
des alternatives il faut qu'il y ait de l'espoir there must be another way et
c'est ce que nous avons fait depuis deux ou trois millénaires voici comment le
judaïsme l'anti sémitisme et la créativité sont liés tout ceci ce sont de
grands thèmes traités en quelques brèves minutes j'ajoute des points plus
concret et je traduis une partie de ces textes sur ma chaîne youtube et sur
mon site web qui sont à votre disposition ceci n'est qu'un début de torora
le reste ce sont des commentaires leur étude vous est ouverte
