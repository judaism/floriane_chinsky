
.. _chinsky_2024_01_10:

===================================================================================================
2024-01-10 **Sur un pied 2024 / Paracha Chémot , judaïsme antisémitisme et sororité**
===================================================================================================

- https://www.youtube.com/watch?v=ClzjVOSY_HM
- https://invidious.fdn.fr/watch?v=ClzjVOSY_HM


La section de la Bible de la semaine en trois points, une phrase et une 
question... Répondez dans les commentaires ;-).

Ceci est juste une petite partie de l'Enseignement, le reste, ce sont des 
commentaires, à approfondir...

#paracha #rabbinchinsky #judaïsme #féminisme #sagesses

la parachat chemote les noms dans la traduction hébraïque est la première
parachat du livre de chéot qui veut toujours dire les noms en toute logique
et donc c'est l'angle de vue juif par opposition un angle de vue chrétien qui
appelle ce livre-là l'exode les grandes thématiques sont on peut dire enfin
c'est un choix aller lire vous-même vous voyez évidemment chapitre 1 verset
1 de l'Exode puisqu'on est au tout début du livre jusqu'au chapitre 6 allez
lire vous-même pour voir ce que sont selon vous les thématiques importantes


on a choisi j'ai choisi judaïsme antisémitisme et sororité
=================================================================

mais donc ici on a choisi j'ai choisi judaïsme antisémitisme et sororité et
donc sur l'antisémitisme pharaon craint l'indépendance des Juifs et juives
et décide de les détruire alleer voir le premier discours euh antisémite
de la Bible euh ça reprend vraiment toute cette thématique de la peur de
l'autre et puis judaïsme l'inspiration de Moïse se définit comme un genre
de force des possible vous voyez qu'ici je fais un peu des circonvolutions
autour du mot Dieu qui a pas beaucoup de sens en hébreu puisque enfin on
le verra même davantage dans la parachat de la semaine prochaine mais donc
la force divine moi j'aime bien l'appeler inspiration et donc l'inspiration
de Moïse Moïse euh comprend que l'esclavage ça va pas être possible que
profiter de son bonheur de son côté ça va pas suffire et qu'il va falloir
aller un petit peu s'occuper des autres de ce peuple qui est le sien et puis
sororité et résistance il y a cinq femmes qui conjuguent leurs forces et leurs
talents pour s'opposer à Pharaon donc bah je vais vous raconter un petit peu
leur histoire ici brièvement donc ces cinq femmes qui d'ailleurs ne sont pas
nommé dans le passage sont d'une part la mère de Moïse la sœur de Moïse
les deux sages-femmes qui s'opposent à Pharaon et la fille de Pharaon qui se
m en position de désobéir et de faire en sorte que ce soit la vie qui gagne
alors une phrase une phrase que j'aime beaucoup vatirena hamealdot les mealdot
si c'est un enfant mealdot c'est les accoucheuses batirena ça veut dire elles
eurent une crainte mais comme une crainte révérentielle et vous voyez vous
pouvez comparer avec la traduction que j'ai mis que j'ai pu mettre après
vatirenaaldot Elohim donc vers qui s'exprimait cette crainte révérentielle
c'est vers Ha Elohim qui est une façon de peut-être de nommer Dieu mais
surtout c'est de la racine d'une force et on voit dans ce même passage je
l'ai pas j'en ai pas fait une slide en soi mais mais que Dieu dit que Moïse
sera le Elohim et que Aaron sera le Navi que Moïse sera le Dieu si on veut
choisir cette traduction que moi je trouve inappropriée et que Aaron sera
le Prophète vis-à-vis de Pharaon donc on voit que c'est pas un mot du tout
spécifique à une divinité c'est plus une force ou une autorité donc les
accoucheuses avaient crainte de la force de vie ou def respect de la force
de vie enfin ell mettaient ça au premier plan comme premier moteur de le de
leurs actions vatirenaaldot Elohim vou elle ne fir pasberen comme à parler
vers elle m mraim le roi d'Égypte donc elle refusèr les ordresen et elle
fir vivre la dîme les enfants donc s'il y a une phrase à retenir pour moi
de cette parachat il y en a de nombreuses mais disons que celle-ci est quand
même vraiment puissante et intéressante une phrase de courage de résistance
de sororité et donc la question que je vous propose de vous poser c'est le nom
des cinq femmes qui passent à l'action euh est-ce que vous êtes capable de le
trouver et ça va être très intéressant parce que vous allez devoir chercher
un petit peu parce que c'est dans différents passages que on les trouve donc
mettez ça en commentaire mettez également les références de où vous avez
trouvé le nom de ces cinq femmes donc voilà n'hésitez pas à approfondir
à travers mon site web rabinchinski.fr la chaîne YouTube sur laquelle vous
êtes maintenant à laquelle vous pouvez vous abonner et également mon compte
Instagram euh voilà pour la parachat chez mot shabbat shalom

