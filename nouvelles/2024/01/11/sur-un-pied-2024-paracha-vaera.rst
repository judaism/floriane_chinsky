
.. _chinsky_2024_01_11:

==============================================================================================================
2024-01-11 **Sur un pied 2024 / Paracha Vaéra , #paracha #rabbinchinsky #judaïsme #féminisme #sagesses**
==============================================================================================================

- https://www.youtube.com/watch?v=FawJdzr9EX8
- https://rabbinchinsky.fr/2024/01/12/chabat-vaera/

La section de la Bible de la semaine en trois points, une phrase et une question... 
Répondez dans les commentaires ;-).

Ceci est juste une petite partie de l'Enseignement, le reste, ce sont des 
commentaires, à approfondir...

#paracha #rabbinchinsky #judaïsme #féminisme #sagesses

la parachat vaera cette semaine la deuxième du livre de chemotte l'Exode si
on veut le dire en français en langage disons chrétien et donc on y parle
d'une certaine révélation mais on va voir ce que ça peut vouloir dire dans
le judaïsme c'est très différent de tout ce qu'on imagine a priori et puis
dans les grands événements il y a ladyplay et aussi la promesse d'une terre
de liberté allez voir par vous-même parce que ça c'est les trois grands
thèmes que j'ai sélectionné mais vous serez peut-être d'un avis différent
faites-vous votre propre opinion en allant lire les chapitres 6 à 9 de l'Exode
comme vous le voyez sur la diapositive et donc quelques points importants donc
euh ben on parle d'un Dieu qui se révèle mais alors il se révèle comme étant
celui de la sortie d'Égypte celui de la liberté et de la Libération comme
une force qui inspire Moïse pour le le faire avancer et donc moi pour moi la
façon dont je définiraai Dieu quand je le lis dans la Torah ou dans euh le
les offices c'est je m'adresse à la force qui permet de sortir de soi-même
de se libérer de sortir des préjugés et des enfermements dans lesquels on
peut être et donc ici sur la diapo j' je parle de la force qui inspire Moïse
se présente comme insaisissable et est irréductible une force de vie et de
liberté son symbole est youud he vav he le nom qui ne se prononce pas les
chrétiens sont plus forts que nous parce qu'ils savent le prononcer pour eux
c'est Jéhovah ou Yav dans le monde juif juste on le prononce pas ce qui est
quelque chose de très fort quand même parce que si Dieu existait on devrait
pouvoir l'appeler par son nom quand même donc qu'est-ce qui se passe en tout
cas ça interpelle puis di PL euh qui sont une tristesse selon la tradition
juive on aurait voulu que ça se passe autrement mais il faut bien que les les
Hébreux sortent d'Égypte euh donc voilà création d'un rapport de force par
Moïse et la promesse la force qui a accompagné les ancêtres des Hébreux
renouvelle sa promesse à leur égard promesse de les ramener sur la terre où
ils habitaient avant la grande famine qui les a obligé à quitter l'Égypte
voilà les quelques points importants et puis s'il y a une phrase à retenir
selon moi c'est cette phrase-là euh fait votre opinion encore partagez dans les
commentaires c'est peut-être une autre qui vous parle davantage la rine pour
cette raison est mort livne Israël parle aux enfants d'Israël enfin dit aux
enfants d'Israël Annie adonï c'est moi la force irréductible verotsti trem
et je vous ferai sortir mitat siivlot mitzraim de dessous les souffrances de
l'Égypte vit salti trè mais av vodatam mais mis à vodatam et je vous ferai
sortir de leur esclavage de l'esclavage qui vous impose vegalti et trem et
je vous délivrerai biseri netouya par un alors par un bras étendu ouichpatim
gedolim et par de grands jugements euh voilà donc bon en gros c'est la meilleure
traduction que je peux vous proposer comme ça en quelques instants donc cette
phrase qui a alors je vous donne petit indice pour la question suivante ici
il y a trois verbes dedans hseti hitsalti galti et puis juste dans la phrase
d'après que je vous ai pas mise pour pas trop compliquer il y a encore un
autre verbe qui est la Carti ce qui amène au chiffre 4 et ça ça vous donne
un indice pour la grande question quel est le rapport entre cette phrase et la
fête de pessar la fête de la Pâque juive donc voilà je vous ai donné un
petit indice qui est le chiffre 4 vous pouvez répondre dans les commentaires
ou sur mon compte Instagram donc voilà pour la parachat de la semaine euh
n'hésitez pas aller approfondir sur mon site sur ma sur ma chaîne Youtube
où vous êtes mais il y a de nombreuxes autres commentairirees que vous vouvez
consulter et de toutes les façons qui peuvent vous intéresser ainsi que sur
mon compte Instagram je vous souhaite shabbat shalom
