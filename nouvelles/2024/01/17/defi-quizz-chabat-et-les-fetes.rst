
.. _chinksy_2024_01_17:

==========================================================================================================================
2024-01-17 Défi Quizz ! **Au programme ce mercredi: le Chabat et les fêtes**. Rendez-vous par zoom à 19h
==========================================================================================================================

- https://rabbinchinsky.fr/2024/01/15/defi-quizz/
- https://poursurmelin.files.wordpress.com/2024/01/syllabus-chabat-et-fetes.pdf
- https://poursurmelin.files.wordpress.com/2024/01/syllabus-chabat-et-fetes.docx

Introduction
===============

Vous voulez reprendre les bases en testant vos connaissances ?

Les défi Syllabus sont là pour ça.

Rendez-vous pour 90 minutes de discussion et d’apprentissage autour des
grands thèmes du judaïsme.

**Au programme ce mercredi: Le Chabat et les fêtes. Rendez-vous par zoom à 19h**.

Les liens et documents divers sont accessibles par le groupe télégram,
rejoignez le groupe ici: https://t.me/+uCgYPanU2SA0Nzc8


Questions au programme
=========================

voir ici, sur ce lien en pdf, sur ce lien en word

Le Chabat
----------

1. Que signifie le mot שבת ? A quoi fait-il référence ?
2. Dans quels textes de la Torah le chabbat est-il mentionné ?
3. Quelles sont les pratiques relatives au chabbat selon la torah ?  selon la tradition rabbinique ?
4. En quoi les chiffres suivants sont-ils important à propos du chabbat : 2, 7, 39 ?
5. Quels sont les deux événements bibliques que célèbre le chabbat ?
6. Pourquoi le שבת est-il important ?
7. Citez 4 prières que l’on fait à la synagogue spécifiquement le שבת ?
8. En quoi le chabbat change-t-il la vie de l’individu ? De la famille ? De la communauté ?
9. A quelle heure commence le chabbat ? Comment marque-t-on l’arrivée du chabbat ?
10. A quelle heure finit le chabbat ? Comment marque-t-on la sortie du chabbat ?
11. Quels sont les travaux interdits le chabbat ?
12. Quelles sont les activités prescrites le chabbat ?
13. Quelle est l’approche libérale du chabbat ? Quelle est l’approche orthodoxe du chabbat ?
14. Quels sont les plats traditionnels du chabbat ?
15. Que désignent les mots, traduisez-les : kabalat chabat, léHa dodi,
    kidouch, motsi, Hala, havdala ?
16. Lorsque le chabbat a été institué, au moment de la traversée du désert,
    il y a eu divers épisodes et revirements de situation.
    Racontez. Que nous enseigne cette histoire ?
17. Pourquoi allume-t-on deux bougies le chabbat ?
18. Pourquoi utilise-t-on deux Halot ?
19. En quoi respecter le chabbat nous éloigne-t-il du risque de devenir
    esclaves ou esclavagistes ?
20. Quelles sont les deux bénédictions du Kidouch du vendredi soir ?
    Écrivez-les en hébreu et en français.
21. Comment prépare-t-on le chabbat ?
22. Qu’est-ce que le commandement de הכנסת אורחים ?
23. Qu’appelle-t-on le עונג שבת ? Que faites-vous pour le pratiquer ?
24. Qu’est-ce que la פרשת השבוע ? De quelle façon l’étudiez-vous ?
25. Peut-on prendre les transports publics pour aller à la synagogue le
    chabbat ?
    Donnez des arguments en faveur et en défaveur avant de partager votre
    opinion personnelle.

Les fêtes juives
------------------

1. traduisez les mots : Hag, Hag SaméaH, Yom tov, Yamim noraim, Moèd, Hol hamoed
2. Citez les fêtes bibliques, Citez les fêtes post-biblique anciennes,
   Citez les fêtes modernes
3. Dans quels passages de la Torah trouve-t-on la liste des fêtes bibliques ?
4. Dans quels textes trouve-t-on des références aux fêtes post-bibliques ?
   Qu’est-ce que méguilat taanit ?
5. Quand commence et quand finissent les fêtes ? Quels sont les interdits
   du Yom Tov ?
6. Comment marque-t-on les fêtes à la maison ?
   Comment marque-t-on les fêtes à la synagogue ?
7. Qu’est-ce que le kidouch ? Quel est le nom générique des bénédictions
   du kidouch du soir, celles du kidouch du matin ?
   Précisément, quel est le texte du kidouch du soir de roch hachana et
   du matin de roch hachana ?
   Du soir de PessaH et du matin de pessaH ?
   Du soir de chavouot et du matin de chavouot ?
8. Quel est la bénédiction de l’allumage des bougies pendant les fêtes ?
   Combien de bougies allume-t-on ?
9. Quelle est la différence entre les bougies de chabat, les bougies des
   fêtes bibliques, les bougies de Hanouka, les bougies de Pourim,
   celles de yom hashoa et celles de yom haatsmaout ?
10. Qu’est-ce que le hallel, à quelles fêtes le récite-t-on ?
11. Qu’est-ce que yaalé véyavo et dans quels textes le récite-t-on ?
12. Qu’est-ce que la havdala, et quelle est la différence entre la havdala
    du chabat et la havdala des fêtes ?
13. Quand dit-on : Hag Ourim saméaH, Hag Pourim saméaH, chabat chalom,
    hag saméaH, moadim léssimHa ?
14. Les femmes ont-elles une place égale dans les fêtes juives ?
15. Quelles méguilot sont lues à quelles fêtes ?

Prochaines dates
====================

- mer 31 janv La lecture de la Torah et de la haftara / Les offices,
- mer 7 fevr Synagogue ou famille-ami.es ? / Le rituel personnel, les bénédictions
- mer 28 fevr Tallit/ téfilines/ autres objets du judaïsme
- mer 13 mars Talmud/ Torah écrite et orale / la meguila
- mer 27 mars Littérature rabbinique / histoire juive générale
- mer 24 avr La soirée du seder / histoire des juifs de France et des institutions juives
- mer 1 mai Yom hashoa, histoire de la Shoa / Yom haatsmaout, histoire d’Israël, sionisme
- mer 8 mai cycle de la vie naissance, BM, mariage, enterrements
- mer 22 mai Shavouot / Kacheroute
- mer 29 mai Roch hachana, Yom Kipour
- mer 26 juin Soukot simHat Torah

