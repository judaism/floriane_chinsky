

==========================================================================
2024-02-09 💚 Reproshabbat – paracha michpatim – birkat hamazon 💚
==========================================================================

- https://rabbinchinsky.fr/2024/02/09/reproshabbat/

Ce chabbat est le reproshabbat, chabbat de la liberté reproductive,
#reproshabbat #judaismeinclusif #prochoice; @ncjwinc @jewishwomenarchives.


Sur un pied 2024 / Paracha Michpatim
=========================================

Reproshabbat - chabbat de la liberté du choix d'engendrer et paracha michpatim...

Rearmons nos libertés ! #reproshabbat, chabbat de la défense de la liberté reproductive.

@jewishwomensarchive @ncjwinc
#prochoice #liberté #judaismeinclusif #judaisme

bonjour à toutes et à tous la parachat de cette semaine est la parachat
michpatime et il y a beaucoup de choses dans cette parachat je vous invité
à la lire des choses qui sont faciles à comprendre et auxquelles il est
facile d'adhérer et des choses qui sont plus compliquées donc parmi toutes
les choses qu'on va souligner il y a la question du droit à l'avortement qui
évoqué très brièvement mais on va en parler parce que d'une part c'est un
sujet essentiel et d'autre part aujourd'hui nous sommes le roabbat le shabbat de
la liberté reproductive et donc c'est une initiative internationale dont on va
parler les deux autres thèmes qu'on verra à la fin seront la libération des
esclaves et la question de la responsabilité civile ça fait bien comme ça la
responsabilité civile mais la base dans le judaïsme de cette responsabilité
civile c'est une phrase qui est controversée pas à cause du judaïsme mais
ça on le verra après donc c'est parti on a la paracha michipatime on a vu que
c'est le pro shabbat et donc ici cette slide nous dit la liberté reproductive
la liberté de donner naissance à des futures générations de façon libre
est une valeur juive en particulier le droit des femmes à décider de ce
qu'elles font de leur corps donc initiative du National Council of Jewish
woman c'est ce que vous avez en Baras rel par le jwish woman archives et c'est
ce que vous avez en vous pouvez les retrouver sur Instagram ou en ligne sans
problème si on considère comme le dit la diapoue précédente que la liberté
de choisir de de de son corps et de la façon dont on fait des enfants est une
valeur juive alors on peut s'interroger dans notre parachat si vous regardez
la traduction du rabina vous verrez nulle femme n'avortera et bien ce n'est
pas du tout une injonction et une interdiction d'avorter et la la l'ambiguïé
en fait est d au fait au fait que cette traduction est une traduction ancienne
donc la phrase en hébreu est l' Makela vaaka il il n'y aura pas de femmes qui
subissent de fausses couches et qui soit stérile dans ta terre donc on dit ici
que si on s'occupe bien des conditions de vie des gens et ben tout se passe
bien au niveau de leur santé et de leur capacités reproductive quand il et
elle le souhaitent donc la Torah interdit-elle l'avortement certainement pas
dans cette phrase en mot on dira non en deux mots une interruption volontaire
de grossesse euh il y a un certain nombre de principes qui existent et quand
la femme est en danger et bien il est important de pouvoir mettre fin à la
grossesse et il est approprié de considérer aujourd'hui que une femme qui
n'est pas en mesure d'avoir un enfant est en danger d'avoir un enfant qu'elle
ne peut pas assumer dans de bonnes conditions donc ça c'est mon avis mais
c'est une vision étendue plutôt large mais existante dans le judaïsme du
droit à l'avortement il y a un grand article que je remettrai sur mon site web
n'hésitez pas à aller le voir donc le fait que il n'y ait pas de stérilité on
pourrait le traduire aujourd'hui par la en fait le fait de favoriser l'accès
de toutes les femmes à la procréation médicalement assistée qui nous
permet justement de vaincre la stérilité ce serait la parfaite application
de la phrase biblique qu'on a cité et bah le fait qu'il y ait pas de fausse
couche c'est aussi lié au fait de prendre bien soin de la santé des femmes
autre point important dans la pararachat la libération des esclaves on n'ime
pas le mot d'esclave et ici on a bien raison et ici il faut rappeler que la
condition de ces esclaves dans la Torah est en fait une bonnes condition de
salarié avec un jour de repos l'interdiction de surexploiter les gens donc
quelque chose d'assez respectueux et euh qui enfin il y a pas de prison pour
dette selon la Torat donc donc quand on est en dette et bien si on peut pas
rembourser on accomplit des travaux chez la personne et tous les 7 ans il y a
une année au cours de laquelle toutes les dettes sont remises et toutes les
personnes esclaves sont libérées et puis un autre passage très important
dans notre parchat j'en ai dit un petit mot au début la responsabilité civile
alors le passage dont il est question est très connu on dit œil pour œil
et dent pour dents certains certaines appellent ça la loi du à Lyon et ça
ça reprendrait toute la vision du code d'amourabi ou la vision précédente
le code d'amourabi c'est pas quelque chose de juif et il y avait un proverbe
œil pour œil dent pour dent qui était appliqué de diverses façons dans
le judaïsme il existe dans la Torah écrite et donc il faut l'interpréter et
dans la Torah orale il pose le principe de la responsabilité civile et pas du
tout de la vengeance et il faut vraiment pas tout mélanger ici ni accuser le
judaïsme de vengeance à partir de ça le judaïsme c'est la Torah orale et
c'est la responsabilité civile ce n'est pas une interprétation restrictive
de la T écrite qui poserait soi-disant un principe de vengeance question
pour vous aujourd'hui est donc quels sont les source de la responsabilité
civile dans le dans le judaïsme dans la Torah écrite ou orale accounability
le fait de rendre compte de ce qu'on fait donc bah il y a un petit passage
ici mais il y en a d'autres donc n'hésitez pas à mettre tout ça dans les
commentaires voilà je vous souhaite shabbat shalom et un bon repro shabbat en
cette parachat mich qui veut dire les lois et je vous dis à la semaine prochaine
