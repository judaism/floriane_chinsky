

.. _chinksy_2024_04_23:

=================================================================================================
2024-02-23 **Le dress code et la Torah** et  **Torah et créativité**  vidéos pour ce chabbat
=================================================================================================

- https://rabbinchinsky.fr/2024/02/23/vieo-tetsve/


Introduction
=================

Il y en aura pour tous les goûts ! 5 vidéos sur la paracha de ce chabbat:

- la vidéo « classique » Sur Un Pied, que je fais toutes les semaines, avec les
  thèmes, le zoom sur un verset, une idée et une question Quizz – sur ma chaine
  youtube Floriane Chinsky
- la vidéo « réflexion » sur le thème « le dress code et la Torah » – sur la
  chaine youtube de JEM et en podcast
- Trois vidéos « au cœur des sources » pour approfondir deux questions dans le
  texte même de la torah et des commentateurs en hébreu, avec Rachi, Ibn Ezra
  et le Natsiv :

  - 1 la liberté d’interprétation,
  - 2 les « causes » de l’intervention divine pour libérer les hébreux.

J’ai une demande à vous faire: Commentez mes vidéos
==========================================================

J’ai une demande à vous faire: Commentez mes vidéos. Pourquoi ?

1 – Cela permet de créer des discussions de bon niveau en ligne, d’offrir des
    espaces safe sur les réseaux, de donner une place à chacun et chacune.
2 – Cela met en avant des contenus égalitaires, non sexistes, non racistes,
    en lien avec le judaïsme, utilisons les algorithmes pour mettre en avant
    nos valeurs.
3 – Cela me permet d’être en contact avec vous, d’avoir vos retours sur les vidéos,
    vos souhaits pour les prochaines vidéo, votre avis sur votre vision du judaïsme
    et des textes.
4 – c’est une mitsva ! ensemble, en ligne, nous pouvons produire du limoud et de la
    MaHlokète!

Paracha Tétsavé « au coeur des sources »
==============================================

Paracha Tétsavé « au coeur des sources », régulièrement, je partage des sources
que je traduis et que j’analyse à partir de l’hébreu, **pour permettre à chacun et
chacune de comprendre comment fonctionnent les sources juives**


⛅️ Quel "dieu", quelle "force spirituelle"? Tétsavé sup 2024
================================================================


.. youtube:: _LH-IHwnKno


#liberté #bible #judaisme

L'inspiration, c'est bien.

Mais comment la cultiver? La paracha Tétsavé parle des vêtements, des lumières,
des parfums du Temple et de la formation des prêtres...

L’énergie incroyable de la sortie d’Égypte va devoir se transformer en inspiration
quotidienne.

#liberté #judaismeinclusif #judaisme #tetsave #bible #FlorianeChinsky #secularjudaism #paracha


Paracha Tétsavé : Le Dress code : contrainte ou liberté ?
==============================================================

.. youtube:: INxGlA-1P4o


Créativité et liberté avec les commentateurs/Tétsava 1/3 Rachi
================================================================

.. youtube::  Q8uaxkJy_AE


#liberté #judaïsme #rachi
Pourquoi agissons-nous? Pourquoi la puissance de liberté a-t-elle sauvé les
hébreux d'Egypte? Rachi, Ibn Ezra et le Natsiv ont des avis divergents.

Cela nous permet de comprendre comment fonctionne le rapport au texte dans le
judaïsme.

Attention, cette vidéo est difficile, dites-moi si c'était clair ou non pour vous.
N'hésitez pas à regarder des vidéos plus accessibles comme les vidéo "sur un pied"
sur la paracha ou les "dix mots d'hébreu des offices".

#judaïsme #judaismeinclusif #rachi #penséejuive #judaismeliberal #florianechinsky #commentairetorah #liberté


Créativité et liberté avec les commentateurs/Tétsava 2/3 Ibn Ezra
=========================================================================

.. youtube::  PqlmslECxt4

Pourquoi agissons-nous? Pourquoi la puissance de liberté a-t-elle sauvé les
hébreux d'Egypte?

Rachi, Ibn Ezra et le Natsiv ont des avis divergents.

Cela nous permet de comprendre comment fonctionne le rapport au texte dans le
judaïsme.

Attention, cette vidéo est difficile, dites-moi si c'était clair ou non pour vous.

N'hésitez pas à regarder des vidéos plus accessibles comme les vidéo "sur un pied"
sur la paracha ou les "dix mots d'hébreu des offices".

Créativité et liberté avec les commentateurs/Tétsavé 3/3 Natsiv
==================================================================

.. youtube::  nJ6IRW7BtHk

#judaïsme #judaismeinclusif #natsiv
Pourquoi agissons-nous? Pourquoi la puissance de liberté a-t-elle sauvé les
hébreux d'Egypte?

Rachi, Ibn Ezra et le Natsiv ont des avis divergents.

Cela nous permet de comprendre comment fonctionne le rapport au texte dans le
judaïsme.
Attention, cette vidéo est difficile, dites-moi si c'était clair ou non pour vous.
N'hésitez pas à regarder des vidéos plus accessibles comme les vidéo "sur un pied"
sur la paracha ou les "dix mots d'hébreu des offices".

#judaïsme #judaismeinclusif #natsiv #penséejuive #judaismeliberal #florianechinsky #commentairetorah #liberté


Réflexions sur la paracha: Le dress code et le judaïsme
===============================================================

Quel rapport entre le maquillage et les vêtements des prêtres ? Cette question
peut sembler surprenante ! pourtant, notre tradition apporte plusieurs réponses.

Version audio pour judaïsme en mouvement, version écrite sur mon site web,
version vidéo sur ma chaine youtube et mon insta. Discutez en en famille etc…

La question qui se pose pour nous au quotidien est : Comment nous habillons-nous
pour aller à l’école ou au travail ? Quelle liberté avons-nous ? Quelles
contraintes s’imposent à nous ? Ces contraintes sont-elles explicites
(on nous dit quoi faire), ou implicites (on est censé deviner). Et dans ce
dernier cas, que se passe-t-il pour les personnes qui ne « devinent » pas ?

La question dans notre paracha est la même, a ceci prêt que ces questions sont
appliquées aux prêtres. Comment s’habillent-ils, quelle est leur liberté,
qui prend en charge la fabrication de leurs vêtements ?

Notre paracha, tétsavé, décrit longuement les vêtements attribués au
grand prêtre pour son service ainsi que les préparatifs qui permettront à
toute l’équipe de prêtrise d’entrer en fonction. Nous sommes dans la
xxe paracha de la torah, et le thème, c’est l’apparence au service de
la mission, les vêtements des prêtres au service de leur fonction. Pourquoi
décrire tout ce cérémoniel en détail ? Les vêtements et l’apparence ont
une grande importance personnelle et sociale. Se vêtir est une besoin premier,
pour protéger son corps du froid et des blessures. Cela représente également
un besoin social, nous sommes jugé.es sur notre apparence. Le texte de la torah
ne passe pas ce thème sous silence. On aurait pu imaginer que les prêtres se
débrouillent seuls pour composer leur tenue d’apparat. Ils auraient eu la
charge de la concevoir et de la réaliser. Ils auraient également pu imposer
leur propre style, mettre leurs vêtements au service de leurs propres ambitions.

Mais la torah prévoit la façon dont ils seront habillés. Ils ne portent pas
seuls la charge de réaliser leurs vêtements. Ils n’ont pas le privilège de
décider de leur tenue. Leur rôle professionnel est défini par le collectif
et les couts liés à ce rôle sont pris en charge par le collectif. En ce
sens, revêtir des vêtements magnifiques n’est pas un privilège pour eux
mais un devoir. Leurs vêtements ne sont pas à leur propre gloire, leur but
est de servir leur fonction. Pour répondre aux questions que nous posions,
on peut dire que

1. les prêtres ont-ils la liberté de leur vêtements : non
2. les contraintes sont elles explicites : oui
3. sont elles à leur charge : non

Cette transparence est très importante, et elle justifie selon moi les longs
passages qui parlent des vêtements des prêtres, dans cette paracha et à de
nombreuses reprises dans la torah. Vous y penserez peut-être en cherchant
la tenue appropriée le matin, lorsque vous vous préparez à différents
événements de la vie quotidienne. Comment s’habiller pour un rendez-vous
amoureux ? Pour un entretien d’embauche ? Pour un jour de rentrée des classes
ou pour une soirée d’anniversaire ? Pour un oral d’examen (cravate à la
fac) ?

Avons-nous une liberté ? Les contraintes sont-elles explicites ? Sont-elles
ou non à notre charge ?

Certaines contraintes peuvent être juridiques, puisqu’il est juridiquement
obligatoire de sortir habillé. D’autres se jouent au niveau de l’acceptation
sociale, une femme jugée habillée « masculine », un homme vêtu de façon
jugée « féminine », une personne jugée « cheap » dans son habillement
ou le contraire, toutes ces personnes risquent de rencontrer des préjugés.

Dans nos vies sociales, on peut choisir nos ami.es, qui devraient nous
respecter, nous, aussi bien que notre style vestimentaire. Est-ce bien le cas
? Quelles sont les limites de l’acceptable en société ? Pour une petite
mise en situation, voici le défi que je vous propose : la fête de Pourim
approche, selon le judaïsme, il est socialement admis de s’habiller dans des
vêtements du genre opposé, et de se déguiser de toutes les façons. Ceci est
admis, et même souhaitable, contribuant à la joie de la fête et à notre
capacité d’auto-dérision. Avez-vous des préjugés concernant le fait
de vous déguiser ? d’où viennent-ils ? Dans quelle mesure désirez-vous
les remettre en question à l’occasion de la fête de Pourim, qui approche
? Ceci vous donnera également un indice de l’acculturation que nous pouvons
avoir à la « respectabilité à la française », dont les critères sont
différents des critères juifs traditionnels.

Le conformisme vestimentaire se pose encore plus fortement dans le monde du
travail. Comme pour les prêtres. Notre style vestimentaire professionnel
est jugé. De lui dépend notre capacité de gagner l’argent nécessaire à
notre nourriture et notre logement, la considération dont nous avons besoin
pour travailler. Il faut cirer ses chaussures. La sociologie se penche elle
aussi sur ces questions. La torah prévoit la prise en charge des vêtements du
grand prêtre de façon explicite. Notre société, au contraire, fait reposer
le devoir de se soumettre « volontairement » et d’assumer seul.es la prise
en charge de notre apparence. Ainsi, dans l’article « Discipliner les corps
dans les métiers de production et de service » de 2019 (voir la référence
sur mon site) les autrices soulignent que « Le maquillage exigé des femmes
travaillant respectivement en restauration et en coiffure nécessite du temps
et de l’argent pour acquérir divers produits de beauté, à leurs frais,
et au-delà de ce qu’elles pourraient utiliser à titre privé ». Elles
poursuivent « Si tout.e salarié.e doit mettre son corps en conformité avec les
normes édictées par l’entreprise en termes d’apparence, l’apprêtement
et d’endurance, la réalisation de ce travail esthétique non rémunéré
(ou très mal rémunéré) implique des couts inégaux pour les femmes et les
hommes, à la fois sur les plans économiques et temporels. Financièrement,
le travail de l’apparence attendu des employées dépasse le seul port de
l’uniforme payé par l’employeur (Mathieu) ou d’une tenue jugée correcte
(Denave et Renard). » Pour répondre toujours aux trois mêmes questions,
dans nos sociétés, bien souvent :

1. nous n’avons qu’une liberté vestimentaire limitée
2. les contraintes ne sont pas très explicites, une personne venant d’un autre
   milieu risque d’être discriminée
3. elles sont à notre charge exclusive, sauf port d’un uniforme, ce qui est
   une autre, vaste question.

Pour conclure, quel rapport entre les vêtements du grand prêtre et le
maquillage ? Nous avons évoqué déjà Pourim, son invitation au déguisement,
au maquillage fantaisiste, son invitation à dépasser les apparences à travers
l’abolition des normes sociales vestimentaires et la minimisation du jugement
critique. De son côté, Kipour semble à l’opposé puisqu’on y parle du
cérémonial du grand prêtre comme dans notre paracha. Mais les opposés parfois
se rejoignent. Kipour = yom hakipourim, traduit par « jour des expiations »
mais également par « le jour qui est comme pourim ». L’habillage léger
et sans jugement de Pourim n’est pas moins cathartique et transformatoire que
l’habillage pesant des cérémonies de Kipour. Les vêtements du grand prêtre
ne sont pas plus valables et respectables que les pas de côtes des déguisements
de Pourim. L’éthique juive n’est pas une éthique de la respectabilité des
apparences, mais de l’utilisation des vêtements à des fins de renforcement
des libertés et de cohésion sociale. En nous donnant des indications sur le
dress code, le judaïsme n’impose pas de normes « il faut bien s’habiller,
comme à Kipour » mais des règles du jeu communes : voyons ce qu’on peut
tirer d’un dress code sérieux à Kipour voyons ce qu’on peut tirer d’un
dress code fantaisiste à Pourim, testons notre capacité de distanciation,
explorons nos choix à Kipour, à Pourim, et chaque jour de notre vie.

Deux autres éléments à noter: La fête de Kipour, la plus connue du judaïsme,
propose à toutes et à tous de se vêtir de vêtements blancs. Dans le monde
achkénaze, ces vêtements ont une autre particularité : ce sont ceux que nous
porterons au moment de notre mort. Ici, plus de jeu, ou de « fonctions »,
chacun, chacune, est invité à être présent uniquement pour renouveler
son rapport à la vie, pour soi-même, à égalité devant notre commune
condition mortelle.

La fête de Tou béav est au contraire la fête la moins connue du judaïsme.
Le blanc y est également à l’honneur. Les jeunes femmes qui cherchent à
se marier sont invitée à emprunter des vêtements blancs, pour efface les
inégalités sociales, puis à aller danser dans les vignes en invitant les
prétendants à se présenter, les enjoignant à dépasser les apparences.

Selon vous, ai-je réussi à rapprocher le maquillage féminin aujourd’hui
et les questions vestimentaires des prêtres dans notre paracha ? Avez-vous
des réflexions complémentaires ?

Je serai heureuse de les partager, et je répondrai à vos commentaires sous ma
vidéo youtube et sous mon compte insta.
