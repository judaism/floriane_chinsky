.. index::
   pair: Floriane Chinsky; Sens de la fête : Pourim est-elle la fête la plus importante du judaïsme ? (2024-03-14)

.. _chinsky_2024_03_14:

=============================================================================================
2024-03-14 **Sens de la fête : Pourim est-elle la fête la plus importante du judaïsme ?**
=============================================================================================

- https://rabbinchinsky.fr/2024/03/22/%f0%9f%91%b8%f0%9f%8f%bb%f0%9f%91%91%f0%9f%91%ba%e2%9c%a1%ef%b8%8fpourim-la-fete-la-plus-importante-du-judaisme-cest-demain-apres-midi-et-dimanche-matin/


- :ref:`jjr:pourim_2023_03_06`
- :ref:`judaisme:pourim`

.. youtube:: fBI2akPekhE


Introduction
==============

notre thème c'est Pourim et je voudrais l'aborder sous un angle un
peu particulier et vous poser la question suivante quelle est la fête la plus
importante du judaïsme est-ce que cette fête-là ça serait pas justement
la fête de Pourim

si vous ne deviez garder qu'une seule chose du judaïsme
garderiez-vous Pourim

alors mon avis sur cette question je vous le donnerai
à la toute fin de la vidéo mais ce que je voudrais faire maintenant c'est
voir avec vous cette fête de Pourim quel sens elle a aujourd'hui et quelle
importance et pourquoi on peut effectivement dire que c'est la fête la plus
importante ?

on n'est pas obligé de faire ce choix mais pourquoi est-ce que ce
choix est justifiable et donc c'est cet angle que je voudrais aborder avec
avec vous


Pourim et l'antisémitisme
===============================

- :ref:`jjr:pourim_2023_03_06`


on pourrait faire un un raccourci un raccourci qui nous mènerait
de l'histoire de Pourim à l'actualité et à l'actualité juive au fil
de l'histoire et on dirait que Pourim c'est vraiment une fête très très
juive en allant directement désolé pour ça mais au point Godwin en disant
euh on raconte à Pourim que Aman a réussi à convaincre l'empereur de
Perse de détruire tous les juifs et donc il y a une grande grande peur de
l'antisémitisme à ce moment-là d'une part et puis d'un autre côté il y a
la deuxième partie de l'histoire de Pourim où ce dessein échoue et où il y
a la joie d'avoir survécu donc rapport immédiat extrêmement saisissant et
c'est ce rapport-là que tout le monde attend en général c'est ce rapport-
là qui rapporte le plus de vues sur tous les réseaux sociaux et le plus
d'intensité émotionnelle donc on pourrait aller dans cette direction mais
je conseille pas forcément de prendre ce raccourci

Pourim et le sens de la dérision
====================================

- https://youtu.be/fBI2akPekhE?t=129

une autre raison que je
trouve extrêmement puissante d'aimer Pourim et de se rattacher à Pourim et
de considérer que la fête de Pourim représente l'identité juive et bien
**c'est le fait qu'elle contient énormément d'autodérision** et le fait d'avoir
du recul le fait de pouvoir rire de nos situations compliquées en tant que
peuple à travers l'histoire ou en tant qu'individu c'est quelque chose qui
est très très juif peut-être pas spécifiquement

tout le monde a le droit
de d'avoir du recul et de la dérision mais en tout cas c'est quelque chose
qui compte beaucoup dans le judaïsme donc Pourim réalise cette chose-là à
travers le fait que par exemple on dit des choses mais on veut pas les entendre

qu'est-ce qu'on dit et qu'on ne veut pas entendre et bien c'est le nom de Aman
très très fort c'est-à-dire que la personne qui a eu qui a semblé selon
le récit d'Esther Aman qui semblait avoir le pouvoir de détruire l'ensemble
du peuple on veut réduire son emprise sur nous à néant quand on entend son
nom on fait un petit coup de crissel on on fait du bruit certains imitent des
bruits d'animaux pour qu'on n'entende pas son nom

donc on dit quelque chose
mais on ne veut pas l'entendre on raconte une histoire mais on ne croit pas
forcément à cette histoire et vous verrez soit dans les euh commentaires en
dessous soit sur ma chaîne youtube ou sur mon site je ferai référence à
différents articles qui prouve mais on le sait et on le sent que l'histoire
d'Esther le livre d'Esther n'est pas une histoire véridique mais un excellent
conte à la mode de la façon dont les Grecs percevaient les Perses

mais
vraiment extrêmement bien réalisé donc il y a une chose qui est réelle
c'est l'antisémitisme au fil de l'histoire et puis il y a une une chose qui
est composée c'est ce récit donc on écrit quelque chose non pas pour le
croire mais parce que ça nous fait du bien de le lire d'en rire ensemble et
donc ça c'est quelque chose de vraiment très important dans la tradition
juive

d'avoir de l'autodérision de pas s'empêcher de dire des choses et de
ne pas faire semblant qu'on serait censé les croire je rappelle d'ailleurs
à cette occasion que quand on dit amen ça veut pas dire j'y crois mais ça
veut dire je suis fidèle à ce que ce récit signifie euh dans la tradition
juive ou simplement parfois ça veut juste dire je suis là tu peux compter
sur moi

donc c'est cette deè raison c'est ce recul c'est cet humour et de
façon très concrète ça se traduit par alors d'une part un commandement
euh qui est de faire un festin pour se réjouir et pour se donner se lester
un petit peu l'estomac quand on doit faire face à des choses difficiles le
fait d'avoir bien manger ça ancre ça c'est on le sait dans le judaïsme et
donc un micheté

donc de faire ce micheté et ça c'est une tradition et puis
du côté de l'autodérision il y a donc le micheté c'est un commandement
du côté des traditions il y a le fait de se déguiser à pourim le fait de
boire un petit peu de telle sorte qu'on a l'esprit un peu embrouillé qu'on
arrête de croire que on a raison et qu'il y a qu'une seule vérité de voir
les choses avec un petit peu plus de recul

donc ça c'est des traditions de
Pourim qui se rattachent à l'esprit de dérision du judaïsme et c'est cet
esprit de dérision il est tellement central que la fête de Pourim est
essentielle ça c'est notre deuxième argument

Pourim se faire du bien et faire du bien aux autres
===========================================================

- https://youtu.be/fBI2akPekhE?t=339

un troisième élément qui fait de Pourim une fête essentielle et très typique
du judaïsme c'est que
on mange mais pas que on doit aussi partager donc on se fait du bien à soi
on fait du bien aux autres et on donne l'ensemble de ces actes on l'attribue
pour nourrir une tradition de bien qui dépasse les générations

donc on doit
soi-même manger ça c'est le micheté on vient d'en parler mais on doit aussi
faire deux autres choses on doit nourrir ses amis ça s'appelle michelir Manot
envoyer des petits des petits paniers enfin en tout cas deux types d'aliments
différents pour réjouir nos amis et matanot laiony on doit faire des cadeaux
ou donner de l'argent le jour même de Pourim aux personnes qui sont en
difficulté parce que il y a pas de joie personnelle s'il n'est pas également
une joie partagée et donc là on a vu quatre les quatre grands commandements
Pourim

lire la mégila lire l'histoire de avec son aspect de dérision on a vu
le micheté le fait de manger soi-même on a vu le fait de donner à manger à
ses proches des choses bonnes et le fait de donner à manger à des personnes
pauvres

c'est les commandements vraiment vraiment fondamentaux de la fête de Pourim
donc qui inscrivent l'amour de soi avec l'amour de l'autre

Pourim et halakha
=======================

- https://youtu.be/fBI2akPekhE?t=432

alors vous avez noté
que j'ai parler ici des quatre grandes pratiques centrales de Pourim micht le
festin Mikra megila la lecture de l'histoire d'Esther le michlmanot l'envoi de
de de mets à des amis et le mattin not laonim le fait de faire des dons aux
pauvres et ces quatre choses là s'inscrivent dans un cadre qui date pas d'hier
soir qui date d'il y a très très très longtemps et ce que je veux dire par
là pourquoi je pourquoi je raconte ça

c'est parce que c'est un autre des
éléments essentiels du judaïsme que on réfléchit à la façon appropriée
de faire les choses et que on l'écrit et qu'on parle et qu'on confronte nos
points de vue et qu'on les reconfronte à la génération d'après et qu'il y a
toujours un renouvellement mais ce renouvellement il est sur une base

et cette
base c'est ce qu'on appelle la halakha on dirait ça on dirait on traduirait par
la loi juive mais ça veut dire juste surtout le système de pratique continu
qui se transmet qui évolue mais qui garde toujours un ancrage et qui permet que
quand je me fais du bien à moi et du bien aussi à mes amis et du bien aussi
aux personnes en difficulté en mangeant et en offrant des mets à déguster
et bien en même temps je le fais d'une certaine façon qui va enrichir et
soutenir la façon dont tous les autres juifs et juives d'aujourd'hui euh et du
passé et de l'avenir à leur façon ils le redéfiniront comme ils veulent
vivent ce partage

et donc ça c'est la halakha elle existe aussi à Pourim et
même si elle ne différencie pas Pourim euh des autres fêtes elle est très
représentative et comme elle est présente dans pourim on pourrait garder Pourim

Pourim: se préparer, féminisme et incompréhension
======================================================

- https://youtu.be/fBI2akPekhE?t=550

je voudrais citer trois autres éléments qui rapprochent Pourim des autres
fêtes juives euh d'une part c'est le fait que à travers cette fête on crée
des liens de l'autodérision de la force de la puissance qui nous prépare à
de la cohésion et à de la liberté de penser et à de la discipline d'action
quand on a décidé les choses tous en même temps qui nous permet d'agir dans
les situations compliquées

donc en faisant Pourim on se prépare au pire et
ça ça reflète la situation aussi des sages de la Michna au 2e siècle qui
ont fondé le judaisme rabinique mais voilà il y a cette dimension là ça
c'est un

d'autre part il y a des aspects féministes inespérés et ça c'est
le cas dans beaucoup de fêtes même si c'est pas toujours souligné autant
que ça devrait donc dans Pourim on signale Vachti qui est la première reine
réfractaire qui s'oppose au roi et puis Esther qui va prendre le pouvoir sur
l'ensemble de la Perse tout ça est purement mythologique hein je je je le
rappelle

mais le fait d'avoir des personnages féminins aussi haut en couleur
par exemple est typique du judaïsme où il y a des aspects féministes cachés
et puis souvent euh

et puis la dernière chose c'est qu'il y a des aspects
incompréhensibles par les personnes qui sont ignorantes du judaïsme donc le
livre d'Esther est tellement incompréhensible que quand il a été repris par
la Bible catholique h le personnage de Esther a été modifié et elle s'est
mise à prier par exemple

et donc ça c'est très typique le judaïsme est
transformé à la sauce de ce qu'on peut en comprendre quand on n'est pas juif
très souvent donc voilà si vous lisez le livre d'Esther si vous vous le lisez
la version juive il y aura pas les prières de la reine Esther et mais dans
la version qui a été reprise dans le monde catholique si donc ce qu'on fait
dans le judaïsme c'est souvent incompréhensible quand on n' pas le contexte
et qu'on veut pas faire un pas de côté

Pourim: première fête: 3 preuves textuelles
================================================

- https://youtu.be/fBI2akPekhE?t=678

cette idée que Pourim est centrale
dans le judaïsme peut-être plus importante que toutes les autres fêtes ce
n'est pas mon idée elle a été dite bien avant moi d'une part par le Zoar
et d'autre part par le talmud de Jérusalem et par le midrache

donc le zoard
nous dit à partir de la fête de Kipour et vous voyez ce que c'est Kip pour
Kip pour c'est cette fête où on s'habille en blanc on jeûne pendant 25 heures on
remet en cause tous nos comportements on va à la synagogue même quand on n'
pas l'habitude souvent on appelle ça ce phénomène est important on appelle
ça les Juifs et juifes de Kippour  des personnes qui viennent à la synagogue
uniquement pour Kippour donc fête extrêmement puissante charismatique
solennelle et bien elle serait comme Pourim Kippour ça dit également Yom
k Pourim qui Pourim vous entendez que le nom de Pourim est dans le nom de
Kippour et donc que Kippour est comme Pourim

donc si moi je vais plus loin
encore je dis si qui pourrait comme Pourim c'est que la f importante c'est
Pourim puisque que c'est le centre de la comparaison vous prenez quiip pour
vous savez pas trop ce que c'est c'est facile c'est comme Pourim donc ça met
vraiment la fête de Pourim au centre

et donc il y a d'autres sources qui
citent également l'importance de la fête de Pourim en disant que là je
parle du Talmud de Jérusalem que tous les livres de la Bible sont amenés
à disparaître sauf les cinq livres de Moïse et et la mégila d'Esther et
l'histoire d'Esther

donc vraiment quelque chose d'absolument central ici et
que toutes les fêtes également là on est on est dans le Middrach que toutes
les fêtes sont également appelées à disparaître on est dans le Middrach
sur les proverbes toutes les fêtes sont appelées à disparaître sauf la
fête de Pourim qui ne disparaîtra jamais

donc voilà pour les sources qui
nous parle elle aussi de l'importance de la fête de pourim

Conclusion
================

nous avons vu de
nombreux aspects de la fête de Pourim et réfléchi à la question de son
importance et je voudrais partager maintenant mon avis avec vous dites-moi
si vous êtes d'accord ou non donc pour moi la fête de Pourim est très
importante elle offre un potentiel extraordinaire

mais

car il y a un mais ce
que je pourrais dire c'est que comme cette fête ne se produit qu'une seule
fois dans l'année ça rend difficile son action sur le long terme en terme de
faire évoluer n nos comportements et c'est ce que dit la Michna orayot entre
une chose régulière et une chose exceptionnelle ben peut-être qu'on aurait
tendance à choisir l'exceptionnel

mais en vrai il faut choisir le régulier et
donc peut-être que shabbat gagnerait sur Pourim à cause de cette raison là
cependant euh bah il suffit de faire vivre Pourim d'une façon ou d'une autre
tout au long de l'année avec le la générosité qui est attachée à la
fête

l'autodérision qui lui est attaché et cette dimension de partage et
d'études qui sont très significatives du judaïsme

donc j'adore cette fête
de Pourim je suis contente d'avoir pu partager quelques mots à ce sujet avec
vous je vous souhaite d'excellents préparatifs et je vous dis à très bientôt



Hey Noam,

Believe it or not, Purim starts in two days! Here’s a handy checklist to help
you keep track of everything you need for a fantastic celebration.

- Costume and accessories 🥸
- Gifts for people in need 🎁
- Mishloach manot (gifts of food for friends) 😋
- Hamantaschen 🍪
- Delicious festival meal 🍲
- Grogger (noisemaker) 📢
- The book of Esther (in 11 languages!) 📚
- Purim donation to Sefaria ❤️
