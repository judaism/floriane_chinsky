

.. _chinksy_2024_04_07:

==========================================================================
2024-04-07 🍷🍷🍷🍷PessaH, Libérons-nous ensemble 🗽
==========================================================================

- https://rabbinchinsky.fr/2024/04/07/pessah-5/
- https://poursurmelin.files.wordpress.com/2016/04/hagada-hc3a9breu-niveau-11.docx
- https://poursurmelin.files.wordpress.com/2015/03/pensc3a9es-pour-pessah.pdf

Voici, comme chaque année, les liens vers les outils les plus utiles pour la fête.

Si vous connaissez déjà tout ça, jetez un coup d’oeil à cette playlist qui
explique un midrach peu connu, que j’ai réalisée spécialement pour cette année : https://www.youtube.com/watch?v=7sgj5FrMKUc&list=PLnHlXjFx9rORhvXTnnUi3Wl-A5NtFBzB9&index=1

Si vous débutez commencez par les trois premières ressouces dans la liste suivante.

- 1. `Hagada courte et très simplifiée  <https://poursurmelin.files.wordpress.com/2016/04/hagada-hc3a9breu-niveau-11.docx>`_ que vous pouvez télécharger ici: hagada simplissime à télécharger
- 2. Une vidéo qui vous montre comment utiliser cette hagada et comment apprendre les chants:

  .. youtube:: MsdOVvBgfdk

- 3. Tout savoir sur:

  - `ma nichtana <https://rabbinchinsky.fr/2020/04/03/koulam-17-ma-nichtana/>`_
  - `les quatre enfants <https://rabbinchinsky.fr/2020/04/02/koulam-16-pessah/>`_,
  - `le plateau du seder <https://rabbinchinsky.fr/2020/04/01/koulam-15-pessah/>`_

- 4. Recueil de textes pour enrichir votre seder:

  - Ouvrir la porte à l’inattendu André Néher, L’Exil de la parole p.58 et s.,
  - Description du plat du Seder  Léo Cohn,
  - Alors seulement assez pour nous !       Edmond Fleg,
  - Renoncer au Hamets, renoncer à la violence  Floriane Chinsky, 14 étapes pour une libération  Floriane Chinsky,
  - Tout ce qui me suffit         Floriane Chinsky, Télécharger ce document à imprimer sur ce lien: `pensées pour pessaH <https://poursurmelin.files.wordpress.com/2015/03/pensc3a9es-pour-pessah.pdf>`_

- 5. Texte de la hagada intégral (Bloch) : `telecharger ici http://www.massorti.com/son/documents/Hagada_bloch_francais.pdf <http://www.massorti.com/son/documents/Hagada_bloch_francais.pdf>`_
- 6. Haggada Bloch abrègée: `hagada courte bloch https://poursurmelin.files.wordpress.com/2019/04/hagada-courte-bloch.pdf <https://poursurmelin.files.wordpress.com/2019/04/hagada-courte-bloch.pdf>`_
- 7. Chants en translittération pour que chacun puisse suivre, et chants de
  libération divers (français, anglais, yiddish): `Chants du seder de PessaH https://poursurmelin.files.wordpress.com/2019/04/chants-du-seder-de-pessah.docx <https://poursurmelin.files.wordpress.com/2019/04/chants-du-seder-de-pessah.docx>`_
- 8. Midrach illustré: différentes visualisations des 4 enfants à imprimer
  pour que vos invités de tous âges et de toutes origines puissent discuter
  des différences entres les êtres humains et également de nos points communs: `4-enfants https://poursurmelin.files.wordpress.com/2015/03/4-enfants.docx <https://poursurmelin.files.wordpress.com/2015/03/4-enfants.docx>`_
- 9. Tableau pour compter le Omer et avancer pas à pas jusqu’à Chavouot: `compter le omer https://poursurmelin.files.wordpress.com/2015/03/compter-le-omer2.pdf <https://poursurmelin.files.wordpress.com/2015/03/compter-le-omer2.pdf>`_

- 10. Offices de PessaH à la synagogue:PessaH 1

  - Mercredi 04/04 de 18h30 à 19h15,
  - Jeudi 05/04 à 10h accueil à 10h

  - Torah: préparatifs du pessaH de la sortie d’Egypte
  - Haftara: préparatifs du pessaH de l’entrée en Israël
  - Lecture Biblique : début du cantique des cantiques

Chavoua tov et bons préparatifs!


Domination ou intelligence collective, PessaHim 66a: Introduction
=====================================================================

.. youtube:: 7sgj5FrMKUc

Le traité PessaHim est le traité talmudique qui parle de PessaH.

Nous présentons ici une histoire d'intelligence individuelle et d'intelligence
collective, de savoir et d'oubli, d'arrogance et d'humilité, dans le contexte
de Pessah tel qu'il était vécu à l'époque du second Temple.

téléchargez la feuille de source ici: https://rabbinchinsky.fr/2023/03/20/pessah-talmud/

Vidéos de la série:

- 1 Introduction
- 2 L'histoire
- 3 Le texte en français
- 4 L'argumentation de Hillel
- 5 Le texte en hébreu
