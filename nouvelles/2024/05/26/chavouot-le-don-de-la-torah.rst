.. index::
   pair: Chercheuse; Leora_Batnitzky
   ! Raphaël Drai

.. _chinsky_2024_05_28:

==========================================================================
2024-05-26 **Chavouot… Le don de la Torah !**
==========================================================================

- https://rabbinchinsky.fr/2024/05/26/chavouot-2/


Raphaël Draï
================

- https://fr.wikipedia.org/wiki/Rapha%C3%ABl_Dra%C3%AF


Raphaël Draï, dont j’ai longtemps suivi les enseignements, rappelait
systématiquement une chose, toujours la même : Lorsque nous étudions, nous
courrons le risque de dénaturer notre lecture par nos idées préconçues.

Les lunettes que nous portons influencent notre regard.
Pour comprendre la Torah, il faut mettre de côté nos préjugés.
Certains sont liés au contexte français actuel.


En France, au cours des deux derniers siècles, le judaïsme tend à devenir une religion
===========================================================================================

En France, au cours des deux derniers siècles, le judaïsme tend à devenir une
religion.

La Torah est perçue comme un enseignement religieux, Chavouot comme une
"révélation", une épiphanie.

La pensée juive est tellement minoritaire qu’elle se teinte de pensée chrétienne.

"Si nous ne sommes pas pour nous, qui le sera ?" rappelle le traité des principes,
les pirké avot, dont nous achevons la lecture à chavouot.

La fête du don de la Torah est l’occasion de rappeler la notion centrale du
judaïsme : l’alliance.
Non pas la croyance.
Non pas la soumission à une divinité. Mais bien l’alliance, la co-contruction
d’un enseignement vivant.

Dans le contexte politique actuel, comme dans toutes les épreuves du passé,
c’est cette alliance qui nous a soutenue, car nous la tenons du passé, et nous
la projetons vers l’avenir.

La Torah est bien plus que la "première partie de la Bible chrétienne".

La fête de chavouot célèbre tous les enseignements, écrits et oraux, développés
au cours des siècles, jusqu’à aujourd’hui.


Les cinq sources que je vous propose aujourd’hui
===================================================

Les cinq sources que je vous propose aujourd’hui sont autant d’approches de
l’Enseignement vivant qu’est la Torah, enseignement revivifié chaque année
par nos Hidouchim et nos réinterprétations.


L’année où j’ai vécu selon la Bible de A.J Jacobs
-----------------------------------------------------

L’année où j’ai vécu selon la Bible de A.J Jacobs est ma première recommandation.

Vous y trouverez humour, dérision, critique et profondeur.
L’auteur s’est évertué à vivre pendant un an les règles de la torah écrite.
Ridicule d’un point de vue juif, certes, puisque la torah orale est au cœur
de notre pratique !

Et pourtant, cette expérience par l’absurde est riche et saisissant.

Je vous laisse découvrir le résultat ce cette expérience…

Et pour info, nous aurons à Surmelin un book-club live, précisément autour
de ce livre, justement la nuit de Chavouot, sous la guidance de Elizabeth Castel Korfer.

La loi juive à l’Aube du XXIe siècle de Rivon Krygier
---------------------------------------------------------------

Puisque les pratiques de la Torah écrite sont élaborées au fil du temps, il
est essentiel de les étudier jusqu’à leur réalité contemporaine.

Pour cela, je recommande La loi juive à l’Aube du XXIe siècle de Rivon Krygier.

L’une des formes essentielles de l’Enseignement Oral est la littérature des
Responsa, les chéélot outéchouvot, les réponses rabbiniques aux questions
des juifs et juives de leur temps.
Elles s’appuient sur la Bible (un peu) et sur les décisions rendues dans le
passé (beaucoup).

Nos mouvements libéraux et massorti incluent toute la diversité des sources
historiques actuelles.
Vous voulez savoir comment la Torah se donne aujourd’hui ?
C’est ici que vous trouverez la réponse.

Les responsa, The five books of Miriam : a woman’s commentary on the Torah
------------------------------------------------------------------------------

Les responsa sont orientés vers la pratique.

La Torah écrite est bien plus large. Ses commentaires sont sans cesse renouvelés.
Ellen Frankel a produit un travail formidable, de dialogue autour de chaque paracha,
dans son livre The five books of Miriam : a woman’s commentary on the Torah.

Elle y met en dialogue les personnages féminins du judaïsme, de toutes époques,
un peu comme la michna met en dialogue les personnalités rabbiniques.

Le texte est vivant, moderne, inspirant.

Un judaïsme en mouvement, comme nous l’aimons.

Ainsi, le style littéraire des commentaires de la Torah se renouvelle pour
nous inspirer toujours plus.

Peut-être serait-il temps d’en proposer une traduction en français, pour le
rendre encore plus accessible ?


Decoding the Mishnah
-------------------------------

Cette michna que je viens de mentionner, qu’est-elle au juste ?

Si vous pensez avoir la réponse, je vous invite à reconsidérer la question.
Le document que je vous propose est en anglais et en vidéo.

Mira Balberg examine la michna et la rend accessible.

En tant que spécialiste du développement du judaïsme dans l’antiquité, elle
nous permet de comprendre la révolution que fut pour le judaïsme le passage
de la Torah écrite à la Torah orale.

La recontextualisation historique nous ouvre ses portes à travers cette conférence.
Vous trouverez la vidéo correspondante ici : https://youtu.be/VlS7ZHn4F0I?si=fwxiAvZTYBM8n8xi


Comment le Judaïsme est devenu une religion :  Une introduction à la pensée juive moderne
----------------------------------------------------------------------------------------------

- https://en.wikipedia.org/wiki/Leora_Batnitzky

Pour conclure notre voyage, je vous propose un ouvrage de la chercheuse
Leora Batnitzky qui nous permet de comprendre comment l’image que nous avons
du judaïsme aujourd’hui est influencée par les courants de pensée occidentaux
des deux derniers siècles.

Son livre s’intitule : Comment le Judaïsme est devenu une religion :
Une introduction à la pensée juive moderne. Cette publication de 2011 place
la directrice du département d’études juives de Princeton au centre de la
recherche dans le domaine, et lui vaut la réputation d’être
« la chercheuse en pensée juive moderne la plus incisive et la plus remarquable
de notre époque ».

Ainsi, l’histoire des idées concernant le judaïsme aura également trouvé sa
place dans nos lectures.


Conclusion
===============

Bonnes lectures, bonne écoute, et bons préparatifs pour la fête de chavouot
qui approche.

Qu’elle nous amène force, perspicacité et courage.


La Torah n’est pas une réflexion abstraite, intellectuelle.

Elle est vivante car elle existe au quotidien dans nos vies.
La Torah, et le peuple juif, sont en vie, ici, maintenant, au présent, dans
le réel, Am Israël Haï.

La Torah est au coeur du rassemblement dans le judaïsme (conférence du Rabbin Farhi),
elle représente l’alliance qui nous unit et se renouvelle sans cesse (conférence du Rabbin Chinsky),
elle se met en place de façon concrète et chaque commandement à un sens (suite des études de la Soirée).

Chavoua tov à toutes et tous! שבוע טוב
