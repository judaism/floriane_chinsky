.. index::
   ! Celui qui n’avait pas besoin d’avoir raison… erouvin 13b (2024-06-25)

.. _chinksy_2024_06_25:

=============================================================================
2024-06-25 📜 **Celui qui n’avait pas besoin d’avoir raison… erouvin 13b**
=============================================================================

- https://rabbinchinsky.fr/2024/06/25/%f0%9f%93%9ccelui-qui-navait-pas-besoin-davoir-raison-erouvin-13b/


Si vous avez besoin de vous rafraichir dans ces journées chaudes, dans 
tous les sens du terme… 

Je mentionnerai ce texte demain dans l’émission de Lise Gutmann. 

Bonne soirée à toutes et tous…


.. youtube:: g225awoytv4

Un conte talmudique, tout en douceur et en humour.. 

La fameuse histoire de Hillel et Chamaï et de la voix du ciel qui explique 
qui a raison... 

Quelles conclusions tirez-vous de cette histoire ? Erouvin 13b


.. figure:: images/peste.webp
