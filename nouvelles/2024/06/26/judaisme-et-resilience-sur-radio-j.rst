

.. _chinksy_2024_06_26:

==========================================================================
2024-06-26 Judaïsme et résilience sur radio J
==========================================================================

- https://rabbinchinsky.fr/2024/06/26/judaisme-et-resilience-sur-radio-j/


Judaïsme et Résilience: S’écarter des débats toxiques
============================================================


.. youtube:: d_rF9f2AeK4



Radio J Les invités de Lise Gutmann du mercredi 29 mai : Le rabbin Floriane Chinsky
========================================================================================


.. youtube:: VeQjZ6hoMmk
