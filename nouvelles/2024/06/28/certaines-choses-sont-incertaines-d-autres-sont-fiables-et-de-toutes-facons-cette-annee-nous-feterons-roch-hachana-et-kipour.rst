

.. _chinsky_2024_06_28:

============================================================================================================================================================
2024-06-28 🍎🍯Certaines choses sont incertaines, d’autres sont fiables. 🌈Et de toutes façons cette année, nous fêterons Roch hachana et Kipour… 🎺⚖️
============================================================================================================================================================

- https://rabbinchinsky.fr/2024/06/28/%f0%9f%8d%8e%f0%9f%8d%afcertaines-choses-sont-incertaines-dautres-sont-fiables-%f0%9f%8c%88et-de-toutes-facons-cette-annee-nous-feterons-roch-hachana-et-kipour-%f0%9f%8e%ba%e2%9a%96%ef%b8%8f/


🍎Mon moniteur de conduite, il y a bien longtemps, me donnait le conseil 
suivant:  » Ne regarde pas l’obstacle, sinon tu risques de foncer dedans, 
regarde plutôt au loin, vers le chemin que tu souhaites prendre. » 

🍯Nous faisons face à beaucoup d’incertitudes. 
La vie, c’est le mouvement, et souvent des remous, et les courants violents 
actuels nous laissent dans l’incertitude. 

Cependant, certaines choses sont fiables. 
Fiable, en hébreu, se dit nééman, נאמן,  et amen, אמן a pour fonction 
de dire: « comptez sur moi ». 

Vous pouvez compter sur nous. Cette année, une fois de plus, nous célébrerons 
le renouveau de l’année juive, Roch hachana, nous passerons dix jours de 
préparation et de « remise à niveau » de nous-mêmes, les asseret yémé hatéchouva, 
avant vivre un jour à part, le jour de yom kipour. 

En ce jour, dit-on, nos actes et nos décisions sont jugées, les courants 
qui traversent le monde sont évalué, le monde est réinscrit dans le livre 
de la viabilité, s’il est viable. ( ראש השנה עשרת ימי התשובה כפור )

🌈 En cette veille de Gay Pride, d’élections, de montée de l’antisémitisme 
et de montée des racismes, sexismes et discriminations, nous tenons plus 
que jamais à accueillir toute personne désireuse de se joindre à nous.

🎺Cette année, avec JEM-Est, nous fêterons Kipour au Palais de la Femme 
(roch hachana et les seliHot sont à Surmelin/Pelleport). 

Nous souhaitons vous proposer un Kipour qui vous ressemble.

⚖️Pour ce qui est des aspects « offices » et « étude », je souhaite 
préparer au plus près de vos besoins. 

Pour cela, je vous demande de prendre quelques minutes et de répondre à 
ces quelques questions. Je me dis que peut-être, pour vous aussi, il est 
important de commencer à vous projeter dans ces rencontres du tichri, 
si pleines de force et de sens. Je vous en remercie. 

Chabat chalom, שבת שלום à toutes et tous
