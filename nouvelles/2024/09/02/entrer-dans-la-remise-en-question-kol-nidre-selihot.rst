.. index::
   pair: Kol Nidré; Entrer dans la remise en question : Kol Nidré – seliHot: demain 18h50 (2024-09-02)

.. _chinsky_kol_nidre_2024_09_02:

=======================================================================================================
2024-09-02 ⚖️💻📚 **Entrer dans la remise en question : Kol Nidré – seliHot: demain 18h50** 🍎🍯⚖️
=======================================================================================================

- https://rabbinchinsky.fr/2024/09/02/kol-nidre-selihot/

Le cours de demain soir n’attend plus que vous! 

Préparez vous à renouveler vos connaissances, et aussi à découvrir des 
choses étonnantes, à m’écouter, mais également à participer, à réfléchir 
ensemble, et d’autres surprises pour faire lien ensemble. 

Je suis certaine que vous ne savez pas tout de KOL NIDRE, ce MONUMENT de 
l’entrée dans Kipour, et que vous apprécierez une petite entrée en matières 
concernant les SELIHOT, ces temps de préparations émouvants. 

Ce sont les thèmes de notre rencontre de demain soir en Visio. 

Elle comprendra une partie "nichma", "étudions" et une partie "naassé", 
"agissons", en vertu du célèbre principe (Exode 24:7).
