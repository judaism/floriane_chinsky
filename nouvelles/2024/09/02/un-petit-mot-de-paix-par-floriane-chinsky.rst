

.. _chinsky_2024_09_02:

==========================================================================
2024-09-02 **Un petit mot de paix** 💔🔥🕊 ✊🏼🥰 par Floriane Chinsky
==========================================================================

- https://www.youtube.com/watch?v=Q2qXn4eRBdc

.. youtube:: Q2qXn4eRBdc

   
"Que le lieu vous réconforte parmi toustes les endeuillées de Sion et 
de Jérusalem"

Depuis au moins le 17e siècle, ( chevout yaakov 3:98) le judaïsme formule 
ainsi le souhait de réconfort pour les personnes endeuillées. 

Cette vidéo est une tentative de réaction à une situation déchirante.

Que le 🥰, l' ✊️ et l'📖 unissent et renforcent les acteurs acteurices 
de la paix et de la justice.

Hamakom yenaHem etHem betoH chaat avlé tsion virouchalaim / .
הַמָּקוֹם יְנַחֵם אֶתְכֶם בְּתוֹךְ שְׁאַר אֲבֵלֵי צִיּוֹן וִירוּשָׁלַיִם

#israel #otages #lovelollutte #therearealternatives #judaismeinclusif 
#care #deuil #soutenirlesendeuillees #GreveGenerale #paix #lll #peace 
#StandingTogether #lapaixpourtoustes #lapaixmaintenant #FreeTheHostages


comment réagir quand on apprend une mauvaise nouvelle ou la mort de
quelqu'un dans le judaïsme ? on coupe le coin de son t-shirt 

j'ai déchiré mon t-shirt hier sous ce vêtement je vais le porter cette 
semaine et on dit tu es une source de bénédiction éternelle notre force 
notre guide toi qui es l'orientation du monde Diana m juge de vérité ça signifie que la
vérité c'est qu'il y a la vie et la mort et la vérité c'est que on est
responsable de ce qui se passe dans le monde et c'est également le message de
Rocha Chana donc je me suis demandé ce que je pouvais faire et le principe
du terrorisme c'est de terroriser 

donc la première chose à faire c'est de pas se laisser terroriser quand 
on peut enfin je dis pas que on est égal on n'est pas au même degré de 
confrontation à la situation mais
au maximum et de ce point de vue là de rester fidèle très ancré très
très ancré dans nos valeurs de d'être très précis dans nos choix et dans
quoi on investit notre temps pour que nos actions soient orientées vers la
question de la paix la paix en hébreu ça se dit Shalom et je vais faire une
série de vidéos tous les matins pour parler de Shalom et des autres mots de
paix pour mettre un petit peu cette crage et accompagner la greffe générale
actuellement déclarée en Israël pour grande question aujourd'hui comment
faire changer un gouvernement dans son orientation et voilà c'est là-dedans
qui se lance donc je ne veux pas perdre d'énergie et je vais faire au mieux
pour que toute mon énergie soit canalisée vers le fait de faire avancer
les choses et d'avancer vers la paix et de partager nos outils bonne journée
