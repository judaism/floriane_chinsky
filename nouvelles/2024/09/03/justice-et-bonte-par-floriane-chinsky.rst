

.. _chinsky_2024_09_03:

==========================================================================
2024-09-03 **Justice et bonté** 💔🔥🕊 ✊🏼🥰
==========================================================================


.. youtube:: iyA9yyBtwWU

" Fais advenir avec nous la justice et la bonté pour que nous soyons délivré.es "

Depuis au moins le 9e siècle ( sévère rav Amram Gaon) le judaïsme formule 
son désir de vivre la justice et la justice sociale, la tsedaka צדקה et 
la bonté, le Hessed חסד. 

Cette vidéo est une tentative de réaction à une situation déchirante.

Assé imanou tsedaka vaHessed véhochiénou / עשה עמנו צדקה וחסד והושיענו

#judaïsme #paix #israel #otages #lovelollutte #therearealternatives 
#judaismeinclusif #antisemitisme #care #grevegenerale #peace #standingtogether 
#lapaixpourtoustes #freetheostages
