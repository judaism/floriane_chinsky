.. index::
   pair: Floriane Chinsky ; Paracha Choftim : Tsedek - justice, autodétermination, coopération mondiale (2024-09-05)
   pair: Floriane Chinsky  (2024-09-05) ; Tsedek

.. _chinsky_2024_09_05:

=============================================================================================================================
2024-09-05 **Paracha Choftim : Tsedek - justice, autodétermination, coopération mondiale** par Floriane Chinsky |floriane|
=============================================================================================================================

- https://rabbinchinsky.fr/2024/09/06/%f0%9f%95%af%f0%9f%95%af%f0%9f%92%90chabbat-choftim-%f0%9f%93%9cvideo-et-podcast-inscriptions-pour-kipour-%e2%9a%96%ef%b8%8f/
- https://www.youtube.com/watch?v=PAfLVUIp440
- https://podcasters.spotify.com/pod/show/judasme-en-mouvement1/episodes/Paracha-Choftim--Tsedek---justice--autodtermination--coopration-mondiale-e2o1btj
- https://www.calj.net/parasha


2024-09-05 **Paracha Choftim : Tsedek - justice, autodétermination, coopération mondiale** par Floriane Chinsky
==================================================================================================================

- https://fr.wikipedia.org/wiki/Shoftim_(parasha)
- https://www.sefaria.org/Judges

.. youtube:: PAfLVUIp440

Bonjour à toutes et à tous la paracha de cette semaine s'appelle Choftim
et elle parle des juges et les personnes qui jugent c'est dans le cadre
de l'exercice de la souveraineté d'un État et cet état n'est pas un
état nation puisqu'on est à l'époque de la Bible mais par contre c'est
une entité territoriale sur laquelle s'exprime **l'autodétermination** des
personnes qui des des Juifs et juifes de l'époque et ce que dit la Torah
c'est que cette autodétermination doit se faire dans la justice alors c'est
intéressant on va voir que ça fait écho avec l'actualité 

nous sommes dans le Deutéronome dans le chapitre 7 au verset 20 c'est ce verset que je
voudrais partager avec vous et vous lire donc Tsedek ti d'OF il y a deux
fois le mot de cedc c'est la justice la justice pourseli-là vous y imaginez
bien que les commentateurs ont interprété ce redoublement et je pense pas
qu'on aura le temps d'approfondir ça tout de suite la justice la justice
tu poursuivras et le verset poursuit les Maan tirié pour que tu vives Ve
et que tu puisses exercer souveraineté sur la terre Acher adonï et la que
l'Éternel ton guide ta force ton Dieu notè les te donne donc ce qui est
intéressant dans ce verset c'est que il y a la notion d'indépendance sur
une terre et ça c'est pas surprenant parce que toute la Torah est tourne
autour de ça et beaucoup des commandements de la Torah font partie de ça et
tous les commandements d'organisation et d'équilibre des pouvoirs politiques
qu'on trouve dans notre paracha sont liés à ça et tous les commandements
également de partage de la terre de 7e année et on on permet le repos de
la terre et le partage de toutes les ressources sont liés également à
ça donc il y a cette notion de souveraineté qui a attaché une notion de
justice pour que cette souveraineté existe il faut qu'il y ait de la justice


.. _chinsky_tsedek_2024_09_05:

Donc on imaginerait je dis ça je dis rien que s'il y avait une association qui s'appelait Tsedek ...
=========================================================================================================

donc on imaginerait je dis ça je dis rien que s'il y avait une association
qui s'appelait Tsedek **et qui défendait la justice en utilisant ce mot en
hébreu elle serait consciente que dans ce verset la justice est étroitement
liée à son exercice souverain sur une terre qui est la terre de Canaan 
correspondant aujourd'hui à la terre d'Israël donc par exemple une telle
association aurait du mal à dire que le combat antiraciste ne peut-être
qu'antisioniste** hypothétiquement, si ça existait 

et on voit ici que le judaïsme a un côté particulariste c'est-à-dire que 
on a besoin d'un endroit où on est chez nous et on exerce nos responsabilités d'une façon ou
d'une autre tout le monde a besoin d'une maison et je pas plus les Juifs que
les autres bien sûr mais pas non plus plus les autres que les Juifs et juives
voilà quel que soi ses ses maisons mais puisque la réalité actuelle du monde
et que les États nations représentent des droits à l'autodétermination
c'est dans ces termes également que le judaïsme a besoin d'un endroit
où exercer sa liberté et ou prendre ses responsabilités et c'est pour
ça qu'il faut un côté particulariste parce que si je n'ai pas de maison
il y a pas d'endroit où j'exerce ma responsabilité il y a pas d'endroit
où on peut m'interpeller sur la façon dont je vis donc c'est absolument
normal il est très facile de se proclamer au service de grande valeur
qu'on a jamais l'occasion de mettre en place parce qu'on a pas de lieu où
on exerce sa liberté donc ça c'est quelque chose de fondamental 

d'un autre côté le judaïsme est pas seulement particulariste il est aussi conscient
que il s'inscrit dans l'humanité c'est pour ça qu'on dit notre Dieu notre
force parce qu'on a notre façon propre d'envisager la force de vie qui nous
soutient mais qu'on dit aussi roi du monde dans chacune des bénédictions
qu'on prononce tu es une source de bénédiction éternelle notre Dieu roi
du monde et l nous MAM donc il y a cette dimension universelle permanente et
elle est extrêmement d'actualité en ce moment puisqu'on est on vient juste
maintenant de rentrer dans le mois de Ellul qui est le mois de préparation
à au début de l'année juive et le début de l'année juive c'est pas
la commémoration de la naissance de Moïse ça aurait pu en théorie mais
non c'est la commémoration de la création du monde parce que on a besoin
d'être ensemble le monde entier peut continuer par parce que suffisamment
de bonnes choses s'y font et que les bonnes choses que l'humanité réalisent
permettent de surpasser les erreurs que nous commettons et donc le monde
continue à exister 

le nouvel an juif n'est pas une fête débridée c'est
une fête c'est très joyeux mais c'est avant tout une fête du bilan et du
rééquilibrage du monde et au cours de cette fête on considère que c'est
la la force de vie elle-même la force créatrice qui est chauffette col
qui est juge de l'ensemble de la terre donc vous voyez que dans notre verset
dans notre paracha on dit qu'il faut des chauftimes des juge dans une terre
spécifique qui est la terre d'Israël et que dans l'histoire de rashashana
on dit qu'il faut un juge et là on parle d'un juge universel d'un équilibre
des forces du monde m qui se réfère à une terre qui est la globalité de
la terre en disant cela ce que je veux dire c'est que H l'État d'Israël
doit assumer ses responsabilités par rapport à la justice qui est attendu
de lui du point de vue des valeurs initiales même qui ont présidé à sa
fondation et qui préside à cet espoir bimillénaire des juifs et juives
de toute époque qu' existe un état qui réponde au verset que qu'on vient
de citer respecte poursuit poursuit la justice parce que c'est ça qui fonde
ta possibilité d'être dans l'autodétermination mais ce qu'on dit également
en revenant au sens de la fête de rochashana c'est que l'équilibre du
monde ne peut pas reposer que sur la conscience juive non plus les Juifs
dans le monde c'est 0,2 % de la poulation en terme d'équilibre global
c'est pas possible de faire reposer l'avenir du monde uniquement sur la
conscience juive bien évidemment que à un face à 200 s si les 200 sens
sont antagonistes et accusateurs on risque de prendre des positions qui qui
seront pas des positions fortes pour le monde et qui pourrai être même des
positions autodestructrice parce qu'à 200 contrein on croit même plus qui
on est on se croit même plus soi-même c'est c'est peut-être pas une règle
mais en tout cas c'est une possibilité et on peut comprendre ça je dirais
donc que à côté du teddeec qui est la justice il y a la atsdaka qui est la
justification et je dirais l'autojustification et les fêtes de ticherie sont
le contraire de ça c'est la techouva c'est revenir sur nos autojustifications
et nous remettre en question et c'est peut-être l'occasion d'une façon
générale pour toutes les personnes juives ou de réfléchir à ce qui
contribue à l'équilibre du monde aux pressions qui s'exerce sur nous et
la façon dont nous les retransmettant éventuellement et je finirai avec un
extrait de du très beau poème de Fleg qu'on lit régulièrement à pessar
dans nos communautés qui s'appelle alors seulement assez pour nous autour de
du texte d'ayenous en formulant le souhait que ce ne soit pas que du point
de vue juif que nous nous intéressions à ça mais que d'autres personnes
partout dans l'humanité puissent également s'identifier et s'investir dans
la phrase suivante  Fleg dit tu nous as sauvé des chars de Pharaon des
pâes de nabucodonosaur et cetera et c'est encore trop peu pour nous et il
poursuit lorsque rendu dès longtemps aux fraternités des peuples nous
aurons fait avec toi et avec tous les hommes sur toute la terre toute la
paix dans toute la justice alors seulement éternel notre Dieu roi du monde
assez pour nous ce que nous souhaitons à titre particulariste à pessar
nous le souhaitons à titre universaliste à rochashana qui est une fête
de rassemblement pour l'ensemble de l'humanité et d'examen de conscience
pour les Juifs à cette occasion mais dans notre dans la conscience juive
c'est une invitation pour que tout le monde tout et tous puisse faire son
examen de conscience et rajouter au bien aux bienfaits aux actes positifs qui
permettront au monde de poursuivre leur chemin jusqu'à l'année prochaine

Hersh Goldberg-Polin
=========================

- https://en.wikipedia.org/wiki/Kidnapping_and_killing_of_Hersh_Goldberg-Polin

On a beaucoup entendu parler de herch Goldberg Pauline et un peu moins des
cinq autres otages qui ont été assassinés par le Hamas cette semaine.

**Il était un militant de la paix et de la justice** et je souhaite accorder
une pensée particulière à ces six otagees en conclusion de cette petite
étude que ce soit la justice et l'intelligence qui nous guide 

shabbat shalom


2023-08-30 Paracha Choftim : Obéir à sa conscience ou obéir aux institutions ? par Floriane Chinsky
===================================================================================================

.. youtube:: hwDQKuYqfJs

devons-nous obéir à la justice avec un grand j à notre conscience ou
devons-nous obéir à la justice avec un petit j la justice déterminée par
les institutions et le jeu politique c'est une question très importante et
on aura tendance peut-être à répondre un peu les deux ou ça dépend bien
sûr il est essentiel du point de vue juif de se doter d'une loi et d'une
constitution enfin la pensée juive considère que l'humanité a besoin de
règles dans une certaine mesure et c'est même une des 7 lois de Noé c'est
également le principe qui est affirmé dans notre pas à ça de la semaine
sauf team chauffaite ça veut dire un juge chauffe-team ça veut dire les
juges et notre paracha de la semaine nous dit choufteam vechotrim titane
les rats nous sommes dans le cinquième livre de la Torah qui s'appelle le
Deutéronome ou de varim nous sommes à la cinquième lecture qui s'appelle
donc chauve-steam et on nous dit des juges et des policiers tu te donneras
et rachis note et je pense que c'est intéressant de le souligner que selon
ce verset on en déduit que les policiers sont servis de la justice donc du
pouvoir judiciaire et non pas au service du pouvoir exécutif le pouvoir
judiciaire avec le pouvoir exécutif et le pouvoir législatif sont les
trois pouvoirs que l'on différencie qui doivent être séparés pour qu'on
puisse prétendre vivre en démocratie donc à examiner c'est ça la question
de notre paracha et par opposition si le pouvoir législatif exécutif et
judiciaire s'ils sont mélangés alors on considère qu'on n'est pas dans
une démocratie mais qu'on rentre dans la dictature bien sûr c'est un
continuum on essaye d'être le plus possible dans la démocratie et dans
la liberté et le moins possible dans la dictature on pourrait dire aussi
que la démocratie cette un régime qui réussit qui devrait réussir qui
a pour objectif que la grande justice interne personnelle cette aspiration
universelle ce besoin que nous avons tout et tous au niveau individuel soit
reflété et traduit dans les institutions par la justice avec un petit j de
telle sorte que les deux soient compatibles parfois ce n'est pas le cas de
la façon la plus dramatique citons le régime de Vichy avec la Shoah et les
lois de ségrégation contre les Juifs puis de déportation et de destruction
massive et dans ce cas obéir à la justice avec un grand j était opposé
à obéir à la justice humaine telle qu'elle était définie par des jeux
politiques extrêmement pervers et dramatique donc notre paracha parle de la
justice avec un petit de l'organisation telle qu'elle devait être et à cette
occasion rappelons que il ne faut pas laisser traiter ce régime antique juif
de théocratie car il y avait une séparation des pouvoirs à cette époque
entre vous le verrez dans notre paracha dans la suite à partir du chapitre
16 du Deutéronome d'une part il y a le pouvoir judiciaire avec lequel on
commence et ensuite il y a le pouvoir exécutif avec un roi qui lui-même
soumis à la loi et puis il y a un certain nombre aussi d'autres institutions
je cite au passage les prophètes qui représentent une liberté d'opinion
et un contre-pouvoir tout à fait important dans le régime constitutionnel
hébreu antique donc quand on parle de théocratie à propos de ce régime
c'est faire des amalgames inacceptables et des erreurs dangereuses du point
de vue de l'astigmatisation du judaïsme comme une religion oppressive
etc etc alors qu'en est-il de la justice avec un grand J et bien on est
toujours dans le même vocabulaire notre paracha c'est chauffe-team mais
un des éléments sur lesquels on insiste au tout début du calendrier
juif là bientôt on rentre dans le mois de eloule dans deux jours jeudi
soir et vendredi matin le 17 et 18 août on est en 2023 et donc on rentre
dans le dernier mois de l'année qui nous prépare à ticherie et tout le
mois de ticherie avec roche Hachana Yom qui pourrait aussi les fêtes qui
se poursuivent après on considère que on se rattache à l'incarnation de
la justice avec un grand j Yom hadine le jour du jugement le jour où nous
sommes jugés par rapport à la force créatrice du monde à l'Éternel à
la transcendance est-ce que vis-à-vis de notre conscience et de la justice
avec un grand j nous sommes au point et le très beau poème liturgique
que vous entendrez les deux premiers matins de Rosh Hashana et le matin de
Kippour chauffe-et-cola arrête souligne cette question là chaufferette Cola
arrête le juge de toute la terre c'est-à-dire qu'il y a une transcendance
qui dépasse les contingences humaines donc c'est Dieu et alors donc qu'est-ce
qu'on n'est pas revenu à une théocratie et à une imposition religieuse et
bien notre tradition vraiment vraiment nous invite à ne jamais rester dans
les apparences à aller chercher plus profond dans les textes et nous allons
ainsi boucler la boucle autour du mot chauffette avec un verset qui est dans
la Genèse qu'on dira en fait dans pas très longtemps dans le chapitre 18
le verset 25 ou Abraham s'oppose à Dieu en disant le juge de toute la terre
a chauffé de la arrête Loya assez michepatte est-ce qu'il ne fera pas le
droit le juge de toute la terre doit faire ce qui est juste c'est à dire
que même le juge de toute la terre si un dieu qui existent une transcendance
personnalisée pourquoi personnaliser la transcendance mais pourquoi pas mais
si cette personnalisation existe alors cette personnalisation elle-même est
soumise à la justice avec un grand j aux Abraham et comme Abraham le dit
et bien cela nous indique que chacun chacune d'entre nous individuellement
sommes garants et garantes que l'idée qu'on se fait de la transcendance
obéissent elle-même à la justice avec un grand j donc la question du jour
est la justice très très concrète autour de nous dans les pays où nous
vivons la gouvernance et la séparation des pouvoirs à travers notre paracha
et leur articulation avec le lien personnel que nous avons à la justice en
général et qu'est-ce qui est juste et qu'est-ce que nous voulons pour nos
vies et comment nous exerçons notre responsabilité vis-à-vis de nos vies
qui est le calendrier de roche Hachana et de Yom Kippour dans un mois ça
commence retrouvez les sources que j'ai citées sur mon site rabbinh s'inscris
point fr rejoignez le petit office du matin avec les femmes du mur par zoom
vendredi matin pour soutenir ce renouveau du mois de Hello pour vous-même
et pour cette réflexion et ce militantisme pour l'équilibre des pouvoirs la
liberté la liberté des femmes la liberté des femmes juives y compris dans le
contexte de démocratie en danger qui est en Israël shabbat shalom et rodestov


2022-08-29 Paracha choftim : les conséquences de la haine par Floriane Chinsky
====================================================================================

- https://www.youtube.com/watch?v=4MTaemtf1_s&list=PLnHlXjFx9rOR31vysZ3ChRMSbrUNnnKN2

.. youtube:: 4MTaemtf1_s

Bonjour à toutes et tous quelques mots sur la paracha de la semaine qui est
la paracha de Choftim mais surtout sur un thème qu'elle traite et
qui est important qu'ils le thème de la haine et de ses conséquences le
premier verset que nous les lisons en lecture triennal et dont tiré 2 du
chapitre 19 du livre de variment du deutéronome et donc vous le voyez ici
sur l'écran tel que je vais repris à partir du site c'est faria very yelich
sonné l'airé héroux et quand il y aura une personne un homme une personne
qui est son prochain v un rêve l'eau et qui le guette v kamal av ec il se
lève contre lui weil cas où nefesh va mettre et qui le frappe à mort et
qu'il meure veine as elle rate et à rim à elle et il sait qu'il s'enfuie
s'il se produit qu'ils s'enfuient vers l'une de ces villes l'une de ces villes
l'une des villes refuges dont il est question dans notre page à la suite du
texte nous dit que si tel est le cas il faut faire sortir la personne de la
ville refuge et la faire passer en jugement car les villes refuges sont là
pour protéger les personnes qui ont commis des meurtres par erreur et par
accident ils doivent s'exiler pour ne pas provoquer susciter la haine des
proches du défunt et pour aussi marqué leur séparation remarquant les
mets pas en prison c'est intéressant mais on les envoie dans des villes
refuges on leur demande de se séparer de la société jusqu'à la mort du
grand prêtre mais ça c'est une autre histoire le verset que nous avons
là nous parle dans une personne qui est son prochain qui le guette qui se
lèvent contre lui qui le frappe contre lui ou elle le ou la frappe à mort
et la personne meurt et dans ce cas là il faut absolument la faire passer
en jugement mais si on regarde les commentaires et je voudrais présenter
pour vous un commentaire qui s'appelle à des raids eliahou adhérait tilia
où ça signifie le manteau délia où vous voyez ici le titre du texte donc
vous voyez à des raids eliyahu sur notre passage et donc il reprend un petit
peu le commentaire de rachi à des raids il y avoue c'est un commentateur
un penseur et un décisionnaire qui est né en 19ème siècle et en europe
de l'est et qui ensuite est parti à jérusalem voilà donc voilà ce qui
nous dit je vous le fais sur du doigt verrier ich sonnés les réer ou v
un rêve et c'est vers où l'évêque baumert c'est cette abréviation là
je veux dire v go mer mikan de là on apprend chez hymne à valtat avait à
cala là je pense qu'ils me fautes d'orthographe je pense que c'est qu'elle
aille pas cana que c'est ce que ce que confirme la comparaison et crèches et
aussi que si tu as comme une transgression légère et là on peut se poser
la question c'est quoi cette transgression les jarres basse et le début ici
werich sonné l'airé où c'est le fait de haïr son prochain alors moi je suis
pas sûr que ce soit une faute légèrement même temps c'est quelque chose
de compliqué et puis de pas quantifié et c'est donc donc tu as volé idée
ramora tu sera aux prises avec une transgression majeure est ici sans aucun
doute la transgression majeure c'est celle du meurtre ici verica ou nefesh
20 m donc si tu commences à laisser place à la haine les conséquences de
la haine vont se poursuivre ce qui est logique c'est ce qu'on voit aussi
en termes de répercussions dans l'inconscient si la base émotionnels et
compliqués ça va être très très difficile de ne pas laisser s'échapper
d'une façon ou d'une autre des éléments de haine même dans la tonalité
de la voix et donc au début batterie lassana au début ils arrivent à rire
car ici la gaieté varaire car beckham à la ville s'est levé ou axa reprend
chaque fois les mots que on a ici en face va rire carrick a ou n'a fait ch et
après il l'a mis à mort d'où l'interdiction de la hem et la nécessité
de gérer la m alors ça c'est quand même comment fait on pour comment
fait-on pour gérer la haine eh bien il faut parler à son prochain essai
la période de techouva qu'on a maintenant qui commence avec rhj hashanah
et yom kippour et c'est donc est ce qu'on se peut se parler les uns les unes
aux autres pour remédier aux brisures qui a dans les relations en tout cas
ici on nous dit les choses ont des conséquences et quand on transgresse une
petite chose eh bien il faut surtout analyser quelles pourraient être les
conséquences pour déraciner cela est renouvelé un comportement qui soit
complètement dans la lignée de ce qui est important pour nous je ne dis
pas que c'est facile à faire je dis juste que ce verset qui est le premier
que nous lirons dans la torah ce shabbat pour les personnes qui lisent la
lecture triennal classique avec m cal est vain ce sera ce par ce verset qu'on
commencera et cette invitation à s'occuper des petites choses des choses
invisibles pour que dans le domaine du visible tout se passe bien et qu'on
se retrouve pas à assumer les conséquences des conséquences voilà donc
bien poser les choses je vous souhaite une bonne semaine de réflexion


2021-08-12 Paracha Choftim : le maître du lieu par Floriane Chinsky
===========================================================================

- https://www.youtube.com/watch?v=oHDXrUdBps4&list=PLnHlXjFx9rOR31vysZ3ChRMSbrUNnnKN2

.. youtube:: oHDXrUdBps4

notre Paracha de la semaine s'appelle Choftim elle est dans le
deutéronome sait faire tu vas rim à partir du chapitre 16 et sa première
phrase est la suivante chauffe team des juges v show trim et une police titane
les rates tu te donnera à toi-même bégrolles charrieras dans toutes tes
portes hr adonaï et le crâne hautaine layrac et l'éternel ton dieu te
donne les chiffres des rats pour tes tribu verschave tout est à ham et
luchon le peuple mich pâte cdec d'un jugement de justice cette année la
paracha Choftim tombe au tout début du mois deux et loool de telle sorte
que nous nous approchons quand même bien tranquillement de tichri et des
grandes fêtes [Musique] 1,2 vers celles les plus célèbres de notre part
achats et au chapitre 17 et il nous dit qu'il faut nous donner des juges et
des prêtres qui seront en ce temps en notre temps by admin à m et c'est
vers eux qu'il faut se tourner brachy nous interroge il faut se tourner
vers les juges du temps c'est une évidence parce qu on aurait beaucoup de
mal à se tourner vers les juges des temps passés alors pourquoi insister
sur le fait qu'on se tourne vers le juge de l'époque c'est parce que il
faut qu'ils aient une adaptation de la loi à la réalité à la réalité
tangible connue de toutes et de tous c'est l'expression talmudique et alaric
éternelle de mahradé atra le maître du lieu c'est à dire qu'il est
normal qu'en différents temps ou en différents lieux les décisions et les
options prises par les rabbins ne soient pas les mêmes parce qu'elles sont
adaptés à la réalité tangible et c'est pour ça qu'il faut préciser
dans le cuir à trouver le juge de ce temps là de maintenant évidemment
ça nous tourner vers cette responsabilité qui est la nôtre de former des
rabbins et des leaders communautaires qui soit à la hauteur des défis du
jour et est notre responsabilité autant que juifs et juive d'aujourd'hui
de nous-mêmes être à la hauteur des exigences très particulière de ce
temps là le nôtre notre part achats touche des sujets très importants
qui sont un peu comme une constitution pour quand les enfants d'israël
s'installeront en terre d'israël et donc il s'agit des juges bien sûr des
rois mais aussi des prophètes et aussi de ce qui se passe quand il ya des
besoins de ville refuge où trouver une place comment mettre en place les
témoignages et en fin de compte qu'est ce qui se passe s'il ya un meurtre
non élucidé c'est à dire qu'on peut pas trouver le coupable mais on peut
pas non plus faire semblant de rien [Musique] vous pouvez approfondir le sens
et tous les détails de cette part achats en allant là lire intégralement
sur le site sfar him.fr en cherchant des commentaires sur attaque ademe sur
les vidéos youtube de la chaîne judaïsme en mouvement et parmi ces vidéos
vous trouverez du côté des miennes quelque chose autour de comment trouver
la justice et autour de qu'est-ce qu'un roi dans notre tradition qu'est ce
que pourrait être la royauté et aussi de la liberté des femmes nos fils
ont des rois c'est le titre un peu paradoxal de cette vidéo il ya plein de
façons de l'approfondir notre Paracha Choftim shabbat shalom [Musique]
paracha/ Deutéronome Flor


2019-09-06 Paracha Choftim: nos filles sont des rois! (et nous aussi) par Floriane Chinsky
===============================================================================================

- https://www.youtube.com/watch?v=LrzFTChQNn0&list=PLnHlXjFx9rOR31vysZ3ChRMSbrUNnnKN2

.. youtube:: LrzFTChQNn0

Ah bon? ne sont-elles pas plutôt des reines? Depuis l'adoption de la loi 
salique en France, une reine n'est plus le féminin grammatical du roi, 
le titre est purement "honorifique", la reine de règne jamais mais est 
tout au plus le ventre censé porter l'héritier mâle.

Sarah n'est-elle pas elle-même "reine"? La reine du Chabbat n'est-elle 
pas l'invitée la plus digne, forte dans la non-violence, et nous l'accueillons 
ensemble, hommes et femmes, tous les vendredi soir? 

Le "roi" évoqué dans notre paracha, ne doit pas avoir trop de chevaux, 
d'argent, et de femmes. 

Qu'est-ce que cette phrase nous dit de la royauté? de l'autorité? 
de la hiérarchie? de la place des femmes dans ce type de systèmes de castes?

Cette année, nos études talmudique du mercredi midi reprendront les textes 
du talmud pour explorer leur position.

En attendant, bonne visualisation, et chabbat chalom, un chabbat de paix 
à toutes et à tous!


2016-09-05 Sur un pied! Paracha Choftim: Dieu est-il roi? par Floriane Chinsky
====================================================================================

- https://www.youtube.com/watch?v=9KNFJy8Q28U&list=PLnHlXjFx9rOR31vysZ3ChRMSbrUNnnKN2


.. youtube:: 9KNFJy8Q28U


parfois on entend que Dieu est roi et qu'est-ce que ça signifie que Dieu
est roi de quoi est-ce que Dieu est-il le roi et quelle est l'organisation
sociale telle qu'elle était prévue par la Torah qui n'a pas forcément
d'implication d'ailleurs sur l'organisation telle qu'elle devrait être
aujourd'hui qu'est-ce que ça signifie que Dieu est le roi dans notre
parachat chautime il y a justement une description des différents outils
des différents bras des différents éléments qui constituent l'état
on dit que Dieu est roi en particulier dans une période de l'année qui
est la nôtre maintenant qui approche qui est celle de rosashana et de Yom
kipur ou à rochashana dans le fil de Moussaf par exemple on on fait des
malrouillotes mer c'est le roi malrouyot c'est des choses qui rappellent la
royauté de Dieu tout en soufflant du chauffard et de la même façon à Kip
pour on dit que Dieu est à la fois roi mais qu'il est aussi on l'appelle
aussi juge dans un des putimes dans une des poésies liturgiques très très
connu et très importante de Kipour qui s'appelle chauet colaret le juge de
toute la terre toute la terre est-ce que Dieu aujourd'hui est considéré
au présent au quotidien comme un roi et comme un juge pour pour le peuple
juif et dans notre tradition alors si on revient un petit peu à l'origine
de cette expression on voit qu'elle a per dans la bouche d'Abraham d'une
façon très très spécifique très particulière et surprenante nous
sommes dans la Genèse au chapitre 18 au verset 22 et Abraham discute avec
Dieu de la question de la destruction de Sodome et gomor et il discute et
c'est là qu'il emploie l'expression chauffet col aret le juge de toute la
la terre mais il l'utilise pas du tout pour glorifier Dieu il l'utilise
pour remettre en cause la décision divine et le verset dit khil NON NON
enfin c'est impossible prends garde à toi est-ce que le juge de toute
la terre ne va pas faire le droit c'est-à-dire tu es le juge de toute la
terre donc tu ne peux pas détruire Sodom et gomor si tu le faisais c'est
une catastrophe c'est dans la contestation que Abraham donne à Dieu le nom
de chauffette col arrê et donc quand quiip pour on dit le juge de toute la
terre d'un côté c'est quelque chose qui a une profonde réalité au moment
des fêtes de ticherie mais d'un autre côté c'est également quelque chose
qui nous rappelle la puissance contestatrice d'Abraham notre père et c'est
la même chose dans notre parachat on nous dit Chau tu te donneras des juges
donc si on doit nous-même nous donner des juge c'est même une injonction
divine de se donner des juges temporels alors c'est certainement que la
justice est rendue par l'être humain et quand on continue avec la notion de
royauté notre parachat dans le chapitre 17 au verset 14 nous sommes dans
le Deutéronome nous parle de la royauté qui est exercé non pas par Dieu
directement non pas par un homme qui serait imprégné de la légitimité
et de l'essence divine ou quelque chose comme ça contrairement à ce qui
qui s'est passé dans d'autres traditions religieuses dans l'Antiquité et
même dans dans l'histoire de France jusqu'à la fin de la royauté où le
roi était comment imbibé de la puissance divine c'est pas du tout le cas
pour nous puisque le roi c'est un être humain il doit être choisi parmi
tes frère il doit pas trop mettre en valeur son pouvoir en a possédant
beaucoup de chevaux beaucoup de femmes et cetera et au contraire il doit
lui-même écrire un SFER torora même il doit en écrire deux un pour ses
archives et un qui transporte tout le temps avec lui il est soumis au droit
d'une façon directe en tant qu'être humain et pas en tant que incarnation
divine de la même façon le Prophète nous sommes toujours dans notre
parachat au chapitre 18 au verset 14 le Prophète il faut pas avoir peur de
lui il faut remettre en cause ce qu'il dit il faut vérifier que ce qu'il
énonce est conforme à la loi et il trouve le fondement de sa prophécie
dans le fait qu'il est conforme à l'enseignement précédent d'une part et
que il vit lui-même et qu'il met en en application le message qu'il essaie
de transmettre par ailleurs on voit ici que les institutions telles qu'elles
sont décrites dans la Bible sont nombreuses qu'elles se complètent qu'elles
dépendent les unes des autres la prétrise est aussi mentionnée dans notre
parachat qu'elles dépendent les unes des autres qu'il y a donc le roi le
juge le prêtre et le prophète comme un équilibre un contrebalancement
des pouvoirs à la mode des Lumières françaises et derrière ça Dieu
est principalement une inspiration pour chacun d'eux un fondement moral
de l'importance de l'État de droit c'est ce qu'on retrouve dans Kip pour
également où on se concentre sur cet aspect-là où on veut nous-même
être au meilleur de nos capacités d'exercice de notre liberté par rapport
à nos vies et où on rend ce pouvoir symbolique de la royauté au créateur
dans le poème liturgique chet colaret juge de toute la terre entre autres
et dans d'autres poèmes égalementù on dit nous sommes ton peuple et tu
es notre Dieu tu es l'objet de nos parole et nous sommes l'objet des tiennes
et avec cette réciprocité ce renforcement du lien qui nous donne la force
d'être plus proche de nos valeurs morales dans un exercice rationnel de la
compréhension humaine de ce qu'est la justice divine notre parachat chauve
team une royauté peut-être indirecte d'inspiration et de


2015-08-15 **Sur un pied ! Parasha Choftim : où trouver la justice ?** par Floriane Chinsky
===============================================================================================

- https://www.youtube.com/watch?v=hQYpf0WmXkg&list=PLnHlXjFx9rOR31vysZ3ChRMSbrUNnnKN2

.. youtube:: hQYpf0WmXkg


Vous voudriez apprendre toute la Torah juste pendant le temps où vous 
pouvez vous tenir sur un pied ? 

Bonne nouvelle, vous pouvez faire mieux : le temps de se tenir sur un 
pied suffit à poser une bonne question sur le judaïsme, à ouvrir le débat. 

"Le reste, ce sont des commentaires, va et étudie !"


Sources :
Deutéronome 16 :18

Tu institueras des juges et des magistrats dans toutes les villes que l'Éternel, ton Dieu, te 

donnera, dans chacune de tes tribus; et ils devront juger le peuple selon la justice.

שֹׁפְטִים וְשֹׁטְרִים, תִּתֶּן-לְךָ בְּכָל-שְׁעָרֶיךָ, אֲשֶׁר יְהוָה אֱלֹהֶיךָ נֹתֵן לְךָ, לִשְׁבָטֶיךָ; וְשָׁפְטוּ אֶת-הָעָם, מִשְׁפַּט-צֶדֶק.

michpat = phrase = sentence

Deutéronome 16 :20 

La justice, c’est la justice que tu poursuivras

Psaumes 34 :15

Demande la paix et poursuis-là

Pirké avot 10

 רבן שמעון בן גמליאל אומר על שלשה דברים העולם עומד על הדין ועל האמת ועל השלום שנאמר (זכריה ח) 

Le monde repose sur trois choses : 

- sur la justice (din) 
- sur la vérité  (émet) 
- et sur la paix  (chalom)

Deutéronome 17 :9

Et tu iras vers les cohanim-léviim et vers le juge qui sera dans ces temps-là et tu leur 

demanderas et ils te diront  ce qu’est la justice (michpat)

Talmud Babylonien Roch Hachana 25b

וכי תעלה על דעתך שאדם הולך אצל הדיין שלא היה בימיו? הא אין לך לילך אלא אצל שופט שבימיו, ואומר אל 

Ne dis pas « comment se fait-il que les époques précédentes étaient meilleures que celles-

ci ? »

Rachi sur Deutéronome 17 :9

Et même s’il ne statue pas comme les juges qui l’ont précédé il faut l’écouter, tu n’as pas 

d’autre choix que le juge de ton temps

צדק צדק תרדף למען תחיה וירשת את־הארץ אשר־יקוק אלהיך נתן לך: 

סור מרע ועשה־טוב בקש שלום ורדפהו:

אמת ומשפט שלום שפטו בשעריכם:

 (ט) ובאת אל־הכהנים הלוים ואל־השפט אשר יהיה בימים ההם ודרשת והגידו לך את דבר המשפט:

תאמר מה היה שהימים הראשונים היו טובים מאלה.

ואפילו אינו כשאר שופטים שהיו לפניו אתה צריך לשמוע לו. אין לך אלא שופט שבימיך:

sefaria Judges
==================

- https://www.sefaria.org/Judges?tab=contents

Judges (“Shoftim”) is the second book of the Prophets, describing the 
period after Joshua’s leadership and before the advent of monarchy, 
when Israel lacked long-term centralized leadership. 

The book details cycles of sin, foreign oppression, repentance, and 
redemption through leaders appointed by God, like Deborah, Gideon, and 
Samson. 

It ends by highlighting the chaos of the period with stories about theft, 
idolatry, rape, murder, and civil war.

