.. index::
   pair: Floriane Chinsky; Lettre à une lectrice en quête de paix (2024-09-20)

.. _chinsky_2024_09_20:

===============================================================================================
2024-09-20 🕯🕯💐 **Lettre à une lectrice en quête de paix**  |floriane|
===============================================================================================

- https://rabbinchinsky.fr/2024/09/20/chabbat-antisemitisme/

Introduction
==============

L’enfer est parfois pavé de bonnes intentions. 

Je pense que ce qui s’est passé pour cette lectrice de mon livre 
"des femmes et des dieux", qui m’a demandé de "m’exprimer clairement sur 
le douloureux sujet de la souffrance des Gazaouis". 

Je lui réponds, mon article est à la fin de ce post… שבת שלום שלום un 
chabbat de paix profonde et réelle à vous toutes et tous!

🕯🕯💐Chabbat Ki tavo + 📣article// antisémitisme inconscient + inscriptions pour Kipour ⚖️

**Lettre à une lectrice en quête de paix**
=================================================

Madame,

Au cours de votre lecture du livre "Des femmes et des dieux" dont je suis
co-autrice, vous prenez la peine de m’écrire. 

Vous vous dites : "Floriane Chinsky ne peut pas approuver le sort fait 
aux Gazaouis". 

Vous avez raison, je n’approuve pas le sort fait aux Gazaouis, leur situation 
est déchirante, la situation de l’ensemble de ce territoire est déchirante. 
Vous "n’oublie[z] pas les horreurs du 7 octobre", et je pense aussi à toutes
les autres victimes israéliennes depuis ce jour. 
Je n’approuve pas, et personne n’a demandé mon approbation. 

Cela est d’ailleurs tout à fait normal : je ne vis pas en Israël, je n’y 
vote pas, je ne suis pas une experte en relations internationales ni en 
géopolitique. 
Si on me demandait mon avis, la bonne réponse serait : parlez aux gens 
sur le terrain, parlez aux merveilleuses personnes qui luttent pour la 
paix là-bas. 
Pour que la paix arrive, **il faut mettre en avant les discours des personnes
concernées, informées, en capacité d’agir, en situation de rendre compte
de leur action**. Je vous laisse faire le tri, parmi toutes les personnes qui
s’expriment, de celles qui répondent réellement à ces critères. 
**Toutes les autres contribuent au brouhaha, à la confusion, à la guerre**.

Nous aimerions mettre fin à toutes les guerres. J’aimerais pouvoir agir
sur l’ensemble des violences du monde. Je suis là où je suis, et c’est
là, et seulement là, que je fais de mon mieux pour la paix. 
En tant que microminorité, représentant 0,2% de la population mondiale, 
la minorité juive est, par définition, microprésente. 
J’enseigne la paix dans ce livre que vous lisez, dans des tribunes, dans 
ma synagogue, sur mon site internet, sur ma chaine youtube, sur instagram. 
Je l’enseigne à travers le judaïsme mais aussi l’Ecoute Mutuelle, la 
Communication Empathique, la Sociocratie dans le cadre de mon association, 
Cocréer. 
**Je ne suis pas omniprésente, et cela m’est reproché, comme c’est reproché 
à de nombreuses personnes juives autour de moi**. 

**L’antisémitisme se nourrit de ces reproches**.

Vous le soulignez d’ailleurs avec douleur : "Les événements actuels
réveillent ANTISEMITISME ET ISLAMOPHOBIE". 
Je vous rejoins sur les faits : l’antisémitisme et l’islamophobie ont 
augmenté. 

Je vous rejoins dans votre jugement de valeur : **c’est un problème majeur**. 

**Je ne vous rejoins pas sur votre analyse : vous dites que ce sont les 
événements actuels qui réveillent ces tendances**. 
**Au contraire, ce sont les personnes antisémites et islamophobes elles-mêmes 
qui portent la responsabilité de leurs haines**. 

Il est **trop facile** d’accuser "les événements"
--------------------------------------------------------

Il est **trop facile** d’accuser "les événements". 

Chacun choisit sa façon de réagir aux événements et en porte la responsabilité. 

Je tiens à rappeler que de nombreuses personnes ont le courage de refuser 
l’antisémitisme et l’islamophobie. 

**Parlons des Guerrières de la Paix**, ces femmes juives et musulmanes françaises 
qui se battent ensemble. 

**Parlons du Cercle des familles endeuillées israéliennes-juives, 
israéliennes-palestiniennes et palestiniennes de Gaza et de Cisjordanie**, 
qui ont perdu des proches dans cette guerre, et qui malgré cela, cultivent 
l’entraide et la paix. 

Ces personnes prouvent que réagir de façon solidaire face aux événements est
possible. **Il ne faut pas accuser "les événements"**. 

**Il ne faut pas accuser les victimes**. 

.. _chinsky_2024_09_20_dja_8:

Il ne faut pas dire "ce qui atténuerait l’antisémitisme c’est que les personnes  juives expriment ouvertement leur souffrance devant celle des Gazaouis"
------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://fr.wikipedia.org/wiki/Serment_more_judaico
- :ref:`antisem:examples_antisem_israel_pal`
- :ref:`antisem:ligne_8_dja`


Il ne faut pas dire "ce qui atténuerait l’antisémitisme c’est que les personnes 
juives expriment ouvertement leur souffrance devant celle des Gazaouis". 

Comment ferait-on ? 

On rassemblerait tous les juifs sur une place et on leur ferait faire 
une déclaration publique ? Un genre de `more judaico ? <https://fr.wikipedia.org/wiki/Serment_more_judaico>`_ 

**Non, il ne faut accuser aucune victime, mais au contraire, réhumaniser et 
respecter toutes les victimes. Toutes**. 

**Et interpeler tous les racismes**.

A votre demande, je me suis "exprim[ée] ouvertement sur ce douloureux sujet". 

Je vous demande, de votre côté, de tendre l’oreille aux voix étouffées 
des minorités, et d’agir en pleine conscience de votre pouvoir d’action 
et de paix. 

**Je reste pour ma part entièrement engagée dans mon travail de paix, je serai 
heureuse de vous y accueillir**.
