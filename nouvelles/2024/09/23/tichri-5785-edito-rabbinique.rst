

.. _chinsky_2024_09_23:

========================================================================================================================
2024-09-23 Tichri 5785 vu par le rabbin Floriane Chinsky Que commence une nouvelle année, et ses bénédictions !
========================================================================================================================

- https://judaismeenmouvement.org/actualites/tichri-5785-edito-rabbinique/


Que commence une nouvelle année, et ses bénédictions !
=============================================================

Bientôt Tichri, bientôt Roch hachana, bientôt Kippour, bientôt ces
fêtes qui nous permettent de tirer un trait sur le passé, d’ouvrir une
nouvelle page. "Tirer un trait sur le passé", cette expression est
quasiment parfaite pour traduire le mot de Kippour, bien plus juste que le
sempiternel "jour des expiations" ou "jour du grand pardon". 

Pour le judaïsme, pour nous, aujourd’hui, avant tout, **nous voulons tirer
un trait sur le passé**. Depuis le XIIIe siècle, nous chantons le poème
**aHot Ketana, "petite sœur"**, qui parle avec tendresse du peuple juif,
de ses espoirs, qu’il tisse pour l’année à venir, de ses traumatismes,
qu’il s’efforce de dépasser. 
Le refrain s’égrène, à huit reprises : **"Que s’achève l’année et ses malédictions !"**.

**Il faut le redire : L’année écoulée a été dure. L’enfer a commencé
le 7 octobre dernier, au moment de conclusion des fêtes de Tichri**. 

Depuis ce jour, le cœur du judaïsme bat au rythme des nouvelles des otages, à
la cadence des actes antisémites explicites, ou dissimulés. 

La question récurrente est apparue dans de nombreux articles : "En quoi cette année
est différente de toutes les autres années ?". Les rabbins du monde entier
se sont interrogés : "Que rajouter dans notre liturgie, pour prendre en
compte les événements douloureux actuels ?". Ces questions font écho
à notre sentiment profond d’injustice, au caractère insupportable de la
situation. Il faut réagir ! Créer de nouveaux textes, de nouveaux rituels !

Et pourtant, à l’occasion du bilan de fin d’année qui approche,
j’hésite. Il faut réagir, oui, mais de quelle façon ? Faut-il créer
de nouvelles pratiques, de nouvelles prières, de nouveaux décors dans nos
synagogues ? 

Les avis rabbiniques d’aujourd’hui sont partagés. 
Certes, l’esprit d’action juif nous y pousse. Mais à côté de cet esprit
d’action, nous avons également une puissance d’équilibre et de
continuité. Nous devons agir, bien sûr, mais cela n’est pas nouveau,
nous réagissons et nous agissons depuis 2000 ans. Les horreurs du présent
font écho aux horreurs du passé, et notre résilience présente nait de
notre résilience passée. Nous pouvons nous appuyer sur l’immense édifice
rituel et symbolique que nous avons construit tout au long de notre histoire.
“Nous réagissons et nous agissons depuis 2000 ans !”

Lorsque je lis les textes traditionnels à la synagogue, à chaque mot,
je pense à la justesse des réactions de nos anciens face aux terribles
situations traversées. Face au traumatisme, il faut avant tout nous soucier de
nos besoins primaires, manger, être ensemble, cultiver nos ressources. 
Les sages de la michna nous proposent une clef : celle des bénédictions à
prononcer justement, avant de se nourrir, aux moments de rassemblements,
lorsqu’on étudie. Roch Hachana nous y invite, avec son repas de fête,
son seder, sa pomme et son miel.

**Face aux injustices inadmissibles du monde, il faut agir et tempérer
l’action par des temps de ressourcement**. Nos pratiques nous y encouragent,
faisant se succéder le travail de la semaine et **le repos du Chabbat**, les
efforts de l’année et **le ressourcement de Tichri**. 

Pour garder son équilibre mental dans l’adversité, il faut embrasser sa 
vulnérabilité. 

La fête de Kippour nous accompagne avec le jeune et l’humilité, la fête de
Souccot nous guide, avec sa fragile cabane. Pour rester vraiment en vie,
enfin, **il faut entretenir le chemin de la joie**. 
**Celle-ci est présente à chacune de ces célébrations, et culmine avec Sim’hat Torah**.

Rabbi Bounam, rabbin polonais du début du XIXe siècle, nous invite à
porter dans notre poche un papier portant la célèbre phrase de la michna
(sanhédrin 4 :5) : "Le monde a été créé pour moi !". 

Roch Hachana est la fête de la célébration du monde, de notre puissance d’action dans
le monde, du bilan du monde. Roch Hachana est la fête qui porte la joie,
la joie qui est le moteur de l’action constructive. L’estime de soi ne
suffit pas, il faut également savoir s’effacer devant l’autre. 

Selon Rabbi Bounam, nous devrions porter une inscription qui contrebalance la
première (Gen. 18 :27) : "Je ne suis que cendre et poussière". 

Kippour est la fête du regret, qui est le moteur du changement. 
Avez-vous saisi le clin d’œil de cette dernière phrase, et son sens caché ? 
Le "Je ne suis que cendre et poussière" est prononcé initialement par Abraham,
lorsqu’il s’oppose à dieu, pour protéger les habitants de Sodome et
Gomorrhe. 

**L’humilité juive consiste à s’opposer à l’ordre injuste du monde, sans 
céder à la crainte des représailles**.

Cette année, comme toutes les autres années, l’esprit juif d’action et
de raison, d’impétuosité et de tempérance, sera à nos côtés. 

Nous avons ouvert ces réflexions avec le "Que ce finisse cette année et ses
malédictions" du aHot Kétana. Je souhaite les conclure avec sa dernière
phrase, que je nous souhaite du plus profond du cœur : Que s’ouvre une
nouvelle année, pleine de bénédictions !

Rabbin Floriane Chinsky
