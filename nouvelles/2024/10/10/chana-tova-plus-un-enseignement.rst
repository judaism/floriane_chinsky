.. index::
   ! Justice nulle part, liberté partout (2024-10-10)

.. _chinsky_2024_10_10:

=====================================================================================================================================================================
2024-10-10  🍎🍯 **Chana Tova !** + 📜 **Un enseignement : Justice nulle part, liberté partout, La liberté est partout, et il nous appartient de nous en saisir**
=====================================================================================================================================================================

- https://rabbinchinsky.fr/2024/10/10/%f0%9f%8d%8e%f0%9f%8d%afchana-tova-%f0%9f%93%9cun-enseignement/


Chana Tova !
===================

L’année 5785 vient de s’ouvrir, je vous souhaite du fond du cœur une 
année de force, de droiture, de connexion avec des personnes qui vous 
aiment et vous respectent. 

Une année où vous accomplirez ce qui vous tient à coeur, où vous agirez 
en adéquation avec vos valeurs, avec détermination et ouverture. 

Et bien sûr également avec santé, de quoi manger et vous abriter, de quoi 
étudier, la santé pour vos proches. 

Et évidemment, une année d’apaisement pour toutes les personnes victimes 
des violences dans le monde, de résilience si possible, de survie au moins, 
jusqu’à la venue, nous l’espérons, de jours meilleurs.

Rendez-vous vendredi soir et samedi toute la journée au Palais de la Femme 
pour un Kipour qui nous rassemble et nous régénère!

Voici ici un enseignement que je partage avec vous: **Justice nulle part, liberté partout**
==============================================================================================

A quoi bon agir pour le bien ? Nous savons que la justice ne triomphera pas. 
Après l’année que nous avons passée, nous avons vu toutes sortes d’événements 
d’une dureté incroyable.

L’année que nous avons passée a été très dure, disais-je, et on pourrait 
se décourager. 
On pourrait se dire que nous sommes impuissant.es, que nos fautes sont 
peut-être trop importantes, qu’elles surpasseront nécessairement nos bonnes 
actions. 

On pourrait se dire que les fautes du monde sont trop importantes, et que 
jamais, quoi que nous fassions, nous ne pourrons remonter la pente. 

Ceci peut s’appliquer au sujet de différentes réalités de notre monde 
actuel, que je ne préciserai pas. 
Ce n’est pas faux, mais il existe une autre façon de voir les choses, 
celle de Maimonide.

Ce halahiste, commentateur et penseur du 12e siècle, ayant vécu quatre 
ans dans le 13e, nous dit la chose suivante : maimonide hilhot téchouva 3 :4

לְפִיכָךְ צָרִיךְ כָּל אָדָם שֶׁיִּרְאֶה עַצְמוֹ כָּל הַשָּׁנָה כֻּלָּהּ כְּאִלּוּ חֶצְיוֹ זַכַּאי וְחֶצְיוֹ חַיָּב. וְכֵן כָּל הָעוֹלָם חֶצְיוֹ זַכַּאי וְחֶצְיוֹ חַיָּב. חָטָא חֵטְא אֶחָד הֲרֵי הִכְרִיעַ אֶת עַצְמוֹ וְאֶת כָּל הָעוֹלָם כֻּלּוֹ לְכַף חוֹבָה וְגָרַם לוֹ הַשְׁחָתָה. עָשָׂה מִצְוָה אַחַת הֲרֵי הִכְרִיעַ אֶת עַצְמוֹ וְאֶת כָּל הָעוֹלָם כֻּלּוֹ לְכַף זְכוּת וְגָרַם לוֹ וְלָהֶם תְּשׁוּעָה וְהַצָּלָה שֶׁנֶּאֱמַר (משלי י כה) " וְצַדִּיק יְסוֹד עוֹלָם"  זֶה שֶׁצָּדַק הִכְרִיעַ אֶת כָּל הָעוֹלָם לִזְכוּת וְהִצִּילוֹ. וּמִפְּנֵי עִנְיָן זֶה נָהֲגוּ כָּל בֵּית יִשְׂרָאֵל לְהַרְבּוֹת בִּצְדָקָה וּבְמַעֲשִׂים טוֹבִים וְלַעֲסֹק בְּמִצְוֹת

Pour cette raison les personnes doivent se considérer toute l’année comme 
si elles étaient à moitié méritantes et à moitié coupables. 

Et de même le monde entier, comme à moitié méritant et à moitié coupable. 

Ainsi, si même une seule personne commet une faute, elle se transfère 
elle-même et le monde entier du côté du mal, et est la cause qu’il se 
détruise. si même une seule personne commet une bonne action, elle se 
transfère elle-même et le monde entier du côté du mérite, et est la 
cause de son sauvetage…

J’ai toujours considéré que Maïmonide nous encourageait à la vigilance, 
à la précision, pour donner le maximum de chance au monde. 

J’ai toujours considéré qu’il nous mettait la pression. 

Au vu de cette année, je vois les choses autrement : il nous invite à ne pas désespérer
-------------------------------------------------------------------------------------------

Au vu de cette année, je vois les choses autrement : il nous invite à ne 
pas désespérer. 

**Nos actions ont une importance, c’est ce qu’il faut considérer**. 

Nos actions de l’année passée sont pleines de sens, et nous pouvons prendre 
le temps de les recompter, comme le fait le berger comptant ses moutons 
dans le merveilleux texte "ountané tokef"  que nous chanterons demain et 
vendredi matin. 

**Chaque mois passé, chaque semaine, chaque jour, chaque heure, compte**, 
et nous apporte son trésor d’expérience. 
Ces heures n’ont pas suffi à apporter la justice dans le monde mais elles 
nt été le reflet de nos choix, de notre liberté. 
Cette liberté fait aujourd’hui l’objet d’un bilan, en ce jour solennel 
de roch hachana.

Au vu du sens de ce temps de vie passé, le talmud roch hachana considère 
que nous sommes placés dans une de ces trois catégories : les justes parfaits, 
les méchants absolus, et les "moyens" .

" שלוש כיתות הן ליום הדין: אחת של צדיקים גמורין ואחת של רשעים גמורין ואחת של בינונים…"  (ראש השנה ט" ז, ב).

Pour Maimonide, que nous venons de lire, nous sommes toutes et tous placé.es 
parmi les "moyens"  dont chaque acte peut faire pencher la balance. 
Il soutient ici avec le talmud Yoma (38b) qui affirme :

"rabbi Hiya bar aba a dit que rabbi YoHanan avait dit : **"même pour une 
seule personne juste, le monde peut subsister"**  יומא ל״ח ב:י״ד

(אָמַר) רַבִּי חִיָּיא בַּר אַבָּא אָמַר רַבִּי יוֹחָנָן: אֲפִילּוּ בִּשְׁבִיל צַדִּיק אֶחָד הָעוֹלָם מִתְקַיֵּים, שֶׁנֶּאֱמַר: ״וְצַדִּיק יְסוֹד עוֹלָם״. רַבִּי חִיָּיא דִּידֵיהּ אָמַר, מֵהָכָא: ״רַגְלֵי חֲסִידָיו יִשְׁמוֹר״ — חֲסִידָיו טוּבָא מַשְׁמַע! אָמַר רַב נַחְמָן בַּר יִצְחָק: חֲסִידוֹ כְּתִיב.

La discussion sur le nombre de justes nécessaires à la poursuite du monde 
est virulente. 
Dans ce même passage la guémara proteste qu’il s’agit nécessairement de 
plusieurs personnes, le midrach considère qu’il en faut 30, le talmud 45. 

Bien plus tard, le légendaire yiddish parlera pour sa part des 36 justes, 
les lamedvavnikim, justes cachés et ignorés, méprisées et invisibles, 
sans lesquelles pourtant le monde ne pourrait subsister.

Ces justes maltraitées témoignent que le monde ne rend pas justice aux  personnes qui agissent avec justice
--------------------------------------------------------------------------------------------------------------

Ces justes maltraitées témoignent que le monde ne rend pas justice aux 
personnes qui agissent avec justice. 

Cela, nous le savons. 

Mais ces personnes témoignent également d’une autre chose : Pour celles 
et ceux qui choisissent la liberté, cette liberté leur appartient totalement, 
quelles que soient les circonstances. 

Laissez-moi vous raconter une histoire talmudique à ce sujet
-----------------------------------------------------------------------

Cette histoire est tragique. Elle se déroule entre le 1e et le 2e siècle 
de la période romaine, dans la province appelée judée, autrement dit, 
en israel. 
Elle concerne le père de la célèbre commentatrice Berouria, **rabbi Hanina ben Tradion**. 

Le traité Avoda zara nous en raconte différents épisodes, nous nous 
concentrons sur la page 18a :

תנו רבנן כשחלה רבי יוסי בן קיסמא  הלך רבי חנינא בן תרדיון לבקרו אמר לו       חנינא אחי (אחי) אי אתה יודע שאומה זו מן השמים המליכוה שהחריבה את ביתו ושרפה את היכלו והרגה את חסידיו ואבדה את טוביו ועדיין היא קיימת ואני שמעתי עליך שאתה יושב ועוסק בתורה [ומקהיל קהלות ברבים] וספר מונח לך בחיקך

Rabbi Yossi ben Kisma dit à Rabbi Hanina : La soumission à l’autorité 
est de mise, car si l’autorité reste impunie, cela signifierait qu’elle 
est légitime aux yeux de la divinité ! 

**Tu prends des risques en enseignant la Torah !**
----------------------------------------------------------

אמר לו         מן השמים ירחמו          אמר לו          אני אומר לך דברים של טעם ואתה אומר לי מן השמים ירחמו תמה אני אם לא ישרפו אותך ואת ספר תורה באש אמר לו רבי מה אני לחיי העולם הבא

Hanina ben tradion et Rabbi Yossi Ben Kisma (traduction littérale sur la 
feuille de sources):

- Je garde espoir !
- Tu mourras sur un bucher avec un sefer torah
- Aurais-je une place dans le monde futur ?
- tu as fait quelque chose de particulier ?
- J’ai confondu et décidé de donner tout l’argent que j’avais mélangé 
  aux personnes dans le besoin.
- j’aimerais que ma part soit comme la tienne !

אמר לו          כלום מעשה בא לידך      אמר לו        מעות של פורים נתחלפו לי במעות של צדקה וחלקתים לעניים        אמר לו         אם כן מחלקך יהי חלקי ומגורלך יהי גורלי

אמרו       לא היו ימים מועטים עד שנפטר רבי יוסי בן קיסמא       והלכו כל גדולי רומי לקברו והספידוהו הספד גדול          ובחזרתן מצאוהו לרבי חנינא בן תרדיון שהיה יושב ועוסק בתורה ומקהיל קהלות ברבים וס" ת מונח לו בחיקו

Rome rend hommage à Rabbi Yossi et **met à mort Rabbi Hanina**, ils 
l’enveloppent dans un sefer torah, l’entourent de branche, mettent le feu, 
posent de la laine humide sur son corps pour prolonger son supplice.

הביאוהו      וכרכוהו בס" ת     והקיפוהו בחבילי זמורות     והציתו בהן את האור     והביאו ספוגין של צמר     ושראום במים      והניחום על לבו               כדי שלא תצא נשמתו מהרה  

 אמרה לו בתו       אבא אראך בכך      אמר לה      אילמלי אני נשרפתי לבדי היה הדבר קשה לי     עכשיו שאני נשרף וס" ת עמי מי שמבקש עלבונה של ס" ת הוא יבקש עלבוני

Rabbi Hanina et sa fille, la sage Berouria :

- Père, dois-je te voir ainsi ?
- Si j’étais seul, ce serait grave pour moi, mais ils brulent également 
  un sefer torah, c’est grave pour eux !

אמרו לו תלמידיו        רבי מה אתה רואה        אמר להן       גליון נשרפין ואותיות פורחות           אף אתה פתח פיך ותכנס בך האש         אמר להן      מוטב שיטלנה מי שנתנה ואל יחבל הוא בעצמו

Rabbi Hanina et ses étudiants :

- Que vois-tu ?
- Les lettres s’envolent.
- Ouvre la bouche et abrège tes souffrances ?!
- Ce n’est pas à moi de le faire.

אמר לו קלצטונירי       רבי אם אני מרבה בשלהבת ונוטל ספוגין של צמר מעל לבך        אתה מביאני לחיי העולם הבא

אמר לו      הן      השבע לי       נשבע לו        

מיד הרבה בשלהבת ונטל ספוגין של צמר מעל לבו יצאה נשמתו במהרה אף הוא קפץ ונפל לתוך האור

Rabbi Hanina et son bourreau:

- Je raccourcis ton supplice et tu m’emmènes dans le monde futur?
- d’accord
- fais en le serment
- d’accord

Rabbi Hanina meurt, le bourreau se jette dans le feu. 
Une voix divine annonce : Rabbi Hanina ben Tradyon et le bourreau sont 
invites au monde futur

יצאה בת קול ואמרה         רבי חנינא בן תרדיון וקלצטונירי מזומנין הן לחיי העולם הבא

בכה רבי ואמר יש קונה עולמו בשעה אחת ויש קונה עולמו בכמה שנים

Comme vous le voyez, dans cette histoire, comme dans d’autres, ce n’est pas la justice qui triomphe. **La justice n’est nulle part**
========================================================================================================================================

Comme vous le voyez, dans cette histoire, comme dans d’autres, ce n’est 
pas la justice qui triomphe. La justice n’est nulle part. 

L’oppression romaine réussit à mettre à mort le père de Berouria. 
Pourtant, la liberté est partout : Rabbi Hanina ben tradion, pourtant, 
garde sa liberté, il choisit. 

Quand enseigner la torah risque de lui couter la vie, il choisit 
d’enseigner la torah. 

Quand ouvrir la bouche peut lui permettre d’abréger ses souffrances, 
il choisit de la garder fermée. 

Lorsque son bourreau lui propose de hâter sa fin, il choisit de lui 
dire oui. 

Nous également, dans les difficiles circonstances de nos vies, nous 
choisissons. 
Nous choisissons à chaque instant. 

Chacun de nos choix peut faire pencher l’équilibre de notre balance 
personnelle, et nous inscrire dans le livre de la vie. 
Chacun de nos choix peut faire pencher l’équilibre du monde, et permettre 
au monde de subsister. 

La question n’est pas la justice, mais la liberté et la solidarité. 

**La liberté est partout, et il nous appartient de nous en saisir**.


Palais de la femme
=======================

- https://fr.wikipedia.org/wiki/Palais_de_la_Femme

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=2.377949953079224%2C48.85254555102873%2C2.38503098487854%2C48.85554224668076&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/48.854044/2.381490">Afficher une carte plus grande</a></small>
