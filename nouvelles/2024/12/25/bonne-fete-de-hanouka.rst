
.. _floriane_chinsky_2024_12_25:

==========================================================================
2024-12-25 🕎Bonne fête de Hanouka! ❤️‍🩹❤️‍🔥🥰
==========================================================================

- http://rabbinchinsky.fr/2024/12/25/%f0%9f%95%8ebonne-fete-de-hanouka%e2%9d%a4%ef%b8%8f%f0%9f%a9%b9%e2%9d%a4%ef%b8%8f%f0%9f%94%a5%f0%9f%a5%b0/
- :ref:`judaisme:hanoucca`

Bonjour mes ami.es!

Je ne sais pas pour vous, mais de mon côté, j’ai vraiment besoin de cette 
lumière et de cette énergie de Hanouka. 

Il y a beaucoup de choses dans la vies auxquelles on ne peut rien. 
J’aimerais faire tellement plus, dans tous les domaines!

Mais il y a une chose qu’on peut faire ce soir, prendre une bougie, une 
spéciale de Hanouka, ou n’importe laquelle, prononcer les bénédictions, 
l’allumer, et passer un moment d’apaisement face à ces lumières.

Puissions-nous seulement être une lumière dans la nuit! (Vous avez tout dans 
la suite de cette newsletter)

- https://www.youtube.com/watch?v=alDQqnQidtQ

.. youtube:: alDQqnQidtQ

Allumer, ce n’est pas compliqué. On peut le faire au niveau super light 
(juste allumer une bougie) ou au niveau ultra-pro (avec chandelier spécial, 
beignets, toupie, chants). 

Le plus important, c’est de ne pas se laisser impressionner. 

Voici le mode d’emploi:

Les deux bénédictions sont:

- BarouH ata adonaï élohénou mélèH haolam acher kidéchanou bémitsvotav 
  vétsivanou léhadlik ner chel Hanouka,
- BarouH ata adonaï élohénou mélèH haolam chéassa nissim laavoténou bayamim 
  hahem bazéman hazé,

On ajoute le premier soir:

- BarouH ata adonaï élohénou mélèH haolam cheHéyanou vékiyémanou véhiguianou lazéman hazé.

En une phrase, l’allumage officiel des bougies de Hanouka consiste dans le 
fait:

- d'allumer,
- entre la tombée de la nuit et la fin de la circulation piétonne dans les rues,
- des bougies multicolores ou pas,
- au moins une bougie chaque soir et selon l’opinion acceptée de Hillel une 
  bougie puis deux puis trois etc.,
- placées près de la fenêtre ou près de la porte en face de la mézouza,
- dans un bougeoir spécial ou pas,
- pendant 8 jours, à partir de ce soir,
- en prononçant les deux bénédictions traditionnelles,
- et à passer 30 minutes joyeuses près des bougies,
- qu’on soit un homme ou une femme.

Pour alimenter les discussions autour des bougies, les chants, les jeux, 
voici deux éléments exclusifs de cette année, ainsi que toutes les ressources 
des années précédentes:


Hag OURIM SaméaH! 

