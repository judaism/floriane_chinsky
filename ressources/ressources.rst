
.. index::
   pair: Ressources; Mastowall

.. _ressources_chinsky:

=======================================================================================================
Ressources
=======================================================================================================

- http://linkertree.frama.io/judaisme


#ShalomSalaam #CeaseFireNow #Israel #Gaza #Mazeldon #Jewdiverse #Peace #Palestine
#jewish #ethics #morals #values #betterworld #israel #hebrew

Mastowall
===========

- https://rstockm.github.io/mastowall/?hashtags=peace,paix,pace&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=mazeldon,jewish,jewdiverse&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=torah&server=https://framapiaf.org
- https://rstockm.github.io/mastowall/?hashtags=chinsky&server=https://framapiaf.org


Calendriers
============

- http://www.calj.net/
- https://www.hebcal.com/

https://rabbinchinsky.fr
===========================

- https://rabbinchinsky.fr


Shabbats
==========

- https://judaism.gitlab.io/shabbats/

Youtube
=======

- https://www.youtube.com/c/FlorianeChinsky/videos
- https://invidious.fdn.fr/channel/UCDtvtoXqGz0Niwl2QGM8gyQ

Peertube
=========

- https://peertube.iriseden.eu/a/floriane_c/video-channels


Bokertov
===========

- https://judaism.gitlab.io/bokertov/


Judaisme
==========

- https://judaism.gitlab.io/judaisme/



Judaisme en mouvement
=======================

- https://judaismeenmouvement.org/


|paix| Guerrières de la Paix |hanna|
==========================================

- :ref:`guerrieres:paix`
